import React from "react";
import {Button, TextField} from "@mui/material";
import Identify from "./Identify";
import Branch from "./variableBranch";

class BranchWrapper2 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            parsedString: this.props.string,
            phase: this.props.phase,
            button: "",
            selection: false,
            //correct: "",
            brancher: this.props.brancher,
        };

        this.phase2 = this.phase2.bind(this);
        this.passChoice = this.passChoice.bind(this);
        this.setChoice = this.setChoice.bind(this);

    }

    passChoice(b, s) {
        let state = this.state;
        state.button = b;
        state.selection = s;
        if(s)
            state.brancher = b;
        else
            state.brancher = ("¬"+b);
        this.setState(state);
    }
    /*passCorrect(c) {
        let state = this.state;
        state.correct = c;
        this.setState(state);
    }*/

    setChoice(){
        this.props.handleBranch(this.state.button, this.state.selection);
    }

    phase2(){
        let state = this.state;
        state.phase = "2";
        this.setState(state);
    }

    render() {
        switch (this.state.phase) {
            default :
                return(
                    <div>

                    </div>
                )
            case "2" :
                return(
                    <div>
                        <h3>Branching (Simplification)</h3>
                        <Identify phase={"branch1"}
                                  prepareBranch={this.props.prepareBranch}
                                  string={this.state.parsedString}
                                  handleBranch = {this.props.handleBranch}
                                  sendData={this.props.sendData}
                                  doUP={this.props.doUP}
                                  doPLE={this.props.doPLE}
                                  detectBranchFailure={this.props.detectBranchFailure}
                                  doSuccess = {this.props.doSuccess}
                                  setChoice={this.setChoice}
                                  brancher={this.state.brancher}
                                  handleConflict = {this.props.handleConflict}
                                  addPoints = {this.props.addPoints}
                                  wrongAnswerPenalty = {this.props.wrongAnswerPenalty}>
                        </Identify>
                    </div>
                )
        }
    }
}

export default BranchWrapper2;