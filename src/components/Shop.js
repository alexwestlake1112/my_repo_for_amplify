import React from "react";
import Cookies from "universal-cookie";
import axios from "axios";
import ShopItem from "./ShopItem";

class Shop extends React.Component {
    constructor(props) {
        super(props);
        this.cookies = new Cookies();
        this.state = {
            loggedIn: 0,
            result: [],
            buyErrorMessage: "",
        }
        this.getItems = this.getItems.bind(this);
        this.buyItem = this.buyItem.bind(this);
        this.listThing = this.listThing.bind(this);
        this.getComponent = this.getComponent.bind(this);
    }

    componentDidMount() {
        //here we want to pull the list of shop items from the shop
        //BUT ONLY IF LOGGED IN.
        let token = this.cookies.get('token');
        if(token !== undefined){
            this.setState({loggedIn : 1})
            this.getItems()
        }
    }

    getItems(){
        //here we want the API call to get the items
        //(bc we'll also call it after buying one)
        axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/getItems?userid=" + this.props.userid).then(r => {
            console.log(r.data);
            this.setState({result : r.data})
        })
    }

    buyItem(itemid){
        //here we want to invoke the API call to buy an item
        //check existence of login cookie
        let token = this.cookies.get('token');

        if (token !== undefined) {
            //if exists, make api call
            let data = {
                "userid" : this.props.userid,
                "itemid" : itemid,
                "token" : token
            }
            console.log(data);
            axios.post("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/buyItem", data).then(r => {
                console.log(r.data)
                if (r.data.userid !== -1) {
                    if (r.data.userid !== -2) {
                        if (r.data.userid !== -3) {
                            if (r.data.userid !== -4) {
                                if (r.data.userid !== -5) {
                                    this.getItems();
                                    this.props.setCoins(r.data.coins);
                                } else {
                                    //failed to set points or level
                                    this.setState({buyErrorMessage : "Insufficient coins to purchase item ("+itemid+")"})
                                }
                            } else {
                                //failed to set points or level
                                this.setState({buyErrorMessage : "Failed to set coins"})
                            }
                        } else {
                            //failed to set points or level
                            this.setState({buyErrorMessage : "Failed to retrieve item data"})
                        }
                    } else {
                        //failed to get stuff
                        this.setState({buyErrorMessage : "Failed to retrieve user data"})
                    }
                } else {
                    //token invalid
                    this.setState({buyErrorMessage : "Purchase of item ("+itemid+") failed due to invalid token. Please log out and back in."})
                }
            })
        } else {
            //if none, set error message saying relog
            this.setState({buyErrorMessage : "Purchase of item ("+itemid+") failed due to invalid token. Please log out and back in."})
        }
    }

    listThing(data){
        if(data.length !== 0){
            return data.map((d) => <ShopItem buyItem = {this.buyItem} itemId ={d.itemId} name = {d.name} price = {d.price} description = {d.description}></ShopItem>)

        } else {
            return (<h3>No items left to buy!</h3>)
        }
    }

    getComponent(){
        if(this.state.loggedIn === 1){
            return(
                <div>
                    <div>{this.listThing(this.state.result)}</div>
                </div>
            )
        }
        else return <p>Please log in or register to access the Shop!</p>
        //here we want to either prompt user to log in, or list their unbought items.
    }

    render(){
        return(
            <div>
                <h2>Shop</h2>
                <p>{this.state.buyErrorMessage}</p>
                {this.getComponent()}
            </div>
        )
    }
}
export default Shop;