import React from 'react'

class ModeChooser extends React.Component {

    //TODO: improve mode choosing UI
    render() {
        return (
            <div>
                <div>
                    <button onClick={this.props.doEntry}><h1>CNF solving</h1></button>
                    <button onClick={this.props.doTree}><h1>Tree view</h1></button>
                </div>
                <div>
                    <button onClick={this.props.doChallenge}><h1>Daily Challenge</h1></button>
                    <button onClick={this.props.doMultiplayer}><h1>Multiplayer mode</h1></button>
                </div>
            </div>

        )
    }
}

export default ModeChooser;