import React from "react";
import {TextField} from "@mui/material";
import axios from "axios";
import Cookies from "universal-cookie";

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.cookies = new Cookies();
        this.state = {
            username: "",
            password: "",
            password2: "",
            mode: 1,
            test: "",

            usernameSuccess: "",
            userid: -1,
            xp: 0,
            level: 0,
            coins: 0,

            loginPasswordError: false,
            loginPasswordText: "",
            loginUsernameError: false,
            loginUsernameText: "",

            regUsernameError: false,
            regUsernameText: "",
            regPasswordError: false,
            regPasswordText: "",

            tokenExpired: "",
        };
        this.attemptLogin = this.attemptLogin.bind(this);
        this.logout = this.logout.bind(this);
        this.deactivate = this.deactivate.bind(this);
        this.getComponent = this.getComponent.bind(this);
        this.attemptRegistration = this.attemptRegistration.bind(this);
        this.hideLogin = this.hideLogin.bind(this);
        this.addPoints = this.addPoints.bind(this);
        this.endingThing = this.endingThing.bind(this);
        this.fromEndingText = this.fromEndingText.bind(this);
    }

    componentDidMount() {
        //TODO: Before setting state stuff, do a check on the cookie in question via database
        // - if invalid, delete the cookie (Get the database to do the same in background).
        // - if correct, pull UID, xp, coins, levels. store em on bar or something idk lol
        let token = this.cookies.get('token');

        if (token !== undefined) {
            //check if cookie is valid
            //axios.get("//localhost:8080/api/checkToken?token="+token).then(r => {
            axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/checkToken?token="+token).then(r => {
                console.log(r.data)
                if (r.data.userid !== -1) {
                    //gather data
                    this.setState({userid: r.data.userid})
                    this.setState({xp: r.data.xp})
                    this.setState({coins: r.data.coins})
                    this.setState({level: r.data.level})
                    this.setState({usernameSuccess: r.data.username})

                    this.setState({mode : 0})
                    this.props.setLoginState(0)

                    this.props.sendUserData(r.data, r.data.username);
                    console.log(r.data.username)
                }
            }).catch(
                (error) => {
                    console.log(error)
                    //this.cookies.remove('token')
                    this.setState({userid : -1})
                    this.setState({xp: 0})
                    this.setState({coins: 0})
                    this.setState({level: 0})
                    this.setState({usernameSuccess: ""})

                    this.setState({mode : 1})
                    this.props.setLoginState(1)
                })




            //send to app also

        }
    }
    //TODO: fromEnding, setFromEnding
    //  make an api call to trigger sendpoints
    attemptLogin() {
        //First just send my string to the main app, so it can be used in subsequent functions

        let data = {
            "username" : this.state.username,
            "password" : this.state.password
        }
        //http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/
        //axios.post("//localhost:8080/api/login", data).then(r => {
        axios.post("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/login", data).then(r => {
            //check for blank username error
            if(r.data.userid !== -3){
                //check for blank password error
                if(r.data.userid !== -2) {
                    //check for failed login
                    if (r.data.userid !== -1) {
                        if (this.cookies.get('token') !== undefined) {
                            this.cookies.remove('token')
                        }
                        let options = {}
                        /*if (this.cookies.get('cookiePreference')[1] && this.state.rememberMe)
                            options = {path: '/', sameSite: 'lax', secure: true, maxAge: 31557600}
                        else*/
                        //options = {path: '/', sameSite: 'lax', secure: true}
                        options = {path: '/', sameSite: 'lax', secure: true, maxAge: 31557600}
                        this.cookies.set('token', r.data.token, options)
                        this.setState({test: r.data.token})
                        this.setState({userid: r.data.userid})
                        this.setState({usernameSuccess: this.state.username})
                        this.setState({mode: 0})
                        this.props.setLoginState(0)
                        this.props.sendUserData(r.data, this.state.username);
                        if(this.props.fromEnding === 1){
                            //Do the sending of points but from this component
                            let data = {
                                "userid" : r.data.userid,
                                "xp" : this.props.points,
                                "coins" : 0,
                                "level" : 0,
                            }
                            console.log("Data i'm sending:")
                            console.log(data)
                            this.addPoints(data);
                            this.props.setFromEnding(0);
                            //this.endingThing().then(
                            //    document.getElementById("Ending2").forceUpdate
                            //)

                            //this.props.evilEndingSendPoints();
                            //this.props.setFromEnding(0);
                        }
                    } else {
                        //password mismatch, throw password mismatch error
                        this.setState({loginPasswordError: true})
                        this.setState({loginPasswordText: "Incorrect password"})
                    }
                } else {
                    this.setState({loginUsernameError: true})
                    this.setState({loginUsernameText: "Username cannot be blank"})
                }
            } else {
                this.setState({loginPasswordError: true})
                this.setState({loginPasswordText: "Password cannot be blank"})
            }
        })
    }

    //TODO: change the dataSent and dataErrorMessage to be properties of app.js rather than Endings
    addPoints(data) {
        axios.post("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/addPoints", data).then(r => {
            console.log(r.data)
            if (r.data.userid !== -1) {
                if (r.data.userid !== -2) {
                    if (r.data.userid !== -3) {
                        this.props.sendUserData(r.data, this.state.username);
                        this.props.setDataSent("succeeded")
                        this.props.setNotLogged(0);
                    } else {
                        //failed to set points or level
                        this.props.setDataErrorMessage("Failed to set points/level")
                    }
                } else {
                    //failed to get stuff
                    this.props.setDataErrorMessage("Failed to retrieve user data")
                }
            } else {
                //token invalid
                this.props.setDataErrorMessage("Token invalid/expired, please log out and in again")
            }
        })
    }

    async endingThing(){
        await this.props.setFromEnding(0);
        /*
        let ending = document.getElementById("Ending2");
        console.log(ending);
        await ending.forceUpdate();*/
    }

    attemptRegistration(){
        //First just send my string to the main app, so it can be used in subsequent functions
        let data = {
            "username" : this.state.username,
            "password" : this.state.password,
            "password2": this.state.password2
        }
        axios.post("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/register", data).then(r => {
            //First, check that passwords aren't mismatched
            if(r.data.userid !== 5) {
                if (r.data.userid !== -4) {
                    if (r.data.userid !== -3) {
                        if (r.data.userid !== -2) {
                            if (r.data.userid !== -1) {
                                if (this.cookies.get('token') !== undefined) {
                                    this.cookies.remove('token')
                                }
                                let options = {}
                                /*if (this.cookies.get('cookiePreference')[1] && this.state.rememberMe)
                                    options = {path: '/', sameSite: 'lax', secure: true, maxAge: 31557600}
                                else*/
                                //options = {path: '/', sameSite: 'lax', secure: true}
                                options = {path: '/', sameSite: 'lax', secure: true, maxAge: 31557600}
                                this.cookies.set('token', r.data.token, options)
                                this.setState({test: r.data.token})
                                this.setState({userid: r.data.userid})
                                this.setState({usernameSuccess: this.state.username})
                                this.setState({mode: 0})
                                this.props.setLoginState(0)
                                this.props.sendUserData(r.data, this.state.username);
                                if(this.props.fromEnding === 1){
                                    this.props.setFromEnding(0);
                                    document.getElementById("Ending").forceUpdate();
                                    //this.props.evilEndingSendPoints();
                                    //document.getElementById("Ending").addPoints2();
                                    //this.props.setFromEnding(0);
                                }
                            } else {
                                //username taken, throw incorrect username error
                                this.setState({regUsernameError: true})
                                this.setState({regUsernameText: "Username taken"})
                            }
                        } else {
                            //password mismatch, throw password mismatch error
                            this.setState({regPasswordError: true})
                            this.setState({regPasswordText: "Password mismatch"})
                        }
                    } else {
                        //invalid userid generation
                        this.setState({regUsernameError: true})
                        this.setState({regUsernameText: "ID generation error"})
                    }
                } else {
                    //empty password
                    this.setState({regPasswordError: true})
                    this.setState({regPasswordText: "Password required"})
                }
            } else {
                //empty username
                this.setState({regUsernameError: true})
                this.setState({regUsernameText: "Username required"})
            }
        })
        //Code to set error message for username error
        //this.setState({regUsernameError : true});
        //this.setState({regUsernameText : "Username taken"});

        //Code to set error message for password error
        //this.setState({regPasswordError : true});
        //this.setState({regPasswordText : "Passwords not matching"});
    }

    logout() {
        let data = {
            "userid" : -1,
            "xp" : 0,
            "level": 0,
            "coins": 0
        }

        axios.delete("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/logout/"+ this.state.userid).then(() => {
            this.cookies.remove('token')
            this.setState({mode : 1})
            this.setState({userid : -1})
            this.setState({usernameSuccess: ""})
            this.props.setLoginState(1)
            this.props.sendUserData(data, "Guest");
        })
    }

    showDeactivate() {
        const deacon = document.getElementById("deactivateContainer");
        console.log("show")
        deacon.style.display = "block";
    }
    hideDeactivate() {
        const deacon = document.getElementById("deactivateContainer");
        console.log("hide")
        deacon.style.display = "none";
    }

    deactivate(password) {
        let data = {
            "password" : password,
            "token": this.cookies.get("token"),
            "userid": this.state.userid
        }

        console.log(data);

        axios.post("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/deactivatePrelim", data).then(r => {
            console.log ("preliminary checks")

            if(r.data.userid !== -2){
                if (r.data.userid !== -1){
                    //If we pass all the checks, deactivate for real.
                    axios.delete("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/deactivate?userid="+ this.state.userid).then(() => {
                        console.log("deactivating account")

                        this.cookies.remove('token')
                        this.setState({mode : 1})
                        this.setState({userid : -1})
                        this.setState({usernameSuccess: ""})
                        this.props.setLoginState(1);
                        this.props.hideDeactivate();
                        this.props.sendUserData({
                            "userid" : -1,
                            "xp" : 0,
                            "level": 0,
                            "coins": 0
                        }, "Guest");
                    })
                }
                else{
                    //password mismatch, throw password mismatch error
                    this.props.setdeactivateIncorrectError(true);
                    this.props.setdeactivateIncorrectText("Incorrect password")
                }
            }
            else{
                //invalid token, log out
                this.cookies.remove('token')
                this.setState({mode : 1})
                this.setState({userid : -1})
                this.props.setLoginState(1)
                //and throw error message
                this.setState({tokenExpired : "User access token expired, please log in"})
                this.props.sendUserData({
                    "userid" : -1,
                    "xp" : 0,
                    "level": 0,
                    "coins": 0
                }, "Guest");
            }
        })
    }

    hideLogin(){
        //hide all them errors
        this.setState({
            loginUsernameError: false,
            loginUsernameText: "",
            loginPasswordError: false,
            loginPasswordText: "",
            regUsernameError: false,
            regUsernameText: "",
            regPasswordError: false,
            regPasswordText: "",
            deactivateIncorrectError: false,
            deactivateIncorrectText : "",
            tokenExpired: "",
        })
        this.props.setdeactivateIncorrectError(false);
        this.props.setdeactivateIncorrectText("")
        //if(this.props.fromEnding === 1){
        //    this.props.setFromEnding(0);
        //}
        this.props.hideLogin();
    }

    fromEndingText(){
        if(this.props.fromEnding === 1){
            return "Log in or register to receive points earned."
        }
        else return null;
    }


    getComponent(){
        let component;
        //initialiseTree();
        // eslint-disable-next-line default-case
        //{this.cookies.get('token')}
        switch (this.state.mode){
            default:
                component = <div>
                                <h2>Account</h2>
                                <div className={"loginContents"}>
                                    <div>
                                        <p>Logged in!</p>
                                    </div>
                                    <div>
                                        <button onClick={this.logout}>Logout</button>
                                    </div>
                                    <div>
                                        <button onClick={this.showDeactivate}>Deactivate account</button>
                                    </div>
                                    <div>
                                        <button className="loginbutton" onClick={this.hideLogin}>Back</button>
                                    </div>
                                </div>
                            </div>
                break;
            case 1 :
                component = <div>
                                <h2>Login</h2>
                                <h3>{this.state.tokenExpired}</h3>
                                <p> {this.fromEndingText()}</p>
                                <div className={"loginContents"}>
                                    <div>
                                        <TextField
                                            className={"inputBox"}
                                            id={"entryField"}
                                            name={"myCNF"}
                                            error={this.state.loginUsernameError}
                                            helperText={this.state.loginUsernameText}
                                            value={this.state.username} label="username"
                                            onChange={(e) => {
                                                this.setState({username : (e.target.value)});
                                                this.setState({loginUsernameError : false});
                                                this.setState({loginUsernameText : ""});
                                            }}
                                        />
                                    </div>
                                    <div>
                                        <TextField
                                            className={"inputBox"}
                                            id={"entryField"}
                                            name={"myCNF"}
                                            value={this.state.password} label="password"
                                            error={this.state.loginPasswordError}
                                            helperText={this.state.loginPasswordText}
                                            onChange={(e) => {
                                                this.setState({password : (e.target.value)});
                                                this.setState({loginPasswordError : false});
                                                this.setState({loginPasswordText : ""});
                                            }}
                                        />
                                    </div>
                                    <div>
                                        <button onClick={() => this.setState({mode : 2})}>Register</button>
                                        <button onClick={this.attemptLogin}>Submit</button>
                                    </div>
                                    <div>
                                        <button className="loginbutton" onClick={this.hideLogin}>Back</button>
                                    </div>
                                </div>
                            </div>
                break;
            case 2 :
                component = <div>
                                <h2>Register</h2>
                                <p> {this.fromEndingText()}</p>
                                <div className={"loginContents"}>
                                    <div>
                                        <TextField
                                            className={"inputBox"}
                                            id={"entryField"}
                                            name={"myCNF"}
                                            error={this.state.regUsernameError}
                                            helperText={this.state.regUsernameText}
                                            value={this.state.username} label="username"
                                            onChange={(e) => {
                                                this.setState({username : (e.target.value)});
                                                this.setState({regUsernameError : false});
                                                this.setState({regUsernameText : ""});
                                            }}
                                        />
                                    </div>
                                    <div>
                                        <TextField
                                            className={"inputBox"}
                                            id={"entryField"}
                                            name={"myCNF"}
                                            value={this.state.password} label="password"
                                            onChange={(e) => {
                                                this.setState({password : (e.target.value)});
                                            }}
                                        />
                                    </div>
                                    <div>
                                        <TextField
                                            className={"inputBox"}
                                            id={"entryField"}
                                            name={"myCNF"}
                                            error={this.state.regPasswordError}
                                            helperText={this.state.regPasswordText}
                                            value={this.state.password2} label="confirm password"
                                            onChange={(e) => {
                                                this.setState({password2 : (e.target.value)});
                                                this.setState({regPasswordError : false});
                                                this.setState({regPasswordText : ""});
                                            }}
                                        />
                                    </div>
                                    <div>
                                        <button onClick={this.attemptRegistration}>Submit</button>
                                    </div>
                                    <div>
                                        <button className="loginbutton" onClick={() => this.setState({mode : 1})}>Back</button>
                                    </div>

                                </div>
                            </div>
                break;
        }
        return component;
    }


    render() {
        return(
            <div>
                {this.getComponent()}
            </div>
        )
    }
}

export default Login;