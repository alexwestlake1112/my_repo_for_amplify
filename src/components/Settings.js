import React from 'react';
import Cookies from "universal-cookie";
import axios from "axios";
import Theme from "./Theme";
class Settings extends React.Component{
    constructor(props) {
        super(props);
        this.cookies = new Cookies();
        this.state = {
            value: this.cookies.get("theme"),
            loggedIn: 0,
            items: []
        }
        this.getItems2 = this.getItems2.bind(this);
        this.listThing2 = this.listThing2.bind(this);
        this.selectTheme = this.selectTheme.bind(this);
        this.getComponentS = this.getComponentS.bind(this);
    }

    componentDidMount() {
        //set the theme state
        if (this.cookies.get("theme") === undefined) {
            //if (this.cookies.get('cookiePreference')[1])
            //    this.cookies.set("theme", "Default", {path: "/", sameSite: "lax", maxAge: 31557600})
            //else
                this.cookies.set("theme", "Default", {path: "/", sameSite: "lax"})
        }
        document.documentElement.setAttribute("data-theme", this.cookies.get("theme"));

        this.setState({ value: this.cookies.get("theme")});
        //document.getElementById("themeselect").value = this.cookies.get("theme");

        //get the items
        let token = this.cookies.get('token');
        if(token !== undefined){
            this.setState({loggedIn : 1})
            this.getItems2()
        }
    }

    getItems2(){
        //here we want the API call to get the items
        //(bc we'll also call it after buying one)
        axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/getItems2?userid=" + this.props.userid).then(r => {
            console.log(r.data);
            this.setState({items : r.data})
        })
    }

    listThing2(data){
        return data.map((d) => <Theme selectTheme = {this.selectTheme} itemId ={d.itemId} name = {d.name} description = {d.description}></Theme>)
    }

    selectTheme(name){
        //if (this.cookies.get('cookiePreference')[1])
        //    this.cookies.set("theme", name, {path:"/", sameSite:"lax",maxAge: 31557600});
        //else
            this.cookies.set("theme", name, {path:"/", sameSite:"lax", maxAge: 31557600});
        document.documentElement.setAttribute("data-theme", this.cookies.get("theme"));
        this.setState({value: name})
    }

    getComponentS(){
        if(this.state.loggedIn === 1){
            return(
                <div>
                    <div>
                        <Theme selectTheme = {this.selectTheme} itemId ={-1} name = {"Default"} description = {"Default green theme, accessible for all users."}></Theme>
                        <Theme selectTheme = {this.selectTheme} itemId ={-2} name = {"High Contrast"} description = {"High Contrast theme, accessible for all users."}></Theme>
                        {this.listThing2(this.state.items)}
                    </div>
                </div>
            )
        }
        else return (
            <div>
                <Theme selectTheme = {this.selectTheme} itemId ={-1} name = {"Default"} description = {"Default green theme, accessible for all users."}></Theme>
                <Theme selectTheme = {this.selectTheme} itemId ={-2} name = {"High Contrast"} description = {"High Contrast theme, accessible for all users."}></Theme>
                <p>Please log in or register to access user-owned themes!</p>
            </div>
        )
        //here we want to either prompt user to log in, or list their unbought items.
    }

    render(){
        return(
            <div>
                <h3>Settings</h3>
                <h2>Current theme: {this.state.value}</h2>
                {this.getComponentS()}
            </div>
        )
    }
}
export default Settings;