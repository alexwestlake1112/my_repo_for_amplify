import React from "react";

class Backtrack extends React.Component {
    render(){
        return(
            <div>
                <h3>Backtrack!</h3>
                <button onClick={this.props.handleBacktrack}>Backtrack!</button>
            </div>
        )
    }
}
export default Backtrack;