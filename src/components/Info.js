import React from "react";

class Info extends React.Component {
    constructor(props) {
        super(props);
        this.getComponent = this.getComponent.bind(this);
    }
    getComponent(){
        let component;
        //initialiseTree();
        // eslint-disable-next-line default-case
        //{this.cookies.get('token')}
        switch (this.props.currentComponent){
            default :
                component = <h1>Info</h1>
                break;
            case "failure" :
                component = <p>The CNF you've solved is unsatisfiable! This doesn't mean you've lost, rather that you've found the CNF's satisfiability via exhaustive search.</p>
                break;
            case "success" :
                component = <p>The CNF you've solved is satisfiable! This means you've found one of the possible ways to assign truth values to certain variables for the formula to return True.</p>
                break;
            case "UP" :
                component = <p>This is Unit Propagation. Your goal in this phase is to simplify the CNF by finding the clauses with only one literal, and removing those literals' complements from the other clauses.</p>
                break;
            case "PLE" :
                component = <p>This is Pure Literal Elimination. Your goal in this phase is to simplify the CNF by identifying the literals that don't have a complement, then removing the clauses that contain them.</p>
                break;
            case "backtrack" :
                component = <p>You've reached a dead end! While this branch may not be a satisfying CNF assignment, you still have branches remaining that you can backtrack to in order to continue.</p>
                break;
            case "entry" :
                component = <p>This is the CNF Entry screen. Input your own CNF, or use the Random buttons to generate a random CNF to solve. Random CNFs will award coins that can be used to buy themes in the shop!</p>
                break;
            case "tree" :
                component = <p>This is Tree View! Here, you can see decision trees for a CNF you input. Try the random generation button and see the different trees that appear!</p>
                break;
            case "branch" :
                component = <p>This is the Branching phase. Select a variable to branch on, and the Boolean value to assign it. Be careful - unnecessary branching will result in point deduction!</p>
               break;
            case "branch1" :
                component = <p>This is the Branching phase. Select a variable to branch on, and the Boolean value to assign it. Be careful - unnecessary branching will result in point deduction!</p>
                break;
            case "branch2" :
                component = <p>This is the second section for Branching. You must now simplify the CNF based on the assignment you provided in the first part.</p>
                break;
            case "backbranch" :
                component = <p>This is the simplification for Backtracking. You're back at a previous state of the string, after backtracking - simplify it using the new assignment to see if it will be satisfiable this time!</p>
                break;
            case "chooser" :
                component = <p>This is the main menu! Choose a mode to start.</p>
                break;
            case "shop" :
                component = <p>This is the shop, where you can buy themes for the webapp. Coins are earned by solving CNFs through the singleplayer Random button, or by doing the daily Challenge.</p>
                break;
            case "settings" :
                component = <p>This is the settings menu, where you can choose themes that you've bought from the shop. The Green theme is unlocked by default, as is the High Contrast theme (for accessibility).</p>
                break;
            case "multiplayer" :
                switch (this.props.multiplayerComponent){
                    default :
                        break;
                    case "lobbySelect" :
                        component = <p>This is the lobby selection menu. Here, you can join players' existing lobbies if they haven't started yet. Or, create your own lobby and invite your friends to solve CNFs with you!</p>
                        break;
                    case "lobby" :
                        component = <p>This is the main lobby screen. Each user in your lobby can choose a CNF for the lobby to solve. Try generating a CNF - or if you're feeling particularly bold, choose the Random option to vote for an unseen, completely random CNF to be used! The CNF used for the lobby will be decided by majority vote; and in the event of a tiebreak, at random.</p>
                        break;
                    case "up1" :
                        component = <p>This is Unit Propagation. Your goal in this phase is to simplify the CNF by finding the clauses with only one literal, and removing those literals' complements from the other clauses.</p>
                        break;
                    case "up2" :
                        component = <p>This is Unit Propagation. Your goal in this phase is to simplify the CNF by finding the clauses with only one literal, and removing those literals' complements from the other clauses.</p>
                        break;
                    case "up3" :
                        component = <p>This is Unit Propagation. Your goal in this phase is to simplify the CNF by finding the clauses with only one literal, and removing those literals' complements from the other clauses.</p>
                        break;
                    case "random1" :
                        component = <p>This is Unit Propagation. Your goal in this phase is to simplify the CNF by finding the clauses with only one literal, and removing those literals' complements from the other clauses.</p>
                        break;
                    case "up4" :
                        component = <p>This is Unit Propagation. Your goal in this phase is to simplify the CNF by finding the clauses with only one literal, and removing those literals' complements from the other clauses.</p>
                        break;
                    case "random2" :
                        component = <p>This is Unit Propagation. Your goal in this phase is to simplify the CNF by finding the clauses with only one literal, and removing those literals' complements from the other clauses.</p>
                        break;
                    case "ple1" :
                        component = <p>This is Pure Literal Elimination. Your goal in this phase is to simplify the CNF by identifying the literals that don't have a complement, then removing the clauses that contain them.</p>
                        break;
                    case "ple2" :
                        component = <p>This is Pure Literal Elimination. Your goal in this phase is to simplify the CNF by identifying the literals that don't have a complement, then removing the clauses that contain them.</p>
                        break;
                    case "random3" :
                        component = <p>This is Pure Literal Elimination. Your goal in this phase is to simplify the CNF by identifying the literals that don't have a complement, then removing the clauses that contain them.</p>
                        break;
                    case "branchSelect" :
                        component = <p>This is the Branching phase. Each player in your lobby can select a variable to branch on, and the Boolean value to assign it. The branching literal will be decided by majority vote; and in the event of a tiebreak, at random. Be careful - unnecessary branching will result in point deduction!</p>
                        break;
                    case "branch1" :
                        component = <p>This is the second section for Branching. You must now simplify the CNF based on the assignment you provided in the first part.</p>
                        break;
                    case "branchrand1" :
                        component = <p>This is the second section for Branching. You must now simplify the CNF based on the assignment you provided in the first part.</p>
                        break;
                    case "branch2" :
                        component = <p>This is the second section for Branching. You must now simplify the CNF based on the assignment you provided in the first part.</p>
                        break;
                    case "branchrand2" :
                        component = <p>This is the second section for Branching. You must now simplify the CNF based on the assignment you provided in the first part.</p>
                        break;
                    case "ending" :
                        component = <p>You've beaten the CNF with your group! If you want to go again, go back to the main menu and rejoin a new lobby.</p>
                        break;
                }
                break;
            case "challenge" :
                component = <p>This is the daily challenge menu. Here, you can try the unique daily CNF. Be warned though - it's hard! Once beaten, you can see how you did compared to other users on the leaderboard!</p>
                break;
            case "blank" :
                component = <p>This is a blank component. You shouldn't be here, and neither should I...</p>
                break;
        }
        return component;
    }


    render() {
        return(
            <div>
                <h2>Info</h2>
                {this.getComponent()}
                <button onClick={()=> this.props.hideInfo()}>Back</button>
            </div>
        )
    }
}

export default Info;