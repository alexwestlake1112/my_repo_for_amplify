import React from "react";
import {TextField} from "@mui/material";

class ShopItem extends React.Component {
    render() {
        return(
            <div>
                <h3>{this.props.itemId}: {this.props.name} || Price: {this.props.price} coins</h3>
                <p>{this.props.description}</p>
                <button onClick={() => this.props.buyItem(this.props.itemId)}>BUY</button>
            </div>
        )
    }
}
export default ShopItem