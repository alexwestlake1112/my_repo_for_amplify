import React from "react";

class Theme extends React.Component {
    render() {
        return(
            <div>
                <h3>{this.props.name} </h3>
                <p>{this.props.description}</p>
                <button onClick={() => this.props.selectTheme(this.props.name)}>Select {this.props.name}</button>
            </div>
        )
    }
}
export default Theme