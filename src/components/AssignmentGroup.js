import React, { useState } from "react";
import {FormControlLabel, Switch} from "@mui/material";

const AssignmentGroup = ({ buttons, doSomethingAfterClick }) => {
    const [bool, setBool] = useState(false);
    const [bools, setBools] = useState(new Array(buttons.length).fill(false));

    const handleClick = (buttonLabel, boole) => {
        //setClickedId(buttonLabel);
        let arr = bools;
        arr[buttons.indexOf(buttonLabel)] = boole
        setBools(arr);
        //setBool(boole);
        doSomethingAfterClick(buttonLabel, boole);
    };

    function checkState() {
        return bool;
    }

    return (
        <>
            {buttons.map((buttonLabel) => (
                <FormControlLabel key = {buttonLabel} control={<Switch
                    name={buttonLabel} key={buttonLabel} id={buttonLabel}
                    //buttonContent={checkState()? "True" : "False"}
                    onChange={() => handleClick(buttonLabel, (! bools[buttons.indexOf(buttonLabel)]))}
                    //className={buttonLabel === clickedId ? "customButton active" : "customButton"}
                    />} label={buttonLabel}
                />)
            )}
        </>
    );
};

export default AssignmentGroup;