import React from "react";
import axios from "axios";
import Cookies from "universal-cookie";
import Leaderboard from "./Leaderboard";
class Endings extends React.Component {
    constructor(props) {
        super(props);
        this.cookies = new Cookies();
        this.state = {
            ending: this.props.ending,
            failureReason: this.props.failureReason,
            sent: false
        };
        this.getComponent = this.getComponent.bind(this)
        this.loginButton = this.loginButton.bind(this)
        this.addPoints = this.addPoints.bind(this)
        this.doLeaderboard = this.doLeaderboard.bind(this)
        this.sendResults = this.sendResults.bind(this)
    }

    componentDidMount() {
        if (this.props.ending !== "backtrack") {
            //check existence of login cookie
            let token = this.cookies.get('token');

            if (token !== undefined) {
                //check difficulty
                let coins;
                switch(this.props.difficulty){
                    default:
                        coins = 0;
                        break;
                    case "Easy":
                        coins = 10;
                        break;
                    case "Medium":
                        coins = 20;
                        break;
                    case "Hard":
                        coins = 30;
                        break;
                    case "Daily":
                        coins = 50;
                        break;
                }


                //if exists, make api call
                let data = {
                    "userid": this.props.userid,
                    "xp": this.props.points,
                    "coins": coins,
                    "level": 0,
                    "token": token
                }

                console.log(data);
                this.addPoints(data);
                if(this.props.difficulty === "Daily"){
                    this.sendResults();
                }
            } else {
                //if none, prompt login, set login state field using app prop
                this.props.setNotLogged(1);
                this.props.setFromEnding(1);
                //this.props.showLogin();
            }
        }
    }

    sendResults(){
        console.log(this.props);
        let data = {
            score : this.props.points,
            userid : this.props.userid,
            username : this.props.username
        }
        console.log(data);
        axios.post("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/sendResults", data).then(r => {
            this.setState({sent : true})
        })
    }

    loginButton(){
        switch(this.props.notLogged){
            default:
                return null;
            case 1:
                return <button onClick={this.props.showLogin}>Log in to receive earned points!</button>
        }
    }
    //TODO: change the dataSent and dataErrorMessage to be properties of app.js rather than Endings
    addPoints(data) {
        axios.post("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/addPoints", data).then(r => {
            console.log(r.data)
            if (r.data.userid !== -1) {
                if (r.data.userid !== -2) {
                    if (r.data.userid !== -3) {
                        this.props.sendUserData(r.data, this.props.username);
                        this.props.setDataSent("succeeded")
                    } else {
                        //failed to set points or level
                        this.props.setDataErrorMessage("Failed to set points/level")
                    }
                } else {
                    //failed to get stuff
                    this.props.setDataErrorMessage("Failed to retrieve user data")
                }
            } else {
                //token invalid
                this.props.setDataErrorMessage("Token invalid/expired, please log out and in again")
            }
        })
    }

    doLeaderboard(){
        if(this.props.difficulty === "Daily"){
            if(this.state.sent === true){
                return (<Leaderboard></Leaderboard>)
            }
        }
        else return null;
    }

    //<p>Wrong answers: {this.props.wrongAnswers}</p>

    getComponent(){
        let component;
        switch (this.state.ending) {
            default:
                component = <div>
                                <h3>Backtrack!</h3>
                                <p>{this.state.failureReason}</p>
                                <button onClick={this.props.handleBacktrack}>Backtrack!</button>
                            </div>
                break;
            case "failure" :
               component =  <div>
                                <h3>CNF Unsatisfiable!</h3>
                                <p>{this.state.failureReason}</p>
                                <p>Points gained: {this.props.rawPoints}</p>
                                <p>Penalty: {this.props.wrongAnswers * 5}</p>
                                <p>Final XP score: {this.props.points}</p>
                                <h3>Updating user stats {this.props.dataSent}.</h3>
                                <h3>{this.props.dataErrorMessage}</h3>
                                <div>{this.loginButton()}</div>
                            </div>
                break;
            case "success" :
                component = <div>
                                <h3>CNF satisfiable!</h3>
                                <p>Points gained: {this.props.rawPoints}</p>
                                <p>Times backtracked: {this.props.backtracks}</p>
                                <p>Penalty: {(this.props.backtracks * 30) + (this.props.wrongAnswers * 5)}</p>
                                <p>Final XP score: {this.props.points}</p>
                                <h3>Updating user stats {this.props.dataSent}.</h3>
                                <h3>{this.props.dataErrorMessage}</h3>
                                <div>{this.loginButton()}</div>
                            </div>
                break;
        }
        return component
    }
    render(){
        return(
            <div>
                {this.getComponent()}
                {this.doLeaderboard()}
            </div>
        )
    }
}
export default Endings;