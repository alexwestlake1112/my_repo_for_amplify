import React from "react";
import {TextField} from "@mui/material";
import axios from "axios";

//TODO: Implement input limitations as set out previously such that the provided input parses correctly
//      and preserves empty clauses upon reconstruction (Also necessary to rework backend to that purpose)

class Entry extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            parsedString: "",
            difficulty: 0
        };
        this.parseInput = this.parseInput.bind(this);
        this.doStringThing = this.doStringThing.bind(this);
        this.filterInput = this.filterInput.bind(this);
        this.literalDupes = this.literalDupes.bind(this);
        this.clauseDupes = this.clauseDupes.bind(this);
        this.useRandom = this.useRandom.bind(this);
        this.getDifficulty = this.getDifficulty.bind(this);
        this.toggleDifficulty = this.toggleDifficulty.bind(this);
    }
    parseInput(input){
        //Remove whitespace
        input = input.replaceAll(/ /g,'');

        //Remove brackets
        input = input.replaceAll("(", "");
        input = input.replaceAll(")", "");

        //Remove double negatives
        input = input.replaceAll("¬¬","");

        console.log(input);

        //Split the formula into clauses and set
        return input.split("∧");
    }

    doStringThing() {
        //First just send my string to the main app, so it can be used in subsequent functions
        if(this.props.detectBranchFailure(this.state.name)){
            this.props.toggleCanvas();
            this.props.setCurrentlyPlaying(1);
            this.props.sendData(this.state.name);
            //I need to check that there are unit clauses, or pure literals
            axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/testUP?CNF=" + this.state.name).then(r => {
                if(r.data) {
                    this.props.doUP(this.state.name);
                }
                else{
                    axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/testPL?CNF=" + this.state.name).then(r2 => {
                        if(r2.data) {
                            this.props.doPLE(this.state.name);
                        }
                        else{
                            console.log("oops")
                            this.props.prepareBranch(this.state.name);
                        }
                    })
                }
            })
        }
    }

    useRandom() {
        //First just send my string to the main app, so it can be used in subsequent functions
        switch(this.state.difficulty){
            default:
                this.props.setDifficulty("Easy")
                break;
            case 1:
                this.props.setDifficulty("Medium")
                break;
            case 2:
                this.props.setDifficulty("Hard")
                break;
        }
        let seed = -1
        axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/generateCNF?seed="+seed+"&difficulty="+(this.state.difficulty+1)).then(r3 => {
            let cnf = r3.data;
            if(this.props.detectBranchFailure(cnf)){
                this.props.toggleCanvas();
                this.props.setCurrentlyPlaying(1);
                this.props.sendData(cnf);
                //I need to check that there are unit clauses, or pure literals
                axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/testUP?CNF=" + cnf).then(r => {
                    if(r.data) {
                        this.props.doUP(cnf);
                    }
                    else{
                        axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/testPL?CNF=" + cnf).then(r2 => {
                            if(r2.data) {
                                this.props.doPLE(cnf);
                            }
                            else{
                                console.log("oops")
                                this.props.prepareBranch(cnf);
                            }
                        })
                    }
                })
            }
        })
    }

    literalDupes(clause){
        let literals = clause.split("∨");
        console.log(clause);
        console.log(literals);
        let a = "";
        let dupefinder = [];
        for(let l of literals){
            if(!dupefinder.includes(l)){
                a = a+l+"∨";
                dupefinder.push(l);
            }
        }
        console.log(a)
        return a.slice(0, -1);
    }

    clauseDupes(clauses){
        console.log(clauses);
        let nonDupes = [];
        for(let c of clauses){
            if (!(nonDupes.includes(c))){
                nonDupes.push(c);
            }
        }
        console.log(nonDupes)
        return nonDupes;
    }

    filterInput(str){
        let b = str.replace(/[^a-z¬∧∨]/gi, '');
        let clauses = this.parseInput(b);
        clauses = this.clauseDupes(clauses);
        let a = "";
        for(let c of clauses){
            console.log(c)
            c = this.literalDupes(c);
            c = "("+c+")";
            a = a+c+"∧";
        }
        let d = a.slice(0, -1)
        this.setState({name : d});
    }

    getDifficulty(){
        switch(this.state.difficulty){
            default:
                return "base";
            case 0:
                return "Easy";
            case 1:
                return "Medium";
            case 2:
                return "Hard";
        }
    }

    toggleDifficulty(){
        switch(this.state.difficulty){
            default:
                console.log("hmm")
                break;
            case 0:
                this.setState({difficulty : 1});
                break;
            case 1:
                this.setState({difficulty : 2});
                break;
            case 2:
                this.setState({difficulty : 0});
                break;
        }
    }

    /*
    //Otherwise, I just skip to branching!
                axios.get("//localhost:8080/api/oneRound?myCNF=" + this.state.name).then(r => {
                    let state = this.state
                    state.parsedString = r.data
                    this.setState(state)

                    //epic function to send data to parent
                    this.props.sendData(r.data);
                    //epic function to swap components
                    //this.props.updateComponent("branch");



                }
     */


    render() {
        return(
            <div>
                <div>
                    <h2>CNF input</h2>
                    <div>
                        <TextField
                            className={"inputBox"}
                            id={"entryField"}
                            name={"myCNF"}
                            error={this.state.name.length === 0}
                            helperText={!this.state.name.length ? 'CNF is required' : ''}
                            value={this.state.name} label="Enter your CNF"
                            onChange={(e) => {this.filterInput(e.target.value)}}
                        />
                        <button className="myButton" onClick={this.doStringThing}>Submit</button>
                        <div>
                            <button className="myButton" onClick={this.useRandom}>Random ({this.getDifficulty()})</button>
                            <button className="myButton" onClick={this.toggleDifficulty}>Toggle random difficulty</button>
                        </div>
                    </div>
                    <button className="myButton" onClick={() => {this.setState({name : (this.state.name + "¬")})}}>Add ¬</button>
                    <button className="myButton" onClick={() => {this.setState({name : (this.state.name + "∧")})}}>Add ∧</button>
                    <button className="myButton" onClick={() => {this.setState({name : (this.state.name + "∨")})}}>Add ∨</button>
                    <div>
                        <h3>Entered CNF: {this.state.name} </h3>
                    </div>
                </div>
            </div>
        )
    }
}

export default Entry;