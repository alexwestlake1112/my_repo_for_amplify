import React from "react";
import Cookies from "universal-cookie";
import axios from "axios";
import LeaderboardItem from "./LeaderboardItem";

class Leaderboard extends React.Component {
    constructor(props) {
        super(props);
        this.cookies = new Cookies();
        this.state = {
            result: [],
        }
        this.getLeaderboard = this.getLeaderboard.bind(this);
        this.listThing = this.listThing.bind(this);
        this.check = this.check.bind(this);
    }

    componentDidMount() {
        //here we want to pull the list of shop items from the shop
        //BUT ONLY IF LOGGED IN.
        this.getLeaderboard()

    }

    getLeaderboard(){
        //here we want the API call to get the items
        //(bc we'll also call it after buying one)
        axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/getLeaderboard").then(r => {
            console.log(r.data);
            this.setState({result : r.data})
        })
    }

    check(element, d) {
        return element === d;
    }

    listThing(data){
        if(!(data.length === 0)){
            data = data.sort(function (a, b) {
                return b.score - a.score;
            });
            return data.map((d) => <LeaderboardItem place = {data.indexOf(d) + 1} userid ={d.userid} username = {d.username} score = {d.score}></LeaderboardItem>)
        } else return(<p>Leaderboard empty - be the first!</p>)
    }

    render(){
        return(
            <div className={"itemWidget"}>
                <h3>Daily Challenge Leaderboard</h3>
                <div>
                    {this.listThing(this.state.result)}
                </div>
            </div>
        )
    }
}
export default Leaderboard