import React, { useState } from "react";

const ButtonGroup = ({ buttons, doSomethingAfterClick }) => {
    const [clickedId, setClickedId] = useState(-1);

    const handleClick = (buttonLabel) => {
        setClickedId(buttonLabel);
        doSomethingAfterClick(buttonLabel);
    };

    return (
        <>
            {buttons.map((buttonLabel) => (
                <button
                    name={buttonLabel} key={buttonLabel} id={buttonLabel}
                    onClick={(event) => handleClick(buttonLabel)}
                    className={buttonLabel === clickedId ? "customButton active" : "customButton"}
                >
                    {buttonLabel}
                </button>
            ))}
        </>
    );
};

export default ButtonGroup;