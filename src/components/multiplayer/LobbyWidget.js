import React from "react";
class LobbyWidget extends React.Component {
    listUsers(users){
        if(users.length !== 0){
            let ret = ""
            ret = ret + this.props.users.length + " "
            if(this.props.users.length == 1){
                ret = ret + "user: "
            } else ret = ret + "users: "
            for(let i = 0; i < (users.length-1); i++){
                ret = ret + users[i].username + ", ";
            }
            ret = ret + users[users.length-1].username;
            console.log("LobbyWidget ret: "+ret);
            return ret;
        }
        else return "No users."
    }
    render() {
        return(
            <div className={"itemWidget"}>
                <h3>{this.props.lobbyId}: {this.props.name}</h3>
                <p>{this.listUsers(this.props.users)}</p>
                <button onClick={() => this.props.joinLobby(this.props.lobbyId)}>Join lobby</button>
            </div>
        )
    }
}
export default LobbyWidget