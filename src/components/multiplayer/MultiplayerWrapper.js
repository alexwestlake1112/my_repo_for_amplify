import React from 'react';
import {TextField} from "@mui/material";
import LobbySelect from "./LobbySelect";
import Endings from "../Endings";
import UP from "../UP";
import PLE from "../PLE";
import Entry from "../cnfEntry";
import TreeView from "../treeView";
import BranchWrapper from "../branchWrapper";
import BackBranchWrapper from "../backbranchWrapper";
import ModeChooser from "../modeChooser";
import Shop from "../Shop";
import Settings from "../Settings";
import Lobby from "./Lobby";
import Cookies from "universal-cookie";
import axios from "axios";
import IdentifyMultiplayer from "./gamelogic/IdentifyMultiplayer";
import UP1 from "./gamelogic/up1";
import UP2 from "./gamelogic/up2";
import VariableBranchMultiplayer from "./gamelogic/variableBranchMultiplayer";
import Random1 from "./gamelogic/random1";
import UP4 from "./gamelogic/up4";
import Random2 from "./gamelogic/random2";
import PLE1 from "./gamelogic/ple1";
import PLE2 from "./gamelogic/ple2";
import Random3 from "./gamelogic/random3";
import Branch1 from "./gamelogic/branch1";
import BranchRand1 from "./gamelogic/branchrand1";
import Branch2 from "./gamelogic/branch2";
import BranchRand2 from "./gamelogic/branchrand2";
import UP3 from "./gamelogic/up3";
import EndingsMultiplayer from "./gamelogic/EndingsMultiplayer";

class MultiplayerWrapper extends React.Component{
    socket;
    constructor(props){
        super(props);
        this.cookies = new Cookies();
        this.state = {
            url: "",
            response: "",
            value: "",
            inLobby: 0,
            currentLobby: -1,
            update : 0,
            result : undefined,
            cnf: "",
            headingPhase: "",
            selectionsTrue: [],
            assignments: [],
            randomOutput1: [],
            randomOutput2: [],
            randomOutput3: [],
            randomOutput4: [],
            randomOutput5: [],

            ending: "",
            currentPoints: 0,
            rawPoints: 0,
            backtracks: 0,
            wrongAnswers: 0,

            //phase : "up1"     We will do this directly through specific individual cases via setMultiplayerComponent
        }
        this.openSocket = this.openSocket.bind(this)
        this.closeSocket = this.closeSocket.bind(this)
        this.handleLobby = this.handleLobby.bind(this)
        this.send = this.send.bind(this)
        this.hi = this.hi.bind(this)
        this.leaveLobby = this.leaveLobby.bind(this)
        this.submitCNF = this.submitCNF.bind(this)
        this.getComponent = this.getComponent.bind(this)
        this.toggleUpdate = this.toggleUpdate.bind(this)
        this.deleteCookie = this.deleteCookie.bind(this)
        this.getLobby = this.getLobby.bind(this)
        this.up1 = this.up1.bind(this)
        this.up2 = this.up2.bind(this)
        this.up3 = this.up3.bind(this)
        this.random1 = this.random1.bind(this)
        this.up4 = this.up4.bind(this)
        this.random2 = this.random2.bind(this)
        this.ple1 = this.ple1.bind(this)
        this.ple2 = this.ple2.bind(this)
        this.random3 = this.random3.bind(this)
        this.branch1 = this.branch1.bind(this)
        this.branchrand1 = this.branchrand1.bind(this)
        this.branch2 = this.branch2.bind(this)
        this.branchrand2 = this.branchrand2.bind(this)
        this.branchSelect = this.branchSelect.bind(this)
        this.leaveLobby2 = this.leaveLobby2.bind(this)
    }

    componentWillUnmount(){
        this.leaveLobby2()
    }

    componentDidMount() {
        this.deleteCookie();
        window.addEventListener("beforeunload", ()=>this.leaveLobby2())
        this.socket = null;
        let lobby = this.cookies.get('lobby');
        console.log("Lobby cookie: "+lobby)
        if (lobby !== undefined){
            this.setState({currentLobby : lobby});
            this.props.setComponent("lobby");
            this.getLobby();
        }
        console.log(this.props.component)
        /*
        this.state.socket.addEventListener("open", event => {
            this.state.socket.send("Connection established")
        });
        this.state.socket.addEventListener("message", event => {
            console.log("Message from server ", event.data)
            this.setState({response : event.data})
        });*/
    }

    getLobby(){
        console.log(this.state.currentLobby)
        if(this.state.currentLobby !== undefined){
            console.log("Attempting to get lobby of ID "+this.state.currentLobby)
            axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/getLobby?lobbyid="+ this.state.currentLobby).then(r => {
                console.log(r.data);
                this.setState({result : r.data})
            })
        }
    }

    async openSocket(){
        let parent = this;
        if(this.socket !== null){
            console.log("Already established socket.")
        } else {
            this.socket = new WebSocket('ws://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/myHandler');
            this.setState({url: this.socket.url});

            //The big dog, the big bear, the lion, the predator of the prey that is hiding
            this.socket.onmessage = function(event) {
                console.log(this.props)
                console.log("Websocket received data: " + event.data);
                //parent.setState({response: event.data});
                let jsonResponse = JSON.parse(event.data);
                //Now we enumerate cases.
                //CASE 1: Message from another user or server
                if (jsonResponse.hasOwnProperty("message")) {
                    //Add the message to the chat
                    let ul = document.getElementById("chatList")
                    let li = document.createElement("ul");
                    li.appendChild(document.createTextNode(jsonResponse.message))
                    ul.appendChild(li);
                }
                //CASE 2: Response from creating/joining lobby
                else if (jsonResponse.hasOwnProperty("lobbyConfirmation")) {
                    console.log(jsonResponse)
                    console.log("LobbyID: " + jsonResponse.lobbyid[0])
                    let newID = jsonResponse.lobbyid[0]
                    parent.setState({currentLobby: newID})
                    console.log("Just set the state of current lobby to " + jsonResponse.lobbyid[0])

                    if (parent.cookies.get('lobby') !== undefined) {
                        parent.cookies.remove('lobby')
                    }
                    let options = {}
                    /*if (this.cookies.get('cookiePreference')[1] && this.state.rememberMe)
                        options = {path: '/', sameSite: 'lax', secure: true, maxAge: 31557600}
                    else*/
                    //options = {path: '/', sameSite: 'lax', secure: true}
                    options = {path: '/', sameSite: 'lax', secure: true, maxAge: 31557600}
                    parent.cookies.set('lobby', newID, options)
                    parent.props.setInitLobby(1);
                    parent.setState({inLobby: 1})
                    parent.props.setComponent("lobby")
                    console.log(parent.state)
                }
                //CASE 3: Response from leaving lobby
                else if (jsonResponse.hasOwnProperty("lobbyLeaving")) {
                    //Add the message to the chat
                    let ul = document.getElementById("chatList")
                    let li = document.createElement("ul");
                    li.appendChild(document.createTextNode(jsonResponse.lobbyLeaving))
                    ul.appendChild(li);
                    if (parent.cookies.get('lobby') !== undefined) {
                        parent.cookies.remove('lobby')
                        console.log("Deleted lobby cookie.")
                    }

                    parent.setState({currentLobby: -1})
                    console.log("Left the lobby.")
                    parent.setState({inLobby: 0})
                    parent.props.setComponent("lobbySelect")
                    parent.closeSocket();
                }
                //CASE 3.5: Response from leaving lobby (bugfix)
                else if (jsonResponse.hasOwnProperty("lobbyLeaving")) {
                    if (parent.cookies.get('lobby') !== undefined) {
                        parent.cookies.remove('lobby')
                        console.log("Deleted lobby cookie.")
                    }

                    parent.setState({currentLobby: -1})
                    console.log("Left the lobby.")
                    parent.setState({inLobby: 0})
                    parent.props.setComponent("lobbySelect")
                    parent.closeSocket();
                }
                //CASE 4: Refresh lobby
                else if (jsonResponse.hasOwnProperty("refreshLobby")) {
                    parent.getLobby();
                }
                //CASE 5: Response from sending CNF (other users have not yet submitted)
                else if (jsonResponse.hasOwnProperty("cnfSuccess")) {
                    //Add the message to the chat
                    let ul = document.getElementById("chatList")
                    let li = document.createElement("ul");
                    li.appendChild(document.createTextNode(jsonResponse.cnfSuccess));
                    ul.appendChild(li);
                    //TODO Might need to uncomment this if refreshing doesnt work properly
                    //  shouldn't though, server asks EVERYONE for refresh after :)
                    //parent.getLobby();
                } else if (jsonResponse.hasOwnProperty("cnfFailure")) {
                    //Add the message to the chat
                    let ul = document.getElementById("chatList")
                    let li = document.createElement("ul");
                    li.appendChild(document.createTextNode(jsonResponse.cnfFailure))
                    ul.appendChild(li);
                    parent.getLobby();
                }
                //CASE 6: UP1
                else if (jsonResponse.hasOwnProperty("up1")) {
                    //parent.props.toggleCanvas();
                    //parent.props.renderTree();
                    //Add the message to the chat
                    let ul = document.getElementById("chatList")
                    let li = document.createElement("ul");
                    li.appendChild(document.createTextNode("UP1, CNF is " + jsonResponse.cnf))
                    ul.appendChild(li);
                    console.log("UP1, CNF is " + jsonResponse.cnf)
                    //Now we set our state values, before changing the component.
                    parent.setState({
                            cnf: jsonResponse.cnf[0],
                            headingPhase: "up1",
                        }, parent.props.setComponent("up1")
                    )
                }
                //CASE 7: UP2
                else if (jsonResponse.hasOwnProperty("up2")) {
                    //Add the message to the chat
                    let ul = document.getElementById("chatList")
                    let li = document.createElement("ul");
                    li.appendChild(document.createTextNode("UP2, CNF is " + jsonResponse.cnf))
                    ul.appendChild(li);

                    let assignments = [];
                    for (let i = 0; i < jsonResponse.selections[0].length; i++) {
                        assignments.push(false)
                    }
                    console.log(assignments);

                    //Now we set our state values, before changing the component.
                    parent.setState({
                        cnf: jsonResponse.cnf[0],
                        headingPhase: "up2",
                        selectionsTrue: jsonResponse.selections[0],
                        assignments: assignments
                    }, parent.props.setComponent("up2"))
                }
                //CASE 8: UP3
                else if (jsonResponse.hasOwnProperty("up3")) {
                    //Add the message to the chat
                    let ul = document.getElementById("chatList")
                    let li = document.createElement("ul");
                    li.appendChild(document.createTextNode("UP3, CNF is " + jsonResponse.cnf))
                    ul.appendChild(li);

                    //Now we set our state values, before changing the component.
                    parent.setState({
                        cnf: jsonResponse.cnf[0],
                        headingPhase: "up3",
                    }, parent.props.setComponent("up3"))
                }
                //CASE 9: random1
                else if (jsonResponse.hasOwnProperty("random1")) {
                    //Add the message to the chat
                    let ul = document.getElementById("chatList")
                    let li = document.createElement("ul");
                    li.appendChild(document.createTextNode("Random 1, CNF is " + jsonResponse.cnf))
                    ul.appendChild(li);
                    console.log(jsonResponse.cnf[0])
                    console.log(jsonResponse.randomOutput1[0])
                    //Now we set our state values, before changing the component.
                    parent.setState({
                        cnf: jsonResponse.cnf[0],
                        headingPhase: "random1",
                        randomOutput1: jsonResponse.randomOutput1[0],
                    }, parent.props.setComponent("random1"))
                }
                //CASE 10: UP4
                else if (jsonResponse.hasOwnProperty("up4")) {
                    //Add the message to the chat
                    let ul = document.getElementById("chatList")
                    let li = document.createElement("ul");
                    li.appendChild(document.createTextNode("UP4, CNF is " + jsonResponse.cnf))
                    ul.appendChild(li);

                    //Now we set our state values, before changing the component.
                    parent.setState({
                        threeString: jsonResponse.threeString[0],
                        cnf: jsonResponse.cnf[0],
                        headingPhase: "up4",
                    }, parent.props.setComponent("up4"))
                }
                //CASE 11: random2
                else if (jsonResponse.hasOwnProperty("random2")) {
                    //Add the message to the chat
                    let ul = document.getElementById("chatList")
                    let li = document.createElement("ul");
                    li.appendChild(document.createTextNode("Random 2, CNF is " + jsonResponse.cnf))
                    ul.appendChild(li);

                    //Now we set our state values, before changing the component.
                    parent.setState({
                        cnf: jsonResponse.cnf[0],
                        randomOutput2: jsonResponse.randomOutput2[0],
                        headingPhase: "random1",
                    }, parent.props.setComponent("random2"))
                }
                //CASE 12: PLE1
                else if (jsonResponse.hasOwnProperty("ple1")) {
                    //Add the message to the chat
                    //parent.props.toggleCanvas();
                    parent.props.renderTree();
                    let ul = document.getElementById("chatList")
                    let li = document.createElement("ul");
                    li.appendChild(document.createTextNode("PLE1, CNF is " + jsonResponse.cnf))
                    ul.appendChild(li);

                    //Now we set our state values, before changing the component.
                    parent.setState({
                        cnf: jsonResponse.cnf[0],
                        headingPhase: "ple1",
                    }, parent.props.setComponent("ple1"))
                }
                //CASE 13: PLE2
                else if (jsonResponse.hasOwnProperty("ple2")) {
                    console.log(this.props)
                    //Add the message to the chat
                    let ul = document.getElementById("chatList")
                    let li = document.createElement("ul");
                    li.appendChild(document.createTextNode("ple2, CNF is " + jsonResponse.cnf))
                    ul.appendChild(li);

                    //Now we set our state values, before changing the component.
                    parent.setState({
                        cnf: jsonResponse.cnf[0],
                        headingPhase: "ple2",
                    }, parent.props.setComponent("ple2"))
                    console.log(this.props)
                }
                //CASE 14: random3
                else if (jsonResponse.hasOwnProperty("random3")) {
                    //Add the message to the chat
                    let ul = document.getElementById("chatList")
                    let li = document.createElement("ul");
                    li.appendChild(document.createTextNode("Random 3, CNF is " + jsonResponse.cnf))
                    ul.appendChild(li);

                    //Now we set our state values, before changing the component.
                    parent.setState({
                        cnf: jsonResponse.cnf[0],
                        randomOutput3: jsonResponse.randomOutput3[0],
                        headingPhase: "random1",
                    }, parent.props.setComponent("random3"))
                }
                //CASE 15: branching (consensus)
                else if (jsonResponse.hasOwnProperty("branchSelect")) {
                    console.log("Received request to go branching.")
                    //Add the message to the chat
                    //parent.props.toggleCanvas();
                    parent.props.renderTree();
                    let ul = document.getElementById("chatList")
                    let li = document.createElement("ul");
                    li.appendChild(document.createTextNode("Branching decision, base is CNF " + jsonResponse.cnf))
                    ul.appendChild(li);
                    //Now we set our state values, before changing the component.
                    parent.setState({
                        cnf: jsonResponse.cnf[0],
                    }, parent.props.setComponent("branchSelect"))
                }
                //CASE 16: branch1
                else if (jsonResponse.hasOwnProperty("branch1")) {
                    if (jsonResponse.hasOwnProperty("backtrack")) {
                        parent.props.handleBacktrack();
                        let ul = document.getElementById("chatList")
                        let li = document.createElement("ul");
                        li.appendChild(document.createTextNode("Backtracking to string " + jsonResponse.cnf))
                        ul.appendChild(li);
                    } else {
                        parent.props.handleBranch(jsonResponse.brancher[0])
                    }
                    //Add the message to the chat
                    let ul = document.getElementById("chatList")
                    let li = document.createElement("ul");
                    li.appendChild(document.createTextNode("branch1, CNF is " + jsonResponse.cnf))
                    ul.appendChild(li);
                    parent.setState({
                        cnf: jsonResponse.cnf[0],
                        brancher: jsonResponse.brancher[0],
                        headingPhase: "branch1",
                    }, parent.props.setComponent("branch1"))
                }
                //CASE 17: branchrand1
                else if (jsonResponse.hasOwnProperty("branchrand1")) {
                    //Add the message to the chat
                    let ul = document.getElementById("chatList")
                    let li = document.createElement("ul");
                    li.appendChild(document.createTextNode("Branch random 1, CNF is " + jsonResponse.cnf))
                    ul.appendChild(li);
                    //Now we set our state values, before changing the component.
                    parent.setState({
                        cnf: jsonResponse.cnf[0],
                        brancher: jsonResponse.brancher[0],
                        randomOutput4: jsonResponse.randomOutput4[0],
                        headingPhase: "random1",
                    }, parent.props.setComponent("branchrand1"))
                }
                //CASE 18: branch2
                else if (jsonResponse.hasOwnProperty("branch2")) {
                    //Add the message to the chat
                    let ul = document.getElementById("chatList")
                    let li = document.createElement("ul");
                    li.appendChild(document.createTextNode("branch2, CNF is " + jsonResponse.cnf))
                    ul.appendChild(li);

                    //Now we set our state values, before changing the component.
                    parent.setState({
                        branch1string: jsonResponse.branch1string[0],
                        brancher: jsonResponse.brancher[0],
                        cnf: jsonResponse.cnf[0],
                        headingPhase: "branch2",
                    }, parent.props.setComponent("branch2"))
                }
                //CASE 19: branchrand2
                else if (jsonResponse.hasOwnProperty("branchrand2")) {
                    //Add the message to the chat
                    let ul = document.getElementById("chatList")
                    let li = document.createElement("ul");
                    li.appendChild(document.createTextNode("Branch random 2, CNF is " + jsonResponse.cnf))
                    ul.appendChild(li);

                    //Now we set our state values, before changing the component.
                    parent.setState({
                        cnf: jsonResponse.cnf[0],
                        randomOutput5: jsonResponse.randomOutput5[0],
                        headingPhase: "random1",
                    }, parent.props.setComponent("branchrand2"))
                }
                //CASE 20: failure cases
                else if (jsonResponse.hasOwnProperty("lose1")) {
                    //Add the message to the chat
                    let ul = document.getElementById("chatList")
                    let li = document.createElement("ul");
                    li.appendChild(document.createTextNode("Incorrect answer!"))
                    ul.appendChild(li);

                    parent.setState({
                        headingPhase: "lose1",
                    })
                } else if (jsonResponse.hasOwnProperty("lose2")) {
                    //Add the message to the chat
                    let ul = document.getElementById("chatList")
                    let li = document.createElement("ul");
                    li.appendChild(document.createTextNode("Incorrect answer!"))
                    ul.appendChild(li);

                    parent.setState({
                        headingPhase: "lose2",
                    })
                } else if (jsonResponse.hasOwnProperty("lose3")) {
                    //Add the message to the chat
                    let ul = document.getElementById("chatList")
                    let li = document.createElement("ul");
                    li.appendChild(document.createTextNode("Incorrect answer!"))
                    ul.appendChild(li);

                    parent.setState({
                        headingPhase: "lose3",
                    })
                } else if (jsonResponse.hasOwnProperty("lose4")) {
                    //Add the message to the chat
                    let ul = document.getElementById("chatList")
                    let li = document.createElement("ul");
                    li.appendChild(document.createTextNode("Incorrect answer!"))
                    ul.appendChild(li);

                    parent.setState({
                        headingPhase: "lose4",
                    })
                } else if (jsonResponse.hasOwnProperty("loserandom1")) {
                    //Add the message to the chat
                    let ul = document.getElementById("chatList")
                    let li = document.createElement("ul");
                    li.appendChild(document.createTextNode("Incorrect answer!"))
                    ul.appendChild(li);

                    parent.setState({
                        headingPhase: "loserandom1",
                    })
                } else if (jsonResponse.hasOwnProperty("loseple1")) {
                    //Add the message to the chat
                    let ul = document.getElementById("chatList")
                    let li = document.createElement("ul");
                    li.appendChild(document.createTextNode("Incorrect answer!"))
                    ul.appendChild(li);

                    parent.setState({
                        headingPhase: "loseple1",
                    })
                } else if (jsonResponse.hasOwnProperty("loseple2")) {
                    //Add the message to the chat
                    let ul = document.getElementById("chatList")
                    let li = document.createElement("ul");
                    li.appendChild(document.createTextNode("Incorrect answer!"))
                    ul.appendChild(li);

                    parent.setState({
                        headingPhase: "loseple2",
                    })
                } else if (jsonResponse.hasOwnProperty("losebranch1")) {
                    //Add the message to the chat
                    let ul = document.getElementById("chatList")
                    let li = document.createElement("ul");
                    li.appendChild(document.createTextNode("Incorrect answer!"))
                    ul.appendChild(li);

                    parent.setState({
                        headingPhase: "losebranch1",
                    })
                } else if (jsonResponse.hasOwnProperty("losebranch2")) {
                    //Add the message to the chat
                    let ul = document.getElementById("chatList")
                    let li = document.createElement("ul");
                    li.appendChild(document.createTextNode("Incorrect answer!"))
                    ul.appendChild(li);

                    parent.setState({
                        headingPhase: "losebranch2",
                    })
                }
                //CASE 21: victory/defeat (unique to each user)
                else if (jsonResponse.hasOwnProperty("ending")) {
                    //Add the message to the chat
                    let ul = document.getElementById("chatList")
                    let li = document.createElement("ul");
                    li.appendChild(document.createTextNode("Endings, ending is " + jsonResponse.ending[0]))
                    ul.appendChild(li);

                    parent.setState({
                        currentPoints: jsonResponse.currentPoints[0],
                        rawPoints: jsonResponse.rawPoints[0],
                        backtracks: jsonResponse.backtracks[0],
                        ending: jsonResponse.ending[0],
                        wrongAnswers: jsonResponse.wrongAnswers[0]

                    }, parent.props.setComponent("ending"))
                } else if (jsonResponse.hasOwnProperty("startCanvas")) {
                    //start da canvas
                    parent.props.toggleCanvas();
                }
            }
            console.log("Socket established.")
        }
    }

    up1(cnf, index, selections){
        //send CNF, temp to server
        //also send orderedSelections so that we can send them if correct
        //make sure to order the literals using orderLiterals before phase 2.
        if (this.socket === null) {
            console.log("Socket is null.")
        } else {
            console.log(this.props.userid)
            let data = JSON.stringify({
                'up1' : "",
                'cnf' : cnf,
                'userid': this.props.userid,
                'lobbyid' : this.state.currentLobby,
                'index': index,
                'selections' : selections
            });
            console.log("Websocket sending message: " + data);
            this.socket.send(data);
        }
    }
    up2(cnf, assignments){
        //send CNF, temp to server
        //also send orderedSelections so that we can send them if correct
        //make sure to order the literals using orderLiterals before phase 2.
        if (this.socket === null) {
            console.log("Socket is null.")
        } else {
            let data = JSON.stringify({
                'up2' : "",
                'cnf' : cnf,
                'userid': this.props.userid,
                'lobbyid' : this.state.currentLobby,
                'assignments': assignments,
            });
            console.log("Websocket sending message: " + data);
            this.socket.send(data);
        }
    }
    up3(cnf, index){
        //send CNF, temp to server
        //also send orderedSelections so that we can send them if correct
        //make sure to order the literals using orderLiterals before phase 2.
        if (this.socket === null) {
            console.log("Socket is null.")
        } else {
            let data = JSON.stringify({
                'up3' : "",
                'cnf' : cnf,
                'userid': this.props.userid,
                'lobbyid' : this.state.currentLobby,
                'index': index,
            });
            console.log("Websocket sending message: " + data);
            this.socket.send(data);
        }
    }
    random1(threeChoice){
        if (this.socket === null) {
            console.log("Socket is null.")
        } else {
            let data = JSON.stringify({
                'random1' : "",
                'threeChoice' : threeChoice,
                'cnf' : this.state.cnf,
                'userid': this.props.userid,
                'lobbyid' : this.state.currentLobby,
            });
            console.log("Websocket sending message: " + data);
            this.socket.send(data);
        }
    }
    up4(cnf, threeString, index){
        //send CNF, temp to server
        //also send orderedSelections so that we can send them if correct
        //make sure to order the literals using orderLiterals before phase 2.
        if (this.socket === null) {
            console.log("Socket is null.")
        } else {
            let data = JSON.stringify({
                'up4' : "",
                'cnf' : cnf,
                'newCNF' : threeString,
                'userid': this.props.userid,
                'lobbyid' : this.state.currentLobby,
                'index': index,
            });
            console.log("Websocket sending message: " + data);
            this.socket.send(data);
        }
    }
    random2(fourChoice){
        if (this.socket === null) {
            console.log("Socket is null.")
        } else {
            let data = JSON.stringify({
                'random2' : "",
                'cnf' : this.state.cnf,
                'fourChoice' : fourChoice,
                'userid': this.props.userid,
                'lobbyid' : this.state.currentLobby,
            });
            console.log("Websocket sending message: " + data);
            this.socket.send(data);
        }
    }


    ple1(cnf, index){
        console.log(this.props)
        if (this.socket === null) {
            console.log("Socket is null.")
        } else {
            let data = JSON.stringify({
                'ple1' : "",
                'cnf' : cnf,
                'userid': this.props.userid,
                'lobbyid' : this.state.currentLobby,
                'index': index,
            });
            console.log("Websocket sending message: " + data);
            this.socket.send(data);
        }
        console.log(this.props)
    }
    ple2(cnf, index){
        console.log(this.props)
        if (this.socket === null) {
            console.log("Socket is null.")
        } else {
            let data = JSON.stringify({
                'ple2' : "",
                'cnf' : cnf,
                'userid': this.props.userid,
                'lobbyid' : this.state.currentLobby,
                'index': index,
            });
            console.log("Websocket sending message: " + data);
            this.socket.send(data);
        }
    }
    random3(pleChoice){
        if (this.socket === null) {
            console.log("Socket is null.")
        } else {
            let data = JSON.stringify({
                'random3' : "",
                'cnf' : this.state.cnf,
                'pleChoice' : pleChoice,
                'userid': this.props.userid,
                'lobbyid' : this.state.currentLobby,
            });
            console.log("Websocket sending message: " + data);
            this.socket.send(data);
        }
    }

    branchSelect(cnf, variable, assignment){
        //This one's a bit tricky.
        //We must send a variable and an assignment.
        if (this.socket === null) {
            console.log("Socket is null.")
        } else {
            let data = JSON.stringify({
                'branchSelect' : "",
                'cnf' : cnf,
                'userid': this.props.userid,
                'lobbyid' : this.state.currentLobby,
                'variable': variable,
                'assignment': assignment
            });
            console.log("Websocket sending message: " + data);
            this.socket.send(data);
        }
    }
    branch1(cnf, brancher, index){
        if (this.socket === null) {
            console.log("Socket is null.")
        } else {
            let data = JSON.stringify({
                'branch1' : "",
                'cnf' : cnf,
                'userid': this.props.userid,
                'lobbyid' : this.state.currentLobby,
                'brancher': brancher,
                'index': index
            });
            console.log("Websocket sending message: " + data);
            this.socket.send(data);
        }
    }
    branchrand1(branch1choice, brancher){
        if (this.socket === null) {
            console.log("Socket is null.")
        } else {
            let data = JSON.stringify({
                'branchrand1' : "",
                'cnf' : this.state.cnf,
                'branch1choice': branch1choice,
                'userid': this.props.userid,
                'brancher': brancher,
                'lobbyid' : this.state.currentLobby,
            });
            console.log("Websocket sending message: " + data);
            this.socket.send(data);
        }
    }
    branch2(cnf, brancher, branch1string, index){
        if (this.socket === null) {
            console.log("Socket is null.")
        } else {
            let data = JSON.stringify({
                'branch2' : "",
                'cnf' : cnf,
                'userid': this.props.userid,
                'lobbyid' : this.state.currentLobby,
                'brancher': brancher,
                'newCNF': branch1string,
                'index': index
            });
            console.log("Websocket sending message: " + data);
            this.socket.send(data);
        }
    }
    branchrand2(branch2choice, brancher){
        if (this.socket === null) {
            console.log("Socket is null.")
        } else {
            let data = JSON.stringify({
                'branchrand2' : "",
                'cnf' : this.state.branch1string,
                'branch2choice': branch2choice,
                'userid': this.props.userid,
                'lobbyid' : this.state.currentLobby,
            });
            console.log("Websocket sending message: " + data);
            this.socket.send(data);
        }
    }

    hi(){
        if (this.socket === null) {
            console.log("Socket is null.")
        } else {
            let data = JSON.stringify({
                'username' : this.props.username,
                'userid': this.props.userid,
            });
            console.log("Websocket sending userid: " + data);
            this.socket.send(data);
        }
    }
    send(message){
        if (this.socket === null) {
            console.log("Socket is null.")
        } else {
            let data = JSON.stringify({
                'username' : this.props.username,
                'userid': this.props.userid,
                'lobbyid': this.state.currentLobby,
                'message': message
            });
            console.log("Websocket sending message: " + data);
            this.socket.send(data);
        }
    }
    toggleUpdate(){
        if(this.state.update === 0){
            this.setState({update: 1})
        } else this.setState({update: 0})
        console.log("toggled update")
    }

    deleteCookie(){
        this.cookies.remove("lobby")
    }

    async handleLobby(lobbyId){
        let name = (this.props.username+"'s lobby");
        if (this.socket === null) {
            console.log("Socket is null.")
        }
        else if (this.state.inLobby !== 0) {
            console.log("Already in a lobby.")
        } else {
            let data = JSON.stringify({
                'initialise': 1,
                'username' : this.props.username,
                'userid': this.props.userid,
                'lobbyid': lobbyId,
                'name': name
            });
            console.log("Websocket sending message: " + data);
            this.socket.send(data);
        }
    }

    closeSocket(){
        if (this.socket !== null) {
            this.socket.close();
        }
        this.socket = null;
        console.log("Websocket is in disconnected state");
        //this.state.socket.close()
        //this.setState({socket: null})
    }

    leaveLobby(){
        console.log(this.state)
        console.log(this.state.currentLobby)
        if (this.socket === null) {
            console.log("Socket is null.")
        } else {
            let data = JSON.stringify({
                'leavingLobby': 1,
                'userid': this.props.userid,
                'lobbyid': this.state.currentLobby,
            });
            console.log("Websocket sending message: " + data);
            this.socket.send(data);
        }
    }

    leaveLobby2(){
        console.log(this.state)
        console.log(this.state.currentLobby)
        if (this.socket === null) {
            console.log("Socket is null.")
        } else {
            let data = JSON.stringify({
                'leavingLobby2': 1,
                'userid': this.props.userid,
                'lobbyid': this.state.currentLobby,
            });
            console.log("Websocket sending message: " + data);
            this.socket.send(data);
        }
    }

    submitCNF(cnf){
        if (arguments.length < 1) {
            if (this.socket === null) {
                console.log("Socket is null.")
            } else {
                let data = JSON.stringify({
                    'submitCNF': 1,
                    'random': 1,
                    'userid': this.props.userid,
                    'lobbyid': this.state.currentLobby,
                });
                console.log("Websocket sending message: " + data);
                this.socket.send(data);
            }
        } else {
            if (this.socket === null) {
                console.log("Socket is null.")
            } else {
                let data = JSON.stringify({
                    'submitCNF': 1,
                    'cnf': cnf,
                    'userid': this.props.userid,
                    'lobbyid': this.state.currentLobby,
                });
                console.log("Websocket sending message: " + data);
                this.socket.send(data);
            }
        }

    }

    getComponent(){
        let retComponent;
        switch (this.props.component){
            default :
                retComponent = <LobbySelect setComponent = {this.props.setComponent} headingPhase={this.state.headingPhase} socket = {this.socket} handleLobby = {this.handleLobby} openSocket = {this.openSocket}></LobbySelect>
                break;
            case "lobbySelect" :
                retComponent = <LobbySelect setComponent = {this.props.setComponent} headingPhase={this.state.headingPhase} socket = {this.socket} handleLobby = {this.handleLobby} openSocket = {this.openSocket}></LobbySelect>
                break;
            case "lobby" :
                retComponent = <Lobby result ={this.state.result} getLobby ={this.getLobby} headingPhase={this.state.headingPhase} update = {this.state.update} leaveLobby = {this.leaveLobby} submitCNF = {this.submitCNF} userid = {this.props.userid} setComponent = {this.props.setComponent} socket = {this.socket} lobby = {this.state.currentLobby} ></Lobby>
                break;
            case "up1" :
                retComponent = <UP1 up1 = {this.up1} cnf={this.state.cnf} headingPhase={this.state.headingPhase} phase={"up1"}></UP1>
                break;
            case "up2" :
                retComponent = <UP2 up2 = {this.up2} cnf={this.state.cnf} headingPhase={this.state.headingPhase} assignments={this.state.assignments} phase={"up2"} selectionsTrue={this.state.selectionsTrue}></UP2>
                break;
            case "up3" :
                retComponent = <UP3 up3 = {this.up3} cnf={this.state.cnf} headingPhase={this.state.headingPhase} phase={"up3"}></UP3>
                break;
            case "random1" :
                retComponent = <Random1 random1 = {this.random1} cnf={this.state.cnf} headingPhase={this.state.headingPhase} phase={"random1"} randomOutput1={this.state.randomOutput1}></Random1>
                break;
            case "up4" :
                retComponent = <UP4 up4 = {this.up4} cnf={this.state.cnf} headingPhase={this.state.headingPhase} threeString={this.state.threeString} phase={"up4"}></UP4>
                break;
            case "random2" :
                retComponent = <Random2 random2 = {this.random2} randomOutput2={this.state.randomOutput2} headingPhase={this.state.headingPhase} cnf={this.state.cnf} phase={"random2"}></Random2>
                break;
            case "ple1" :
                retComponent = <PLE1 ple1 = {this.ple1} cnf={this.state.cnf} headingPhase={this.state.headingPhase} phase={"ple1"}></PLE1>
                break;
            case "ple2" :
                retComponent = <PLE2 ple2 = {this.ple2} cnf={this.state.cnf} headingPhase={this.state.headingPhase} phase={"ple2"}></PLE2>
                break;
            case "random3" :
                retComponent = <Random3 random3 = {this.random3} randomOutput3={this.state.randomOutput3} headingPhase={this.state.headingPhase} cnf={this.state.cnf} phase={"random3"}></Random3>
                break;
            case "branchSelect" :
                retComponent = <VariableBranchMultiplayer result={this.state.result} getLobby={this.getLobby} cnf={this.state.cnf} branchSelect={this.branchSelect}></VariableBranchMultiplayer>
                break;
            case "branch1" :
                retComponent = <Branch1 branch1 = {this.branch1} brancher = {this.state.brancher} cnf={this.state.cnf} headingPhase={this.state.headingPhase} phase={"branch1"}></Branch1>
                break;
            case "branchrand1" :
                retComponent = <BranchRand1 branchrand1 = {this.branchrand1} brancher = {this.state.brancher} headingPhase={this.state.headingPhase} randomOutput4={this.state.randomOutput4} cnf={this.state.cnf} phase={"branchrand1"}></BranchRand1>
                break;
            case "branch2" :
                retComponent = <Branch2 branch2 = {this.branch2} brancher = {this.state.brancher} cnf={this.state.cnf} headingPhase={this.state.headingPhase} branch1string = {this.state.branch1string} phase={"branch2"}></Branch2>
                break;
            case "branchrand2" :
                retComponent = <BranchRand2 branchrand2 = {this.branchrand2} randomOutput5={this.state.randomOutput5} cnf={this.state.cnf} branch1string = {this.state.branch1string} headingPhase={this.state.headingPhase} phase={"branchrand2"}></BranchRand2>
                break;
            case "ending" :
                retComponent = <EndingsMultiplayer sendUserData = {this.props.sendUserData} ending={this.state.ending} currentPoints={this.state.currentPoints} rawPoints={this.state.rawPoints} backtracks={this.state.backtracks} wrongAnswers={this.state.wrongAnswers}></EndingsMultiplayer>
                break;


        }
        return retComponent;
    }

    lobbyChat(){
        if(this.state.inLobby === 1){
            return(
                <div className={"chatWrapperWrapper"}>
                    <h3>Lobby Chat / Log</h3>
                    <TextField
                        className={"inputBox"}
                        id={"entryField"}
                        name={"myCNF"}
                        value={this.state.value} label='Say something'
                        onChange={(e) => {this.setState({value : e.target.value})}}
                    />
                    <button className={"chatButton"} onClick={() => this.send(this.state.value)}>SEND</button>
                </div>
            )
        } else {
            return null;
        }
    }

    render(){
        return(
            <div className={"mpWrapper"}>
                <h2>Multiplayer</h2>
                <div>
                    {this.getComponent()}
                </div>
                <div className={"chatWrapper"}>
                    <div className={"chatBox"}>{this.lobbyChat()}</div>
                    <div className={"chatList"} id={"chatList"}></div>
                </div>
            </div>
        )
        /*
                <p>Returned value: {this.state.response}</p>
                <p>Socket: {this.state.url}</p>
                <div>
                    <button onClick={() => this.openSocket()}>OPEN SOCKET</button>
                    <button onClick={() => this.hi()}>SAY HI</button>
                    <button onClick={() => this.closeSocket()}>CLOSE SOCKET</button>
                    <button onClick={() => this.deleteCookie()}>DELETE COOKIE</button>
                    <button onClick={() => this.forceUpdate()}>FORCED UPDATE</button>
                </div>

                */
    }
}

export default MultiplayerWrapper