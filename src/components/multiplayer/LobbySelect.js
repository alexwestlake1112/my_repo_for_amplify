import React from 'react'
import axios from "axios";
import LobbyWidget from "./LobbyWidget";
import Cookies from "universal-cookie";
class LobbySelect extends React.Component {
    constructor(props) {
        super(props);
        this.cookies = new Cookies();
        this.state = {
            result : [],
            loggedIn: 0
        }
        this.getLobbies = this.getLobbies.bind(this);
        this.joinLobby = this.joinLobby.bind(this);
        this.createLobby = this.createLobby.bind(this);
        this.listThing = this.listThing.bind(this);
        this.getComponent = this.getComponent.bind(this);
    }

    componentDidMount() {
        let token = this.cookies.get('token');
        if(token !== undefined){
            this.setState({loggedIn : 1})
            this.getLobbies();
        }
    }

    async createLobby(){
        await this.props.openSocket()
        this.props.socket.onopen = () => this.props.handleLobby(-1)
        /*
        this.props.openSocket().then(() => {
            this.props.handleLobby(-1, "Epic lobby")
        });*/
        //Next we need to send our data to the server such that we create a user object and a new lobby.
    }

    async joinLobby(lobbyid) {
        await this.props.openSocket()
        this.props.socket.onopen = () => this.props.handleLobby(lobbyid)
        /*
        this.props.openSocket().then(() => {
            this.props.handleLobby(lobbyid);
        });*/
    }

    getLobbies(){
        axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/getLobbies").then(r => {
            console.log(r.data);
            this.setState({result : r.data})
        })
    }

    listThing(data){
        if (data.length !== 0){
            return data.map((d) => <LobbyWidget joinLobby = {this.joinLobby} lobbyId ={d.lobbyId} name = {d.lobbyName}
                                                users={d.lobbyUsers}>
            </LobbyWidget>)
        } else return <h3>No lobbies found.</h3>

    }

    getComponent(){
        if(this.state.loggedIn === 1){
            return(
                <div>
                    <div>{this.listThing(this.state.result)}</div>
                    <button onClick={this.createLobby}>Create new lobby</button>
                    <button onClick={this.getLobbies}>Refresh list</button>
                </div>
            )
        }
        else return <p>Please log in or register to access the Multiplayer lobbies!</p>
        //here we want to either prompt user to log in, or list their unbought items.
    }

    render() {
        return(
            <div>
                <div>
                    {this.getComponent()}
                </div>
            </div>
        )
    }

}

export default LobbySelect