import React from 'react'
import axios from "axios";
import Cookies from "universal-cookie";
import {TextField} from "@mui/material";
class Lobby extends React.Component {
    constructor(props) {
        super(props);
        this.cookies = new Cookies();
        this.state = {
            value : "",
            result : undefined,
            loggedIn : 0,
            update : this.props.update,
            difficulty : 0
        }
        //this.getLobby = this.getLobby.bind(this);
        this.lobbyConstruct = this.lobbyConstruct.bind(this);
        this.listThing = this.listThing.bind(this);
        this.getComponent = this.getComponent.bind(this);
        this.emptyCNF = this.emptyCNF.bind(this);
        this.showCNFButton = this.showCNFButton.bind(this);
        this.isThisMe = this.isThisMe.bind(this);
        this.stealCNF = this.stealCNF.bind(this);
        this.parseInput = this.parseInput.bind(this);
        this.filterInput = this.filterInput.bind(this);
        this.literalDupes = this.literalDupes.bind(this);
        this.clauseDupes = this.clauseDupes.bind(this);
    }

    componentDidMount() {
        let token = this.cookies.get('token');
        if(token !== undefined){
            this.setState({loggedIn : 1})
            this.props.getLobby();
        }
        //this.props.getLobby();
    }

    parseInput(input){
        //Remove whitespace
        input = input.replaceAll(/ /g,'');

        //Remove brackets
        input = input.replaceAll("(", "");
        input = input.replaceAll(")", "");

        //Remove double negatives
        input = input.replaceAll("¬¬","");

        console.log(input);

        //Split the formula into clauses and set
        return input.split("∧");
    }

    literalDupes(clause){
        let literals = clause.split("∨");
        console.log(clause);
        console.log(literals);
        let a = "";
        let dupefinder = [];
        for(let l of literals){
            if(!dupefinder.includes(l)){
                a = a+l+"∨";
                dupefinder.push(l);
            }
        }
        console.log(a)
        return a.slice(0, -1);
    }

    clauseDupes(clauses){
        console.log(clauses);
        let nonDupes = [];
        for(let c of clauses){
            if (!(nonDupes.includes(c))){
                nonDupes.push(c);
            }
        }
        console.log(nonDupes)
        return nonDupes;
    }

    filterInput(str){
        let b = str.replace(/[^a-z¬∧∨]/gi, '');
        let clauses = this.parseInput(b);
        clauses = this.clauseDupes(clauses);
        let a = "";
        for(let c of clauses){
            console.log(c)
            c = this.literalDupes(c);
            c = "("+c+")";
            a = a+c+"∧";
        }
        let d = a.slice(0, -1)
        this.setState({value : d});
    }



    /*getLobby(){
        console.log("Attempting to get lobby of ID "+this.props.lobby)
        axios.get("//localhost:8080/api/getLobby?lobbyid="+ this.props.lobby).then(r => {
            console.log(r.data);
            this.setState({result : r.data})
        })
    }*/

    listThing(users){
        console.log(users)
        console.log("users: "+users)
        if (users !== undefined){
            if (users.length !== 0){
                return users.map((u) => (
                    <div className={"itemWidget"}>
                        <h3>User {u.userid}: {u.username}{this.isThisMe(u.userid)}</h3>
                        <p>Chosen CNF: {this.emptyCNF(u.cnfSelect)}</p>
                        {this.showCNFButton(u.cnfSelect)}
                    </div>
                ))
            }
        }
    }

    stealCNF(cnf){
        if (cnf !== ""){
            this.setState({value : cnf})
        }
    }

    showCNFButton(cnf){
        if (cnf === ""){
            return null
        } else if (cnf === "@"){
            return null
        } else return <button onClick={()=>this.stealCNF(cnf)}>Copy CNF</button>
    }

    isThisMe(userid){
        if (userid === this.props.userid){
            return " (You)"
        } else return ""
    }

    emptyCNF(cnf){
        if (cnf === ""){
            return "Undecided"
        } else if (cnf === "@"){
            return "Random choice"
        } else return cnf
    }

    randomCNF() {
        //First just send my string to the main app, so it can be used in subsequent functions
        let seed = -1
        axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/generateCNF?seed="+seed+"&difficulty="+(this.state.difficulty+1)).then(r3 => {
            let cnf = r3.data;
            this.setState({value : cnf})
        })
    }
    getDifficulty(){
        switch(this.state.difficulty){
            default:
                return "base";
            case 0:
                return "Easy";
            case 1:
                return "Medium";
            case 2:
                return "Hard";
        }
    }
    toggleDifficulty(){
        switch(this.state.difficulty){
            default:
                console.log("hmm")
                break;
            case 0:
                this.setState({difficulty : 1});
                break;
            case 1:
                this.setState({difficulty : 2});
                break;
            case 2:
                this.setState({difficulty : 0});
                break;
        }
    }

    lobbyConstruct(data){
        console.log("Data: "+data)
        if (data !== undefined){
            console.log(data)
            return (
                <div className={"lobbyWrap"}>
                    <div className={"lobbyUserList"}>
                        <div>{this.listThing(data.lobbyUsers)}</div>
                    </div>
                    <div className={"lobbyInput"}>
                        <h2>Lobby {data.lobbyId}: {data.lobbyName}</h2>
                        <div>
                            <h3>Please enter a CNF for the lobby!</h3>
                            <p>CNFs will be chosen by majority vote.</p>
                            <div>
                                <TextField
                                    className={"inputBox"}
                                    id={"entryField"}
                                    name={"myCNF"}
                                    //error={this.state.value.length === 0}
                                    //helperText={!this.state.value.length ? 'CNF is required' : ''}
                                    value={this.state.value} label="Enter your CNF"
                                    onChange={(e) => {this.filterInput(e.target.value)}}
                                />
                                <div>
                                    <button className="myButton" onClick={() => {this.setState({value : (this.state.value + "¬")})}}>Add ¬</button>
                                    <button className="myButton" onClick={() => {this.setState({value : (this.state.value + "∧")})}}>Add ∧</button>
                                    <button className="myButton" onClick={() => {this.setState({value : (this.state.value + "∨")})}}>Add ∨</button>
                                </div>
                                <div>
                                    <button onClick={() => this.props.submitCNF(this.state.value)}>Submit</button>
                                    <button onClick={() => this.randomCNF()}>Generate CNF ({this.getDifficulty()})</button>
                                    <button onClick={() => this.toggleDifficulty()}>Toggle generator difficulty</button>
                                    <button onClick={() => this.props.submitCNF()}>Random CNF</button>
                                </div>
                            </div>
                            <button onClick={this.props.leaveLobby}>Leave lobby</button>
                        </div>
                    </div>
                </div>
            )
        } else return <h3>Something went wrong.</h3>

    }

    getComponent(){
        if(this.state.loggedIn === 1){
            return(
                <div>
                    {this.lobbyConstruct(this.props.result)}
                </div>
            )
        }
        else return <p>log in pls</p>
        //here we want to either prompt user to log in, or list their unbought items.
    }

    render() {
        return(
            <div>
                <div>
                    {this.getComponent()}
                </div>
            </div>
        )
    }

}

export default Lobby