import React from "react";
import IdentifyMultiplayer from "./IdentifyMultiplayer";

class UP1 extends React.Component {
    render() {
        console.log(this.props)
        return(
            <div>
                <h3>Unit Propagation 1</h3>
                <div>
                    <IdentifyMultiplayer headingPhase={this.props.headingPhase} up1 = {this.props.up1} cnf={this.props.cnf} phase={this.props.phase}></IdentifyMultiplayer>
                </div>
            </div>
        )
    }
}

export default UP1;