import React from "react";
import IdentifyMultiplayer from "./IdentifyMultiplayer";

class PLE1 extends React.Component {
    render() {
        return(
            <div>
                <h3>Pure Literal Elimination 1</h3>
                <div>
                    <IdentifyMultiplayer headingPhase={this.props.headingPhase} ple1 = {this.props.ple1} cnf={this.props.cnf} phase={this.props.phase}></IdentifyMultiplayer>
                </div>
            </div>
        )
    }
}

export default PLE1;