import React from "react";
import IdentifyMultiplayer from "./IdentifyMultiplayer";

class PLE2 extends React.Component {
    render() {
        console.log(this.props)
        return(
            <div>
                <h3>Pure Literal Elimination 2</h3>
                <div>
                    <IdentifyMultiplayer headingPhase={this.props.headingPhase} ple2 = {this.props.ple2} cnf={this.props.cnf} phase={this.props.phase}></IdentifyMultiplayer>
                </div>
            </div>
        )
    }
}

export default PLE2;