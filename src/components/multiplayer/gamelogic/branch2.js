import React from "react";
import IdentifyMultiplayer from "./IdentifyMultiplayer";

class Branch2 extends React.Component {
    render() {
        return(
            <div>
                <h3>Branching Simplification 2</h3>
                <div>
                    <IdentifyMultiplayer branch1string = {this.props.branch1string} brancher = {this.props.brancher}headingPhase={this.props.headingPhase} branch2 = {this.props.branch2} cnf={this.props.cnf} phase={this.props.phase}></IdentifyMultiplayer>
                </div>
            </div>
        )
    }
}

export default Branch2;