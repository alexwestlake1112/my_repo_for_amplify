import React from "react";
import IdentifyMultiplayer from "./IdentifyMultiplayer";

class UP2 extends React.Component {
    componentDidMount() {
        console.log(this.props.selectionsTrue)
    }

    render() {
        return(
            <div>
                <h3>Unit Propagation 2</h3>
                <div>
                    <IdentifyMultiplayer headingPhase={this.props.headingPhase} up2 = {this.props.up2} cnf={this.props.cnf} assignments={this.props.assignments} selectionsTrue={this.props.selectionsTrue} phase={this.props.phase}></IdentifyMultiplayer>
                </div>
            </div>
        )
    }
}

export default UP2;