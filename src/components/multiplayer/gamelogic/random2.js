import React from "react";
import IdentifyMultiplayer from "./IdentifyMultiplayer";

class Random2 extends React.Component {
    render() {
        return(
            <div>
                <h3>Unit Propagation 4.5</h3>
                <div>
                    <IdentifyMultiplayer random2 = {this.props.random2} headingPhase={this.props.headingPhase} randomOutput2={this.props.randomOutput2} cnf={this.props.cnf} phase={this.props.phase}></IdentifyMultiplayer>
                </div>
            </div>
        )
    }
}

export default Random2;