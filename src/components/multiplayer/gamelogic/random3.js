import React from "react";
import IdentifyMultiplayer from "./IdentifyMultiplayer";

class Random3 extends React.Component {
    render() {
        return(
            <div>
                <h3>Pure Literal Elimination 2.5</h3>
                <div>
                    <IdentifyMultiplayer random3 = {this.props.random3} headingPhase={this.props.headingPhase} randomOutput3={this.props.randomOutput3} cnf={this.props.cnf} phase={this.props.phase}></IdentifyMultiplayer>
                </div>
            </div>
        )
    }
}

export default Random3;