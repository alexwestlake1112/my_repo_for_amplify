import React from "react";
import IdentifyMultiplayer from "./IdentifyMultiplayer";

class UP4 extends React.Component {
    render() {
        return(
            <div>
                <h3>Unit Propagation 4</h3>
                <div>
                    <IdentifyMultiplayer headingPhase={this.props.headingPhase} up4 = {this.props.up4} cnf={this.props.cnf} threeString={this.props.threeString} phase={this.props.phase}></IdentifyMultiplayer>
                </div>
            </div>
        )
    }
}

export default UP4;