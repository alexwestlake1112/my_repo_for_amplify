import React from "react";
import IdentifyMultiplayer from "./IdentifyMultiplayer";

class BranchRand1 extends React.Component {
    render() {
        return(
            <div>
                <h3>Branching Simplification 1.5</h3>
                <div>
                    <IdentifyMultiplayer brancher = {this.props.brancher} headingPhase={this.props.headingPhase} branchrand1 = {this.props.branchrand1} randomOutput4={this.props.randomOutput4} cnf={this.props.cnf} phase={this.props.phase}></IdentifyMultiplayer>
                </div>
            </div>
        )
    }
}

export default BranchRand1;