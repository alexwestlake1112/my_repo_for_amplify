import React from "react";
import ButtonGroup from '../../ButtonGroup';
import axios from "axios";
import {Button, TextField} from "@mui/material";
import Cookies from "universal-cookie";

class Branch extends React.Component {
    constructor(props) {
        super(props);
        this.cookies = new Cookies();
        this.state = {
            cnf: this.props.cnf,
            clauses: [],
            literals: [],
            variables: [],
            button: "",
            selection: false,
        };
        this.submitChoice = this.submitChoice.bind(this);
        this.setButton = this.setButton.bind(this);
        this.toggle = this.toggle.bind(this);
        this.listThing = this.listThing.bind(this);
        this.lobbyConstruct = this.lobbyConstruct.bind(this);
        this.emptyAssignment = this.emptyAssignment.bind(this);
        this.emptyVariable = this.emptyVariable.bind(this);
        this.isThisMe = this.isThisMe.bind(this);
    }
    parseInput(input){
        //Remove whitespace
        input = input.replaceAll(/ /g,'');

        //Remove brackets
        input = input.replaceAll("(", "");
        input = input.replaceAll(")", "");

        //Remove double negatives
        input = input.replaceAll("¬¬","");

        console.log(input);

        //Split the formula into clauses and set
        return input.split("∧");
    }

    getLiterals(clauses){
        let literals = [];
        for(const clause of clauses){
            const lits = clause.split("∨");
            for(const lit of lits){
                if (!literals.includes(lit)){
                    literals.push(lit);
                }
            }
        }
        return literals;
    }

    getVariables(literals){
        let variables = [];
        for(const l of literals){
            const temp = l.split("¬");
            variables.push(temp[temp.length-1]);
        }
        return variables.reduce(function (a, b) {
            if (a.indexOf(b) < 0) a.push(b);
            return a;
        }, []);

    }
    setButton(buttonSel){
        let state = this.state;
        state.button = buttonSel;
        this.setState(state);
    }
    toggle(){
        if(this.state.selection){
            let state = this.state;
            state.selection = false;
            this.setState(state);
        }
        else{
            let state = this.state;
            state.selection = true;
            this.setState(state);
        }
    }

    submitChoice() {
        //this.props.passChoice(this.state.button, this.state.selection);
        //this.props.setChoice();
        //this.props.handleBranch(this.state.button, this.state.selection)
        //send CNF, button, selection to server
        this.props.branchSelect(this.props.cnf, this.state.button, this.state.selection)
    }

    emptyVariable(v){
        console.log(v)
        if (v === ""){
            return "Undecided"
        } else return v
    }
    emptyAssignment(a){
        console.log(a)
        if (a === undefined || a === null){
            return "Undecided"
        } else {
            if(a){
                return "True"
            } else return "False"
        }
    }
    isThisMe(userid){
        if (userid === this.props.userid){
            return " (You)"
        } else return ""
    }

    listThing(users){
        console.log(users)
        console.log("users: "+users)
        if (users !== undefined){
            if (users.length !== 0){
                return users.map((u) => (
                    <div className={"itemWidget"}>
                        <h3>User {u.userid}: {u.username}{this.isThisMe(u.userid)}</h3>
                        <p>Chosen variable: {this.emptyVariable(u.variable)}</p>
                        <p>Chosen assignment: {this.emptyAssignment(JSON.parse(u.assignment))}</p>
                    </div>
                ))
            }
        }
    }
    lobbyConstruct(data){
        console.log("Data: "+data)
        if (data !== undefined){
            console.log(data)
            return (
                <div /*className={"itemWidget"}*/>
                    <div>
                        <h3>Please choose a variable to branch on and its assignment.</h3>
                        <p>The resulting literal will be chosen by majority vote.</p>
                        <h3>Current CNF: {this.state.cnf}</h3>
                        <ButtonGroup buttons={this.state.variables} doSomethingAfterClick={this.setButton}></ButtonGroup>
                        <h3>current variable: {this.state.button}</h3>
                        <button onClick={this.toggle}>Toggle bool</button>
                        <h3>current assignment: {this.state.selection.toString()}</h3>
                        <button onClick={this.submitChoice}>Submit branch choice</button>
                    </div>
                    <div>{this.listThing(data.lobbyUsers)}</div>
                </div>
            )
        } else return <h3>Something went wrong.</h3>

    }


    componentDidMount() {
        let state = this.state;
        state.clauses = this.parseInput(this.state.cnf);
        state.literals = this.getLiterals(state.clauses);
        state.variables = this.getVariables(state.literals);
        state.button = state.variables[0];
        this.setState(state);
        let token = this.cookies.get('token');
        if(token !== undefined){
            this.props.getLobby();
        }
        console.log("THE PROPS")
        console.log(this.props)
    }

    //TODO: Display other users' choices.
    render() {
        return(
            <div>
                <div>
                    <div>
                        {this.lobbyConstruct(this.props.result)}
                    </div>
                </div>
            </div>
        )
    }
}

export default Branch;