import React from "react";
import ButtonGroup from "../../ButtonGroup";
import {Button} from "@mui/material";
import axios from "axios";
import AssignmentGroup from "../../AssignmentGroup";

class IdentifyMultiplayer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cnf: this.props.cnf,
            phase: this.props.phase,
            headingPhase: this.props.headingPhase,

            //correct result from randomise
            threeString: this.props.threeString,
            fourString: this.props.fourString,
            pleString: this.props.pleString,
            branch1string: this.props.branch1string,
            branch2string: this.props.branch2string,

            //user input for random segments
            threeChoice: "",
            fourChoice: "",
            pleChoice: "",
            branch1choice: "",
            branch2choice: "",

            clauses: [],
            literals: [],
            literals4: [],

            //the things user selects (usually literals or clauses)
            selections: [],
            assignments: this.props.assignments,
            selections3: [],
            randomOutput1: this.props.randomOutput1, //randomise output
            selections4: [],
            randomOutput2: this.props.randomOutput2, //randomise output

            selectionsTrue: this.props.selectionsTrue, //the server thing for 2

            pleselections1: [],
            pleselections2: [],
            randomOutput3: this.props.randomOutput3, //randomise output

            branchselections1: [],
            branchselections2: [],
            randomOutput4: this.props.randomOutput4, //randomise output
            randomOutput5: this.props.randomOutput5, //randomise output
        };

        this.submission = this.submission.bind(this);
        this.buttonAction = this.buttonAction.bind(this);
        this.displaySelections = this.displaySelections.bind(this);
        this.assignmentAction = this.assignmentAction.bind(this)
        this.getAssignments = this.getAssignments.bind(this);
        this.shuffle = this.shuffle.bind(this);
        //this.parseInput = this.parseInput.bind(this);

    }

    parseInput(input){
        console.log(this.props)
        console.log(this.state)
        console.log("parseinput input: "+input);

        //Remove whitespace
        input = input.replaceAll(/ /g,'');

        //Remove brackets
        input = input.replaceAll("(", "");
        input = input.replaceAll(")", "");

        //Remove double negatives
        input = input.replaceAll("¬¬","");

        console.log(input);

        //Split the formula into clauses and set
        return input.split("∧");
    }

    getLiterals(clauses){
        let literals = [];
        for(const clause of clauses){
            const lits = clause.split("∨");
            for(const lit of lits){
                if (!literals.includes(lit)){
                    literals.push(lit);
                }
            }
        }
        return literals;
    }

    heading(){
        switch (this.state.headingPhase) {
            default :
                return "Please select the unit clauses."
            case "up2" :
                return "Please choose the correct assignments."
            case "up3" :
                return "Identify the correct clauses to remove according to assignments:"
            case "random1" :
                return "Choose the correct simplification"
            case "up4" :
                return "Identify the correct literals to remove according to assignments:"
            case "win" :
                return "Success!"
            case "lose1" :
                return "Incorrect- select the unit clauses."
            case "lose2" :
                return "Incorrect - choose the correct assignments."
            case "lose3" :
                return "Incorrect - identify the correct clauses to remove according to assignments:"
            case "loserandom1" :
                return "Incorrect - Choose the correct simplification"
            case "lose4" :
                return "Incorrect - identify the correct literals to remove according to assignments:"
            case "ple1" :
                return "Identify the pure literals."
            case "loseple1" :
                return "Identify the pure literals."
            case "ple2" :
                return "Select the clauses to remove, that contain those literals."
            case "loseple2" :
                return "Select the clauses to remove, that contain those literals."
            case "branch1":
                return "Based on your chosen branching of "+this.props.brancher+", select clauses to remove that contain it."
            case "branch2":
                return "Based on your chosen branching of "+this.props.brancher+", select instances of its complement to remove."
            case "losebranch1":
                return "Incorrect - select clauses containing "+this.props.brancher
            case "losebranch2":
                return "Incorrect - select the complement of "+this.props.brancher
        }
    }

    buttonAction(button){
        switch (this.state.phase) {
            default :
                let state = this.state;
                if(state.selections.includes(button)){
                    state.selections.splice(state.selections.indexOf(button), 1)
                }
                else{
                    state.selections.push(button);
                }
                this.setState(state);
                break;
            case "up2" :
                break;
            case "up3" :
                let state3 = this.state;
                if(state3.selections3.includes(button)){
                    state3.selections3.splice(state3.selections3.indexOf(button), 1)
                }
                else{
                    state3.selections3.push(button);
                }
                this.setState(state3);
                break;
            case "random1":
                let stater1 = this.state;
                stater1.threeChoice = button;
                this.setState(stater1);
                this.submission();
                break;
            case "up4" :
                let state4 = this.state;
                if(state4.selections4.includes(button)){
                    state4.selections4.splice(state4.selections4.indexOf(button), 1)
                }
                else{
                    state4.selections4.push(button);
                }
                this.setState(state4);
                break;
            case "random2" :
                let stater2 = this.state;
                stater2.fourChoice = button;
                this.setState(stater2);
                this.submission();
                break;
            case "ple1" :
                let state5 = this.state;
                if(state5.pleselections1.includes(button)){
                    state5.pleselections1.splice(state5.pleselections1.indexOf(button), 1)
                }
                else{
                    state5.pleselections1.push(button);
                }
                this.setState(state5);
                break;
            case "ple2" :
                let state6 = this.state;
                if(state6.pleselections2.includes(button)){
                    state6.pleselections2.splice(state6.pleselections2.indexOf(button), 1)
                }
                else{
                    state6.pleselections2.push(button);
                }
                this.setState(state6);
                break;
            case "random3" :
                let stater3 = this.state;
                stater3.pleChoice = button;
                this.setState(stater3);
                this.submission();
                break;
            case "branch1" :
                let state7 = this.state;
                if(state7.branchselections1.includes(button)){
                    state7.branchselections1.splice(state7.branchselections1.indexOf(button), 1)
                }
                else{
                    state7.branchselections1.push(button);
                }
                this.setState(state7);
                break;
            case "branchrand1" :
                let stater4 = this.state;
                stater4.branch1choice = button;
                this.setState(stater4);
                this.submission();
                break;
            case "branch2" :
                let state8 = this.state;
                if(state8.branchselections2.includes(button)){
                    state8.branchselections2.splice(state8.branchselections2.indexOf(button), 1)
                }
                else{
                    state8.branchselections2.push(button);
                }
                this.setState(state8);
                break;
            case "branchrand2" :
                let stater5 = this.state;
                stater5.branch2choice = button;
                this.setState(stater5);
                this.submission();
                break;
        }
    }

    submission(){
        switch (this.state.phase) {
            default:
                let temp = "";
                for(let i= 0; i < this.state.clauses.length; i++) {
                    if (this.state.selections.includes(this.state.clauses[i])){
                        temp += i;
                        temp += ",";
                    }
                }
                let orderedSelections = this.orderLiterals(this.state.literals, this.state.selections)
                //send CNF, temp to server
                //also send orderedSelections so that we can send them if correct
                //make sure to order the literals using orderLiterals before phase 2.
                this.props.up1(this.state.cnf, temp, orderedSelections)
                break;
            case "up2" :
                let temp2 = this.getAssignments();
                //send cnf, temp2 to server
                this.props.up2(this.state.cnf, temp2)
                break;
            case "up3" :
                let temp3 = "";
                for(let i= 0; i < this.state.clauses.length; i++) {
                    if (this.state.selections3.includes(this.state.clauses[i])){
                        temp3 += i;
                        temp3 += ",";
                    }
                }
                this.props.up3(this.state.cnf, temp3);
                //send cnf, temp3 to server
                //crucially, on server, make sure to do checkConflict(cnf) if the indexes are right
                //Randomise then takes place on clauses.
                break;
            case "random1":
                //Here, I should have received a correct CNF and three false ones.
                //We will store the correct value in the server under the lobby object.
                //send choice to server
                //crucially, check if 4 is necessary on the correct cnf if correct choice.
                this.props.random1(this.state.threeChoice)
                break;
            case "up4" :
                let temp4 = "";
                for(let i= 0; i < this.state.literals4.length; i++) {
                    if (this.state.selections4.includes(this.state.literals4[i])){
                        temp4 += i;
                        temp4 += ",";
                    }
                }
                //Here, we send the original CNF at UP1, the simplified CNF after random1, and temp4.
                //Randomise then takes place on literals.
                this.props.up4(this.props.cnf, this.props.threeString, temp4)
                break;
            case "random2":
                //send cnf to server
                //on server, we check if we can do UP again, or PLE, otherwise goto branching
                this.props.random2(this.state.fourChoice)
                break;
            case "ple1":
                let temp5 = "";
                for(let i= 0; i < this.state.pleliterals.length; i++) {
                    if (this.state.pleselections1.includes(this.state.pleliterals[i])){
                        temp5 += i;
                        temp5 += ",";
                    }
                }
                //send cnf, indexes to server

                this.props.ple1(this.state.cnf, temp5);
                break;
            case "ple2":
                let temp6 = "";
                for(let i= 0; i < this.state.clauses.length; i++) {
                    if (this.state.pleselections2.includes(this.state.clauses[i])){
                        temp6 += i;
                        temp6 += ",";
                    }
                }
                //send cnf and indexes
                //Randomise then takes place on clauses.
                this.props.ple2(this.state.cnf, temp6);
                break;
            case "random3":
                //send cnf to server
                //on server, we check if we can do UP otherwise goto branching
                this.props.random3(this.state.pleChoice)
                break;
            case "branch1" :
                let temp7 = "";
                for(let i= 0; i < this.state.clauses.length; i++) {
                    if (this.state.branchselections1.includes(this.state.clauses[i])){
                        temp7 += i;
                        temp7 += ",";
                    }
                }
                //Send cnf, chosen branching literal ("brancher") to server
                //Randomise then takes place on clauses.
                this.props.branch1(this.state.cnf, this.props.brancher, temp7)
                break;
            case "branchrand1":
                //Here, I should have received a correct CNF and three false ones.
                //We will store the correct value in the server under the lobby object.
                //send choice to server
                //logically there must be a branch2 phase (branch means all var has complement)
                this.props.branchrand1(this.state.branch1choice, this.props.brancher)
                break;
            case "branch2":
                let temp8 = "";
                for(let i= 0; i < this.state.literals4.length; i++) {
                    if (this.state.branchselections2.includes(this.state.literals4[i])){
                        temp8 += i;
                        temp8 += ",";
                    }
                }
                //here we send original CNF (pre branch), branching literal, the new CNF, and temp8.
                //we then randomise on LITERALS.
                this.props.branch2(this.state.cnf, this.props.brancher, this.props.branch1string, temp8)
                break;
            case "branchrand2":
                //send choice to server like before
                //on server, check for UP and PLE.
                this.props.branchrand2(this.state.branch2choice, this.props.brancher)
                break;

        }
    }

    orderLiterals(literals, selections) {
        console.log("Literals: "+literals);
        console.log("Selections: "+selections);
        let arr = [];
        for (let lit of literals){
            if(selections.includes(lit)){
                arr.push(lit);
            }
        }
        return arr;
    }

    shuffle(arr){
        //Shuffle array
        for (let i = arr.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            const temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        }
        return arr;
    }


    //Also need to implement a way to check if my simplification is a result of empty clauses or no clauses.
    //TODO: Finish the alternate case --> Literals, need an API call to reduce. Send OG string with 1 stringified lits 3x arr.
    randomise(simplification, string, type){
        console.log("Simplification: "+simplification)
        let clauses = this.parseInput(string);
        let literals = this.getLiterals(clauses);
        let simpClauses = this.parseInput(simplification);
        let simpLiterals = this.getLiterals(simpClauses);
        //We let clauses = 0, literals = 1.
        let rem;
        //let lock = 0;
        console.log("SimpClauses: "+simpClauses)
        console.log("SimpLiterals: "+simpLiterals)

        //TODO consider dem cases innit
        // I need to take the unsimplified string and using the correct string, create some fake clause simplifications.
        // Two conditions - is it a clause simplification or a literal simplification?
        // The former implies we select some number from the set of clauses to remove - randomise the set.
        // Then divide into three (or two) equal parts.
        // For each new false simplification, use one of those subsets.
        // This only applies for n>=3 though

        //Construct array
        // No case for when simplification is both string and "" (we should capture that case and send to victory)
        let arr;
        console.log("Randomise string: "+string);
        console.log("Randomise simplification: "+simplification);

        if(simplification === "Fully simplified"){
            arr = ["Fully simplified"];
            rem = 3;
        } else {
            arr = [simplification, "Fully simplified"];
            rem = 2;
        }
        console.log("Rem: "+rem)
        console.log("Length of clauses: "+clauses.length)

        //The odd cases first
        if(clauses.length < 3){
            console.log("String: "+string)
            console.log("arr: "+arr)
            arr = arr.concat(string)
            if(clauses.length === 2){
                if(type === "clause"){
                    if(rem === 2){
                        //Need to bracket all elements in clauses before adding.
                        console.log("Removed element: "+arr.shift())
                        console.log("Array now: "+arr)
                        for(let i=0;i<2;i++)
                            arr = arr.concat("("+clauses[i]+")")
                        //arr = arr.concat(clauses)
                        console.log("Clauses: "+clauses)
                        console.log("Array now: "+arr)
                        //arr = arr.concat(string)
                        arr = this.shuffle(arr);
                        console.log("Array after shuffle: "+arr)
                    }
                    else{
                        arr.concat(clauses)
                    }
                    //end
                }
                else if(type === "literal"){
                    // a lot harder, I need to reduce all the clauses on each literal
                    // I'll do an api call for this perhaps
                    axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/reduceRandomiseBaby?CNF="+string+"&simp="+simplification+"&rem="+rem).then(r => {
                        if(r.data.includes(",")){
                            arr.concat(r.data.split(","));
                        } else arr.concat([r.data]);
                        arr = this.shuffle(arr);
                        console.log(arr);
                        //end
                    })
                }
            }
            else {
                //end
            }
        }
        else {
            //do everything inside this, we know that we have at least 3 literals.
            if (simplification === "Fully simplified") {
                //Simplification has no literals or clauses.
                //I should generate an arbitrary cardinality less than that of the string
                //Then make all the results of that cardinality.
                if (type === "clause") {
                    let c = Math.floor(Math.random() * (clauses.length - 1)) + 1;
                    axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/reduceRandomiseClauses?CNF="+string+"&simplification="+simplification+"&cardinality="+c+"&rem="+rem).then(r => {
                        arr = this.shuffle(arr.concat(r.data.split(',')));
                        console.log(arr);
                        //end
                    })


                } else if (type === "literal") {
                    let c = Math.floor(Math.random() * (literals.length - 1)) + 1;
                    axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/reduceRandomiseLiterals?CNF="+string+"&simplification="+simplification+"&cardinality="+c+"&rem="+rem).then(r => {
                        arr = this.shuffle(arr.concat(r.data.split(',')));
                        console.log(arr);
                        //end
                    })

                }

            } else {
                //I have the number of simplification's clauses and literals.
                //I can use that to generate a result of the same cardinality.
                if (type === "clause") {
                    let c = clauses.length - simpClauses.length;
                    console.log("boutta do it "+c);
                    axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/reduceRandomiseClauses?CNF="+string+"&simplification="+simplification+"&cardinality="+c+"&rem="+rem).then(r => {
                        arr = this.shuffle(arr.concat(r.data.split(',')));
                        console.log(arr);
                        //end
                    })

                } else if (type === "literal") {
                    console.log("this one?")
                    console.log("Lit: "+literals)
                    console.log("SimpLit: "+simpLiterals)
                    let c = literals.length - simpLiterals.length;
                    axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/reduceRandomiseLiterals?CNF="+string+"&simplification="+simplification+"&cardinality="+c+"&rem="+rem).then(r => {
                        arr = this.shuffle(arr.concat(r.data.split(',')));
                        console.log(arr);
                        //end
                    })

                }

            }
        }
    }

    displaySelections(){
        let temp = "";
        for(let i= 0; i < this.state.selections.length; i++) {
            temp += this.state.selections[i];
        }
        return temp;
    }

    assignmentAction(button, bool){
        let state = this.state;
        state.assignments[state.selectionsTrue.indexOf(button)] = bool
        this.setState(state);
    }

    getAssignments(){
        let str = "";
        for(let i=0;i<this.state.assignments.length;i++){
            if(this.state.assignments[i]){
                str += "1";
                //str += ",";
            }
            else{
                str += "0";
                //str += ",";
            }
        }
        console.log(str);
        return str;
    }

    componentDidMount() {
        if(this.state.phase !== "random1") {
            if(this.state.phase !== "random2") {
            console.log("CDM state:")
            console.log(this.state)
            let state = this.state;
            state.clauses = this.parseInput(state.cnf);
            state.literals = this.getLiterals(state.clauses);
            //state.phase = this.props.phase;
            if (this.props.phase === "up4") {
                console.log("ooh we're at up4")
                state.threeString = this.props.threeString
                state.clauses4 = this.parseInput(this.props.threeString)
                state.literals4 = this.getLiterals(this.parseInput(this.props.threeString))
            }
                if (this.props.phase === "branch2") {
                    console.log("ooh we're at branch2")
                    state.branch1string = this.props.branch1string
                    state.clauses4 = this.parseInput(this.props.branch1string)
                    state.literals4 = this.getLiterals(this.parseInput(this.props.branch1string))
                }
                if (this.props.phase === "branchrand2"){
                    state.branch1string = this.props.branch1string
                }
            state.pleliterals = this.getLiterals(state.clauses)
            console.log(state.pleliterals);
            this.setState(state);
            //this.getLiterals();
            //this.getVariables();
            console.log(this.props.selectionsTrue)
                console.log(this.state.assignments)
            }
        }
    }

        render() {
            console.log("From render (IDMP)")
            console.log(this.props)
            console.log(this.state)
            switch (this.state.phase) {
                default :
                    return(
                        <div>
                            <h3>Current CNF: {this.state.cnf}</h3>
                            <p id={"heading"}>{this.heading()}</p>
                            <ButtonGroup buttons={this.state.clauses} doSomethingAfterClick={this.buttonAction}></ButtonGroup>
                            <button onClick={this.submission}>Submit</button>
                            <p>Selections: {this.state.selections.length}</p>
                        </div>
                    )
                case "up2" :
                    //For this one, I need a different structure - can't remove clauses.
                    return(
                        <div>
                            <h3>Current CNF: {this.state.cnf}</h3>
                            <p id={"heading"}>{this.heading()}</p>
                            <AssignmentGroup buttons={this.props.selectionsTrue} doSomethingAfterClick={this.assignmentAction}></AssignmentGroup>
                            <button onClick={this.submission}>Submit</button>
                            <p>Assignments: {this.getAssignments()}</p>
                        </div>
                    );
                case "up3" :
                    return(
                        <div>
                            <h3>Current CNF: {this.state.cnf}</h3>
                            <p id={"heading"}>{this.heading()}</p>
                            <ButtonGroup buttons={this.state.clauses} doSomethingAfterClick={this.buttonAction}></ButtonGroup>
                            <button onClick={this.submission}>Submit</button>
                            <p>Selections: {this.state.selections3.length}</p>
                        </div>
                    )
                case "random1" :
                    return(
                        <div>
                            <h3>Current CNF: {this.state.cnf}</h3>
                            <p id={"heading"}>{this.heading()}</p>
                            <ButtonGroup buttons={this.state.randomOutput1} doSomethingAfterClick={this.buttonAction}></ButtonGroup>
                            <p>Selections: {this.state.selections3.length}</p>
                        </div>
                    )
                case "up4" :
                    //This one is literals of the negations.
                    return(
                        <div>
                            <h3>Current CNF: {this.state.threeString}</h3>
                            <p id={"heading"}>{this.heading()}</p>
                            <ButtonGroup buttons={this.state.literals4} doSomethingAfterClick={this.buttonAction}></ButtonGroup>
                            <Button onClick={this.submission}>Submit</Button>
                            <p>Selections: {this.state.selections4.length}</p>
                        </div>
                    )
                case "random2" :
                    return(
                        <div>
                            <h3>Current CNF: {this.state.threeString}</h3>
                            <p id={"heading"}>{this.heading()}</p>
                            <ButtonGroup buttons={this.state.randomOutput2} doSomethingAfterClick={this.buttonAction}></ButtonGroup>
                            <p>Selections: {this.state.selections4.length}</p>
                        </div>
                    )
                case "ple1" :
                    //For this one, I need a different structure - can't remove clauses.
                    return(
                        <div>
                            <h3>Current CNF: {this.state.cnf}</h3>
                            <p id={"heading"}>{this.heading()}</p>
                            <ButtonGroup buttons={this.state.literals} doSomethingAfterClick={this.buttonAction}></ButtonGroup>
                            <Button onClick={this.submission}>Submit</Button>
                            <p>Selections: {this.state.pleselections1.length}</p>
                            <p>CNF: {this.state.cnf}</p>
                            <p>Plestring: {this.state.pleString}</p>
                        </div>
                    )
                case "ple2" :
                    //For this one, I need a different structure - can't remove clauses.
                    return(
                        <div>
                            <h3>Current CNF: {this.state.cnf}</h3>
                            <p id={"heading"}>{this.heading()}</p>
                            <ButtonGroup buttons={this.state.clauses} doSomethingAfterClick={this.buttonAction}></ButtonGroup>
                            <Button onClick={this.submission}>Submit</Button>
                            <p>Selections: {this.state.pleselections2.length}</p>
                            <p>CNF: {this.state.cnf}</p>
                            <p>Plestring: {this.state.pleString}</p>
                        </div>
                    )
                case "random3" :
                    return(
                        <div>
                            <h3>Current CNF: {this.state.cnf}</h3>
                            <p id={"heading"}>{this.heading()}</p>
                            <ButtonGroup buttons={this.state.randomOutput3} doSomethingAfterClick={this.buttonAction}></ButtonGroup>
                            <p>Selections: {this.state.pleselections2.length}</p>
                            <p>CNF: {this.state.cnf}</p>
                            <p>Plestring: {this.state.pleString}</p>
                        </div>
                    )
                case "branch1" :
                    return(
                        <div>
                            <h3>Current CNF: {this.state.cnf}</h3>
                            <p id={"heading"}>{this.heading()}</p>
                            <ButtonGroup buttons={this.state.clauses} doSomethingAfterClick={this.buttonAction}></ButtonGroup>
                            <Button onClick={this.submission}>Submit</Button>
                            <p>Selections: {this.state.branchselections1.length}</p>
                        </div>
                    )
                case "branch2" :
                    return(
                        <div>
                            <h3>Current CNF: {this.state.branch1string}</h3>
                            <p id={"heading"}>{this.heading()}</p>
                            <ButtonGroup buttons={this.state.literals4} doSomethingAfterClick={this.buttonAction}></ButtonGroup>
                            <Button onClick={this.submission}>Submit</Button>
                            <p>Selections: {this.state.branchselections2.length}</p>
                        </div>
                    )
                case "branchrand1" :
                    return(
                        <div>
                            <h3>Current CNF: {this.state.cnf}</h3>
                            <p id={"heading"}>{this.heading()}</p>
                            <ButtonGroup buttons={this.state.randomOutput4} doSomethingAfterClick={this.buttonAction}></ButtonGroup>
                            <p>Selections: {this.state.branchselections1.length}</p>
                        </div>
                    )
                case "branchrand2" :
                    return(
                        <div>
                            <h3>Current CNF: {this.state.branch1string}</h3>
                            <p id={"heading"}>{this.heading()}</p>
                            <ButtonGroup buttons={this.state.randomOutput5} doSomethingAfterClick={this.buttonAction}></ButtonGroup>
                            <p>Selections: {this.state.branchselections2.length}</p>
                        </div>
                    )


            }
        }
    }

    export default IdentifyMultiplayer;