import React from "react";
import IdentifyMultiplayer from "./IdentifyMultiplayer";

class Branch1 extends React.Component {
    render() {
        return(
            <div>
                <h3>Branching Simplification 1</h3>
                <div>
                    <IdentifyMultiplayer brancher = {this.props.brancher} headingPhase={this.props.headingPhase} branch1 = {this.props.branch1} cnf={this.props.cnf} phase={this.props.phase}></IdentifyMultiplayer>
                </div>
            </div>
        )
    }
}

export default Branch1;