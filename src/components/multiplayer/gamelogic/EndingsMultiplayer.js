import React from "react";
import axios from "axios";
import Cookies from "universal-cookie";
class Endings extends React.Component {
    constructor(props) {
        super(props);
        this.cookies = new Cookies();
        this.state = {
            ending: this.props.ending,
        };
        this.getComponent = this.getComponent.bind(this)
    }

    componentDidMount() {
        let token = this.cookies.get('token');
        if (token !== undefined) {
            //check if cookie is valid
            axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/checkToken?token="+token).then(r => {
                console.log(r.data)
                if (r.data.userid !== -1) {
                    //gather data
                    this.props.sendUserData(r.data, r.data.username);
                    console.log(r.data.username)
                }
            })
        }
    }


    getComponent(){
        let component;
        switch (this.state.ending) {
            default:
               component =  <div>
                                <h3>CNF Unsatisfiable!</h3>
                                <p>Points gained: {this.props.rawPoints}</p>
                                <p>Wrong answers: {this.props.wrongAnswers}</p>
                                <p>Penalty: {this.props.wrongAnswers * 5}</p>
                                <p>Final XP score: {this.props.currentPoints}</p>
                            </div>
                break;
            case "success" :
                component = <div>
                                <h3>CNF satisfiable!</h3>
                                <p>Points gained: {this.props.rawPoints}</p>
                                <p>Times unnecessarily backtracked: {this.props.backtracks}</p>
                                <p>Wrong answers: {this.props.wrongAnswers}</p>
                                <p>Penalty: {(this.props.backtracks * 30) + (this.props.wrongAnswers * 5)}</p>
                                <p>Final XP score: {this.props.currentPoints}</p>
                            </div>
                break;
        }
        return component
    }
    render(){
        return(
            <div>
                {this.getComponent()}
            </div>
        )
    }
}
export default Endings;