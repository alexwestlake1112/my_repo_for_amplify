import React from "react";
import IdentifyMultiplayer from "./IdentifyMultiplayer";

class Random1 extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            randomOutput1 : this.props.randomOutput1,
            cnf : this.props.cnf,
            phase: this.props.phase
        }
    }

    componentDidMount() {
        console.log(this.props);
        console.log(this.props.phase);
    }

    render() {
        console.log("From render (Random1)")
        console.log(this.props)
        console.log(this.props.randomOutput1)
        console.log(this.state)
        return(
            <div>
                <h3>Unit Propagation 3.5</h3>
                <div>
                    <IdentifyMultiplayer headingPhase={this.props.headingPhase} phase={this.props.phase} random1 = {this.props.random1} randomOutput1={this.props.randomOutput1} cnf={this.props.cnf}></IdentifyMultiplayer>
                </div>
            </div>
        )
    }
}

export default Random1;