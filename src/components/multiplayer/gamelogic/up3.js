import React from "react";
import IdentifyMultiplayer from "./IdentifyMultiplayer";

class UP3 extends React.Component {
    render() {
        return(
            <div>
                <h3>Unit Propagation 3</h3>
                <div>
                    <IdentifyMultiplayer headingPhase={this.props.headingPhase} up3 = {this.props.up3} cnf={this.props.cnf} phase={this.props.phase}></IdentifyMultiplayer>
                </div>
            </div>
        )
    }
}

export default UP3;