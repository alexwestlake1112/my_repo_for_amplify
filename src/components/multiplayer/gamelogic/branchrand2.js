import React from "react";
import IdentifyMultiplayer from "./IdentifyMultiplayer";

class BranchRand2 extends React.Component {
    render() {
        return(
            <div>
                <h3>Branching Simplification 2.5</h3>
                <div>
                    <IdentifyMultiplayer branch1string = {this.props.branch1string} brancher = {this.props.brancher} headingPhase={this.props.headingPhase} branchrand2 = {this.props.branchrand2} randomOutput5={this.props.randomOutput5} cnf={this.props.cnf} phase={this.props.phase}></IdentifyMultiplayer>
                </div>
            </div>
        )
    }
}

export default BranchRand2;