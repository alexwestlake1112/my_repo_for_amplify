class TreeNode {
    constructor(parent, root){
        this.variable = "Current";

        this.parent = parent;
        this.isRoot = false;
        this.direction = undefined;
        this.varTrue = undefined;
        this.varFalse = undefined;
        this.root = root;
    }

//quick note, to get the parent's string do current.parent.string
// this could be useful if you need to get the parent string before branching
// so that you can quickly do the branching calculations or something to supply the right string here

    branchLeft(variable){
        //Add a new child based on the supplied string and var name
        //Var is attached to this node as it denotes what we are branching off of.
        this.variable = variable;
        this.varTrue = new TreeNode(this, this.root);
        this.varTrue.direction = true;
        this.root.current = this.varTrue;
    }

    branchRight(variable){
        //Add a new child based on the supplied string and var name
        //Var is attached to this node as it denotes what we are branching off of.
        this.variable = variable;
        this.varFalse = new TreeNode(this, this.root);
        this.varFalse.direction = false;
        this.root.current = this.varFalse;
    }

    backtrack()
    {
            //Move the focus to the current node's parent
            //this.root.current = this.root.current.parent;

            //Now look at THAT node's parent and see if we can create another child on left or right
            //(If these break, try doing .equals instead of ==)

            //Child on right free
            if(this.root.current.parent.varFalse == null){
                //parent.varFalse = new Node(null, this.root.current.parent, this.root);
                //this.root.current = parent.varFalse;
                this.root.current.parent.branchRight(this.root.current.parent.variable);
            }
            //Child on left free
            else if(this.root.current.parent.varTrue == null){
                //parent.varTrue = new Node(null, this.root.current.parent, this.root);
                //this.root.current = parent.varTrue;
                this.root.current.parent.branchLeft(this.root.current.parent.variable);
            }
            //No free spots :(
            else {
                console.log("Both spots taken!");
                this.root.current = this.root.current.parent;
                return this.root.current.backtrack();
                //this.root.current.var = "Newest current";
            }
            let retBool;
            if(this.root.current.parent.varTrue === this.root.current){
                retBool = true
            }
            else if(this.root.current.parent.varFalse === this.root.current){
                retBool = false
            }
            else retBool = null

            return [this.root.current.parent.getString(), this.root.current.parent.getVariable(), retBool];
        }

    setVar(variable){
        this.variable = variable;
    }
    setString(variable){
        this.string = variable;
    }
    getString(){
        return this.string;
    }
    getVariable(){
        return this.variable;
    }
}

export default TreeNode;