import React from "react";
import ButtonGroup from './ButtonGroup';
import axios from "axios";
import {Button} from "@mui/material";

class Branch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            parsedString: this.props.string,
            clauses: [],
            literals: [],
            variables: [],
            button: "",
            selection: false,
        };
        this.submitChoice = this.submitChoice.bind(this);
        this.setButton = this.setButton.bind(this);
        this.toggle = this.toggle.bind(this);
    }

    parseInput(input){
        //Remove whitespace
        input = input.replaceAll(/ /g,'');

        //Remove brackets
        input = input.replaceAll("(", "");
        input = input.replaceAll(")", "");

        //Remove double negatives
        input = input.replaceAll("¬¬","");

        console.log(input);

        //Split the formula into clauses and set
        return input.split("∧");
    }

    getLiterals(clauses){
        let literals = [];
        for(const clause of clauses){
            const lits = clause.split("∨");
            for(const lit of lits){
                if (!literals.includes(lit)){
                    literals.push(lit);
                }
            }
        }
        return literals;
    }

    getVariables(literals){
        let variables = [];
        for(const l of literals){
            const temp = l.split("¬");
            variables.push(temp[temp.length-1]);
        }
        return variables.reduce(function (a, b) {
            if (a.indexOf(b) < 0) a.push(b);
            return a;
        }, []);

    }
    setButton(buttonSel){
        let state = this.state;
        state.button = buttonSel;
        this.setState(state);
    }
    toggle(){
        if(this.state.selection){
            let state = this.state;
            state.selection = false;
            this.setState(state);
        }
        else{
            let state = this.state;
            state.selection = true;
            this.setState(state);
        }
    }

    submitChoice() {
        this.props.passChoice(this.state.button, this.state.selection);
        this.props.setChoice();
        //this.props.handleBranch(this.state.button, this.state.selection)
        axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/justBranch?CNF="+this.state.parsedString+
            "&variable="+this.state.button+"&value="+this.state.selection).then(r0 => {
                //this.props.passCorrect(r0.data);
                let state = this.state;
                state.parsedString = r0.data;
                this.setState(state);

                this.props.phase2();

                //How to adapt?
                //I need to just go along with it like the other phases - if I get empty clause then so be it.
                //Step 1 - remove clauses containing my branch variable - if I run out, great!
                //Step 1.5: randomisation phase
                //Step 2 - remove literals that are the complement of my branch variable - if empty, cry!
                //Step 2.5: randomisation phase
                //After this, I can THEN send my string over to the root node.
                //Then do the testUP/testPL tests like before.

                //I should also make sure that my simplification things send over "empty clause detected"
                //if it happens! Otherwise - it won't detect failure properly.

                //This checks for the inherent branch simplification returning an empty clause
                //  "Empty clause detected" or "Fully simplified"
                //Then if we're good, it sets the current node's string and proceeds either UP, PLE or branch.
                /*
                if(this.props.detectBranchFailure(r0.data)) {
                    this.props.sendData(r0.data)
                    //I need to check that there are unit clauses, or pure literals
                    axios.get("//localhost:8080/api/testUP?CNF=" + r0.data).then(r => {
                        if(r.data) {
                            this.props.doUP(r0.data);
                            console.log("1, "+r.data)
                        }
                        else{
                            axios.get("//localhost:8080/api/testPL?CNF=" + r0.data).then(r2 => {
                                if(r2.data) {
                                    this.props.doPLE(r0.data);
                                    console.log("2, "+r.data)
                                }
                                else{
                                    console.log("oops, "+r.data)
                                    this.props.prepareBranch(r0.data);
                                }
                            })
                        }
                    })




                 */
                    /*axios.get("//localhost:8080/api/oneRound?myCNF=" + this.state.parsedString).then(r => {
                        let state = this.state;
                        state.parsedString = r.data;
                        this.setState(state);

                        //epic function to send data to parent
                        this.props.sendData(r.data);
                        //epic function to swap components
                        //this.props.updateComponent("branch");
                        this.props.prepareBranch(r.data);
                    })
                }*/
            })
    }


    componentDidMount() {
        let state = this.state;
        state.clauses = this.parseInput(this.state.parsedString);
        state.literals = this.getLiterals(state.clauses);
        state.variables = this.getVariables(state.literals);
        state.button = state.variables[0];
        this.setState(state);
    }

    render() {
        return(
            <div>
                <div>
                    <h3>Current CNF: {this.state.parsedString}</h3>
                    <ButtonGroup buttons={this.state.variables} doSomethingAfterClick={this.setButton}></ButtonGroup>
                    <h3>current variable: {this.state.button}</h3>
                    <button onClick={this.toggle}>Toggle bool</button>
                    <h3>current assignment: {this.state.selection.toString()}</h3>
                    <button onClick={this.submitChoice}>Submit branch choice</button>
                </div>
            </div>
        )
    }
}

export default Branch;