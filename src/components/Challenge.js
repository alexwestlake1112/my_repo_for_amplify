import React from "react";
import Leaderboard from "./Leaderboard";
import Cookies from "universal-cookie";
import axios from "axios";

class Challenge extends React.Component {
    constructor(props) {
        super(props);
        this.cookies = new Cookies();
        this.state = {
            eligible : false,
            loggedIn : 0

        }
        this.getComponent = this.getComponent.bind(this);
        this.getEligibility = this.getEligibility.bind(this);
        this.startChallenge = this.startChallenge.bind(this);
        this.toggleButton = this.toggleButton.bind(this);
    }

    componentDidMount() {
        //here we want to pull the list of shop items from the shop
        //BUT ONLY IF LOGGED IN.
        let token = this.cookies.get('token');
        if(token !== undefined){
            this.setState({loggedIn : 1})
            this.getEligibility(this.props.userid)
        }
    }

    getEligibility(uid){
        axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/getEligibility?userid="+uid).then(r => {
            console.log(r.data);
            if(r.data){
                this.setState ({eligible : true});
            }
        })
    }

    startChallenge() {
        //First just send my string to the main app, so it can be used in subsequent functions
        this.props.setDifficulty("Daily")
        axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/generateCNF?seed="+0+"&difficulty="+0).then(r3 => {
            let cnf = r3.data;
            if(this.props.detectBranchFailure(cnf)){
                this.props.toggleCanvas();
                this.props.setCurrentlyPlaying(1);
                this.props.sendData(cnf);
                //I need to check that there are unit clauses, or pure literals
                axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/testUP?CNF=" + cnf).then(r => {
                    if(r.data) {
                        this.props.doUP(cnf);
                    }
                    else{
                        axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/testPL?CNF=" + cnf).then(r2 => {
                            if(r2.data) {
                                this.props.doPLE(cnf);
                            }
                            else{
                                console.log("oops")
                                this.props.prepareBranch(cnf);
                            }
                        })
                    }
                })
            }
        })
    }

    toggleButton(){
        if(this.state.eligible){
            return(
                <button onClick={this.startChallenge}>Begin challenge</button>
            )
        } else return <p>Already done daily challenge!</p>;
    }

    getComponent(){
        if(this.state.loggedIn === 1){
            return(
                <div>
                    <h2>Daily Challenge mode</h2>
                    {this.toggleButton()}
                    <Leaderboard></Leaderboard>
                </div>
            )
        }
        else return <p>Please log in or register to access the Daily Challenge!</p>
        //here we want to either prompt user to log in, or list their unbought items.
    }

    render(){
        return(
            <div>
                {this.getComponent()}
            </div>
        )
    }
}
export default Challenge