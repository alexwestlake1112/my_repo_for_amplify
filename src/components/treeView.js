import React from "react";
import {Divider, TextField} from "@mui/material";
import Root from "./Root";
import axios from "axios";

//TODO: Implement input limitations as set out previously such that the provided input parses correctly
//      and preserves empty clauses upon reconstruction (Also necessary to rework backend to that purpose)

class TreeView extends React.Component {
    constructor(props) {
        super(props);
        let tree = new Root();
        this.state = {
            name: "",
            parsedString: "",
            breadcrumb: "",
            tree: tree,
            base: false
        };
        this.parseInput = this.parseInput.bind(this);
        this.getLiterals = this.getLiterals.bind(this);
        this.subFunction = this.subFunction.bind(this);
        this.subFunction2 = this.subFunction2.bind(this);
        this.filterInput = this.filterInput.bind(this);
        this.goLeft = this.goLeft.bind(this);
        this.goRight = this.goRight.bind(this);
        this.goBack = this.goBack.bind(this);
        this.literalDupes = this.literalDupes.bind(this);
        this.clauseDupes = this.clauseDupes.bind(this);
        this.getRandomCNF = this.getRandomCNF.bind(this);
    }
    parseInput(input){
        //Remove whitespace
        input = input.replaceAll(/ /g,'');

        //Remove brackets
        input = input.replaceAll("(", "");
        input = input.replaceAll(")", "");

        //Remove double negatives
        input = input.replaceAll("¬¬","");

        //console.log(input);

        //Split the formula into clauses and set
        return input.split("∧");
    }

    getLiterals(clauses){
        let literals = [];
        for(const clause of clauses){
            const lits = clause.split("∨");
            for(const lit of lits){
                if (!literals.includes(lit)){
                    literals.push(lit);
                }
            }
        }
        return literals;
    }

    literalDupes(clause){
        let literals = clause.split("∨");
        console.log(clause);
        console.log(literals);
        let a = "";
        let dupefinder = [];
        for(let l of literals){
            if(!dupefinder.includes(l)){
                a = a+l+"∨";
                dupefinder.push(l);
            }
        }
        console.log(a)
        return a.slice(0, -1);
    }

    clauseDupes(clauses){
        console.log(clauses);
        let nonDupes = [];
        for(let c of clauses){
            if (!(nonDupes.includes(c))){
                nonDupes.push(c);
            }
        }
        console.log(nonDupes)
        return nonDupes;
    }

    subFunction(subTree, string){
        console.log("subfunction string: "+string)
        //only create children if our current node isn't a leaf
        if((string !== "Fully simplified") && (string !== "Empty clauses detected")){
            //get the branching variable (first one that appears)
            let clauses = this.parseInput(string)
            let literals = this.getLiterals(clauses)
            let branchingLit = literals[0];
            let branchingVar = branchingLit;
            if(branchingLit.charAt(0) === '¬'){
                branchingVar = branchingLit.substring(1);
            }
            subTree.variable = branchingVar;
            //output for branching simplifications
            let leftString, rightString;
            axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/justBranch2?CNF=" + string +"&variable=" + branchingVar).then(r2 => {
                leftString = r2.data.split(',')[0]
                rightString = r2.data.split(',')[1]

                //Now that we've gotten the child simplifications for our root node,
                // we need to create new children for each one and store them there.
                subTree.branchLeft(branchingVar);
                subTree.varTrue.setString(leftString);
                subTree.branchRight(branchingVar);
                subTree.varFalse.setString(rightString);

                //Perform the subfunction
                this.subFunction(subTree.varTrue, leftString)
                this.subFunction(subTree.varFalse, rightString)
            })
        }
        else {
            subTree.variable = "Leaf"
        }
    }

    subFunction2(subTree, string, parent){
        return new Promise(function(resolve, reject) {
            console.log("subfunction: " + string)
            //only create children if our current node isn't a leaf
            if ((string !== "Fully simplified") && (string !== "Empty clauses detected")) {
                //get the branching variable (first one that appears)
                let clauses = parent.parseInput(string)
                let literals = parent.getLiterals(clauses)
                let branchingLit = literals[0];
                let branchingVar = branchingLit;
                if (branchingLit.charAt(0) === '¬') {
                    branchingVar = branchingLit.substring(1);
                }
                subTree.variable = branchingVar;
                //output for branching simplifications
                let leftString, rightString;
                axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/justBranch2?CNF=" + string + "&variable=" + branchingVar).then(r2 => {
                    leftString = r2.data.split(',')[0]//.replaceAll("¬","");
                    rightString = r2.data.split(',')[1]//.replaceAll("¬","");

                    //Now that we've gotten the child simplifications for our root node,
                    // we need to create new children for each one and store them there.
                    subTree.branchLeft(branchingVar);
                    subTree.varTrue.setString(leftString);
                    subTree.branchRight(branchingVar);
                    subTree.varFalse.setString(rightString);

                    //Perform the subfunction
                    parent.subFunction2(subTree.varTrue, leftString, parent).then
                    (r3 => {
                        parent.subFunction2(subTree.varFalse, rightString, parent).then(r3 => {
                            console.log("third")
                                let state = parent.state;
                                state.tree.root.current = state.tree.root;
                                parent.setState(state);
                                resolve();
                        }
                        )}
                    )
                    //this.subFunction2(subTree.varTrue, leftString).then(
                    //this.subFunction2(subTree.varFalse, rightString))
                })
            } else {
                console.log("outer leaf")
                subTree.variable = "Leaf"
                let state = parent.state;
                state.tree.root.current = state.tree.root;
                parent.setState(state);
                resolve();
            }
        })
    }

    filterInput(str){
        //parse the input
        console.log("-------------------")
        console.log(str)
        let b = str.replace(/[^a-z¬∧∨]/gi, '');
        let clauses = this.parseInput(b);
        clauses = this.clauseDupes(clauses);
        let a = "";
        for(let c of clauses){
            c = this.literalDupes(c);
            c = "("+c+")";
            a = a+c+"∧";
        }
        let d = a.slice(0, -1)
        this.setState({name : d});

        if(this.getLiterals(this.parseInput(d)).includes("¬")){
            console.log("As intended (not so fast!).")
        } else {

            //reinitialise base
            this.setState({base: false});

            //Ok, now we do the new stuff!
            //> Input CNF in textbox -> "name"
            //> On textbox change -> parse "name" using oneRound
            //      - Store in "parsedString"
            //> Begin branching using that value
            //      - Create base node as new Root storing parsedString
            //      - Branch based on first variable in parsedString
            //      - Call doBranch (or whatever it is) and oneRound on each side
            //      - Store resultant string in each node
            //> Call a subfunction on each child node of root to recursively branch
            //      - Essentially, our stopping condition is "Fully simplified"
            //      - Alternatively we might get "Failure" or whatever it is
            //      - When we see it, we don't branch and instead just stop
            //      - We don't return the branch since it's added to root object dynamically
            //> Once tree built, pass the completed tree to App.js
            //      - RenderTree(root) should display the tree on main canvas

            //simplify string through simplification, store in parsedString
            axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/oneRound2?myCNF=" + d).then(r => {
                console.log("Our initial input: " + d)
                console.log("Our initial output from oneRound2 aka the simplification: " + r.data)
                let state = this.state;
                state.parsedString = r.data;
                this.setState(state);

                //create tree
                let newTree = new Root();
                newTree.string = r.data;


                //console.log(r.data);
                //console.log(this.state.parsedString);

                if ((this.state.parsedString !== "Fully simplified") && (this.state.parsedString !== "Empty clauses detected")) {
                    console.log("main bit")
                    //get the branching variable (first one that appears)
                    let clauses = this.parseInput(r.data)
                    let literals = this.getLiterals(clauses)
                    let branchingLit = literals[0];
                    let branchingVar = branchingLit;
                    if (branchingLit.charAt(0) === '¬') {
                        branchingVar = branchingLit.substring(1);
                    }
                    //output for branching simplifications
                    let leftString, rightString;
                    axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/justBranch2?CNF=" + r.data + "&variable=" + branchingVar).then(r2 => {
                        leftString = r2.data.split(',')[0]//.replaceAll("¬","");
                        rightString = r2.data.split(',')[1]//.replaceAll("¬","");

                        newTree.branchLeft(branchingVar);
                        newTree.branchRight(branchingVar);

                        newTree.varTrue.setString(leftString);
                        newTree.varFalse.setString(rightString);

                        let state = this.state;
                        state.tree = newTree;
                        state.tree.root.current = state.tree.root;
                        this.setState(state);

                        //Perform the subfunction

                        let parent = this;

                        this.subFunction2(newTree.varTrue, leftString, parent).then
                        (r3 => {
                                this.subFunction2(newTree.varFalse, rightString, parent).then(r3 => {
                                        this.props.setTree(this.state.tree);
                                        let state = this.state;
                                        state.tree.root.current = state.tree.root;
                                        this.setState(state);
                                    }
                                )
                            }
                        )

                        //this.subFunction(newTree.varTrue, leftString)
                        //this.subFunction(newTree.varFalse, rightString)

                        //Now that we've gotten the child simplifications for our root node,
                        // we need to create new children for each one and store them there.
                        //newTree.branchLeft(branchingVar);
                        //newTree.varTrue.setString(leftString);
                        //newTree.branchRight(branchingVar);
                        //newTree.varFalse.setString(rightString);
                        this.props.setTree(this.state.tree);
                    })
                } else {
                    console.log("initial leaf node")
                    newTree.variable = "Leaf"
                    let state = this.state;
                    state.tree = newTree;
                    state.tree.root.current = state.tree.root;
                    this.setState(state);
                    this.props.setTree(newTree);
                }
            }).finally(r4 => {
                console.log("let's see if this happens at the end")
                let state2 = this.state;
                state2.tree.root.current = state2.tree.root;
                this.setState(state2);
                (async () => {
                    // stuff
                    await this.props.setTree(this.state.tree); // This will work now.
                    // more stuff
                    console.log("very last thing")
                    await this.props.renderTree();
                })();
                //this.goBack();
            })
        }
    }

    goLeft(){
        let state = this.state;
        if((state.tree.current.varTrue !== null) && (state.tree.current.varTrue !== undefined)){
            state.tree.current = state.tree.current.varTrue;
        }
        this.setState(state);
    }
    goRight(){
        let state = this.state;
        if((state.tree.current.varFalse !== null) && (state.tree.current.varFalse !== undefined)) {
            state.tree.current = state.tree.current.varFalse;
        }
        this.setState(state);
    }
    goBack(){
        if(this.state.tree.current !== this.state.tree){
            let state = this.state;
            state.tree.current = state.tree.current.parent;
            this.setState(state);
        }
    }

    getRandomCNF() {
        let seed = -1
        let difficulty = 4
        axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/generateCNF?seed="+seed+"&difficulty="+difficulty).then(r => {
            console.log(r.data);
            this.filterInput(r.data);
            //this.setState({name : r.data})
            //this.filterInput(this.state.name)
        })
    }


    render() {
        //<button className="myButton" onClick={() => {this.getRandomCNF()}}>Generate random</button>
        return(
            <div>
                <div>
                    <h2>Tree view</h2>
                    <div>
                        <TextField
                            className={"inputBox"}
                            id={"entryField"}
                            name={"myCNF"}
                            error={this.state.name.length === 0}
                            helperText={!this.state.name.length ? 'CNF is required' : ''}
                            value={this.state.name} label="Enter your CNF"
                            onChange={(e) => {this.filterInput(e.target.value)}}
                        />
                    </div>
                    <div>
                        <button className="myButton" onClick={() => {this.setState({name : (this.state.name + "¬")})}}>Add ¬</button>
                        <button className="myButton" onClick={() => {this.setState({name : (this.state.name + "∧")})}}>Add ∧</button>
                        <button className="myButton" onClick={() => {this.setState({name : (this.state.name + "∨")})}}>Add ∨</button>
                    </div>
                    <button className="myButton" onClick={() => {this.getRandomCNF()}}>Generate random</button>

                    <Divider/>
                    <div>
                        <p>Simplified input: {this.state.parsedString} </p>
                    </div>
                    <Divider/>
                    <h3>Tree navigation controls</h3>
                    <button className="myButton" onClick={() => {this.goLeft()}}>Left</button>
                    <button className="myButton" onClick={() => {this.goRight()}}>Right</button>
                    <button className="myButton" onClick={() => {this.goBack()}}>Back</button>
                    <Divider/>
                    <div>
                        <h3>Current node string: {this.state.tree.current.string}</h3>
                    </div>
                </div>
            </div>
        )
        /*<button className="myButton" onClick={this.props.setTree(this.state.tree)}>Update Tree</button>
                    <Divider/>*/
    }
}

export default TreeView;