import React from "react";
import {Divider} from "@mui/material";

class LeaderboardItem extends React.Component {
    render() {
        return(
            <div>
                <p>{this.props.place} || {this.props.username} (ID: {this.props.userid}) || Score: {this.props.score}</p>
            </div>
        )
    }
}
export default LeaderboardItem