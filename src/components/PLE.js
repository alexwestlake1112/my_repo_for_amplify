import React from "react";
import {Button, TextField} from "@mui/material";
import Identify from "./Identify";

class PLE extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            parsedString: this.props.string,
            phase: "ple1"
        };

    }

    //Agenda:
    //  -Select unit clauses to remove
    //  -Select the variable assignment for those clauses
    //  -Select clauses to remove
    //  -Select variables to remove

    // -Component to remove clauses (indexed) 1,3
    // -Component to remove literals (indexed) 4
    // -Component to select assignment for those unit clauses (10010101) 5

    render() {
        return(
            <div>
                <h3>Pure Literal Elimination</h3>
                <div>
                    <Identify phase={this.state.phase}
                              prepareBranch={this.props.prepareBranch}
                              string={this.state.parsedString}
                              sendData={this.props.sendData}
                              doUP={this.props.doUP}
                              doPLE={this.props.doPLE}
                              detectBranchFailure={this.props.detectBranchFailure}
                              doSuccess = {this.props.doSuccess}
                              addPoints = {this.props.addPoints}
                              wrongAnswerPenalty = {this.props.wrongAnswerPenalty}>
                    </Identify>
                </div>
            </div>
        )
    }
}

export default PLE;