//Translation of Tree file from Java to JS
import TreeNode from "./Node"
class Root {

    constructor(){
        this.string = "";
        this.variable = "";
        this.direction = undefined;
        this.current = this;
        this.isRoot = true;
        this.varTrue = undefined;
        this.varFalse = undefined;
        this.root = this;
    }
    //Root constructor (We don't care about the var as we assign that later

    //quick note, to get the parent's string do current.parent.string
// this could be useful if you need to get the parent string before branching
// so that you can quickly do the branching calculations or something to supply the right string here

    branchLeft(variable){
        //Add a new child based on the supplied string and var name
        //Var is attached to this node as it denotes what we are branching off of.
        this.variable = variable;
        this.varTrue = new TreeNode(this, this);
        this.varTrue.direction = true;
        this.root.current = this.varTrue;
    }

    branchRight(variable){
        //Add a new child based on the supplied string and var name
        //Var is attached to this node as it denotes what we are branching off of.
        this.variable = variable;
        this.varFalse = new TreeNode(this, this.root);
        this.varFalse.direction = false;
        this.root.current = this.varFalse;
    }

    backtrack(){
            console.log("We're at the base, boys.");
            return ["placeholder", "placeholder", true];
        }

    setVar(variable){
        this.variable = variable;
    }
    setString(variable){
        this.string = variable;
    }
    getString(){
        return this.string;
    }
    getVariable(){
        return this.variable;
    }
}

export default Root;