import React from "react";
import ButtonGroup from "./ButtonGroup";
import {Button} from "@mui/material";
import axios from "axios";
import AssignmentGroup from "./AssignmentGroup";

class Identify extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            parsedString: this.props.string,
            phase: this.props.phase,
            headingPhase: this.props.phase,
            brancher: this.props.brancher,
            threeString: "",
            threeChoice: "",
            fourString: "",
            fourChoice: "",
            clauses: [],
            literals: [],
            clauses4: [],
            literals4: [],
            selections: [],
            assignments: [],
            selections3: [],
            selections4: [],
            randomOutput1: [],
            randomOutput2: [],
            pleliterals: [],
            pleselections1: [],
            pleselections2: [],
            randomOutput3: [],
            pleString: "",
            pleChoice: "",
            branchselections1: [],
            branchselections2: [],
            randomOutput4: [],
            randomOutput5: [],
            branch1string: "",
            branch2string: "",
            branch1choice: "",
            branch2choice: "",
        };
        this.submission = this.submission.bind(this);
        this.buttonAction = this.buttonAction.bind(this);
        this.displaySelections = this.displaySelections.bind(this);
        this.assignmentAction = this.assignmentAction.bind(this)
        this.getAssignments = this.getAssignments.bind(this);
        this.shuffle = this.shuffle.bind(this);

    }

    reinitialize(newCNF){
        let clauses = this.parseInput(newCNF);
        let literals = this.getLiterals(clauses);
        this.setState({
            parsedString: newCNF,
            phase: 1,
            headingPhase: 1,
            threeString: "",
            threeChoice: "",
            fourString: "",
            fourChoice: "",
            clauses: clauses,
            literals: literals,
            clauses4: [],
            literals4: [],
            selections: [],
            assignments: [],
            selections3: [],
            selections4: [],
            randomOutput1: [],
            randomOutput2: [],
            pleliterals: [],
            pleselections1: [],
            pleselections2: [],
            randomOutput3: [],
            pleString: "",
            pleChoice: "",
            branchselections1: [],
            branchselections2: [],
            randomOutput4: [],
            randomOutput5: [],
            branch1string: "",
            branch2string: "",
            branch1choice: "",
            branch2choice: "",
        })
    }

    parseInput(input){
        //Remove whitespace
        input = input.replaceAll(/ /g,'');

        //Remove brackets
        input = input.replaceAll("(", "");
        input = input.replaceAll(")", "");

        //Remove double negatives
        input = input.replaceAll("¬¬","");

        console.log(input);

        //Split the formula into clauses and set
        return input.split("∧");
    }

    getLiterals(clauses){
        let literals = [];
        for(const clause of clauses){
            const lits = clause.split("∨");
            for(const lit of lits){
                if (!literals.includes(lit)){
                    literals.push(lit);
                }
            }
        }
        return literals;
    }

    heading(){
        switch (this.state.headingPhase) {
            default :
                return "Please select the unit clauses."
            case "2" :
                return "Please choose the correct assignments."
            case "3" :
                return "Identify the correct clauses to remove according to assignments:"
            case "random1" :
                return "Choose the correct simplification"
            case "4" :
                return "Identify the correct literals to remove according to assignments:"
            case "win" :
                return "Success!"
            case "lose1" :
                return "Incorrect- select the unit clauses."
            case "lose2" :
                return "Incorrect - choose the correct assignments."
            case "lose3" :
                return "Incorrect - identify the correct clauses to remove according to assignments:"
            case "loserandom1" :
                return "Incorrect - Choose the correct simplification"
            case "lose4" :
                return "Incorrect - identify the correct literals to remove according to assignments:"
            case "ple1" :
                return "Identify the pure literals."
            case "loseple1" :
                return "Identify the pure literals."
            case "ple2" :
                return "Select the clauses to remove, that contain those literals."
            case "loseple2" :
                return "Select the clauses to remove, that contain those literals."
            case "branch1":
                return "Based on your chosen branching of "+this.props.brancher+", select clauses to remove that contain it."
            case "branch2":
                return "Based on your chosen branching of "+this.props.brancher+", select instances of its complement to remove."
            case "losebranch1":
                return "Incorrect - select clauses containing "+this.props.brancher
            case "losebranch2":
                return "Incorrect - select the complement of "+this.props.brancher
        }
    }

    buttonAction(button){
        switch (this.state.phase) {
            default :
                let state = this.state;
                if(state.selections.includes(button)){
                    state.selections.splice(state.selections.indexOf(button), 1)
                }
                else{
                    state.selections.push(button);
                }
                this.setState(state);
                break;
            case "2" :
                break;
            case "3" :
                let state3 = this.state;
                if(state3.selections3.includes(button)){
                    state3.selections3.splice(state3.selections3.indexOf(button), 1)
                }
                else{
                    state3.selections3.push(button);
                }
                this.setState(state3);
                break;
            case "random1":
                let stater1 = this.state;
                stater1.threeChoice = button;
                this.setState(stater1);
                this.submission();
                break;
            case "4" :
                let state4 = this.state;
                if(state4.selections4.includes(button)){
                    state4.selections4.splice(state4.selections4.indexOf(button), 1)
                }
                else{
                    state4.selections4.push(button);
                }
                this.setState(state4);
                break;
            case "random2" :
                let stater2 = this.state;
                stater2.fourChoice = button;
                this.setState(stater2);
                this.submission();
                break;
            case "ple1" :
                let state5 = this.state;
                if(state5.pleselections1.includes(button)){
                    state5.pleselections1.splice(state5.pleselections1.indexOf(button), 1)
                }
                else{
                    state5.pleselections1.push(button);
                }
                this.setState(state5);
                break;
            case "ple2" :
                let state6 = this.state;
                if(state6.pleselections2.includes(button)){
                    state6.pleselections2.splice(state6.pleselections2.indexOf(button), 1)
                }
                else{
                    state6.pleselections2.push(button);
                }
                this.setState(state6);
                break;
            case "random3" :
                let stater3 = this.state;
                stater3.pleChoice = button;
                this.setState(stater3);
                this.submission();
                break;
            case "branch1" :
                let state7 = this.state;
                if(state7.branchselections1.includes(button)){
                    state7.branchselections1.splice(state7.branchselections1.indexOf(button), 1)
                }
                else{
                    state7.branchselections1.push(button);
                }
                this.setState(state7);
                break;
            case "branchrand1" :
                let stater4 = this.state;
                stater4.branch1choice = button;
                this.setState(stater4);
                this.submission();
                break;
            case "branch2" :
                let state8 = this.state;
                if(state8.branchselections2.includes(button)){
                    state8.branchselections2.splice(state8.branchselections2.indexOf(button), 1)
                }
                else{
                    state8.branchselections2.push(button);
                }
                this.setState(state8);
                break;
            case "branchrand2" :
                let stater5 = this.state;
                stater5.branch2choice = button;
                this.setState(stater5);
                this.submission();
                break;
        }
    }

    submission(){
        switch (this.state.phase) {
            default:
                let temp = "";
                for(let i= 0; i < this.state.clauses.length; i++) {
                    if (this.state.selections.includes(this.state.clauses[i])){
                        temp += i;
                        temp += ",";
                    }
                }
                axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/checkup1?CNF="+this.state.parsedString+"&indexes="+temp).then(r => {
                    if(r.data){
                        let state = this.state;
                        state.phase = "2";
                        state.headingPhase = "2";
                        for(let i=0;i<state.selections.length;i++){
                            state.assignments.push(false)
                        }
                        state.selections = this.orderLiterals(state.literals, state.selections);
                        this.setState(state);
                    }
                    else {
                        let state = this.state;
                        state.headingPhase = "lose1";
                        this.setState(state);
                        this.props.wrongAnswerPenalty();
                    }
                })
                break;
            case "2" :
                let temp2 = this.getAssignments();

                axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/checkup2?CNF="+this.state.parsedString+"&assignments="+temp2).then(r => {
                    if(r.data){

                        let state = this.state;
                        state.phase = "3";
                        state.headingPhase = "3";
                        this.setState(state);
                    }
                    else {
                        let state = this.state;
                        state.headingPhase = "lose2";
                        this.setState(state);
                        this.props.wrongAnswerPenalty();
                    }
                })
                break;
            case "3" :
                let temp3 = "";
                for(let i= 0; i < this.state.clauses.length; i++) {
                    if (this.state.selections3.includes(this.state.clauses[i])){
                        temp3 += i;
                        temp3 += ",";
                    }
                }
                console.log("Phase 3: Sending "+temp3+" to server")
                axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/checkup3?CNF="+this.state.parsedString+"&indexes="+temp3).then(r => {
                    console.log(r.data);
                    if(r.data === "@"){
                        let state = this.state;
                        state.headingPhase = "lose3";
                        this.setState(state);
                        this.props.wrongAnswerPenalty();
                    }
                    else {
                        //FIRST, CHECK IF WE HAVE THE X ^ ¬X
                        console.log(this.state.parsedString);
                        axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/checkConflict?CNF="+this.state.parsedString).then(r2 => {
                            console.log(r2.data)
                            if(r2.data === 1){
                                this.props.handleConflict();
                        }
                        else {
                            let state = this.state;
                            state.threeString = r.data;
                            state.headingPhase = "random1";
                            this.setState(state);


                            //this.randomise(r.data, this.state.parsedString, "clause", "random1");
                            this.randomiseClauses(r.data, this.state.parsedString, "random1");
                        }
                        })
                    }
                })
                break;
            case "random1":
                if(this.state.threeString === this.state.threeChoice){

                    console.log("hi");
                    this.props.addPoints(20).then(() => {

                    if(this.props.detectBranchFailure(this.state.threeString)) {
                        this.props.sendData(this.state.threeString);
                        axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/4necessary?CNF="+this.state.parsedString).then(r => {
                            console.log(r.data);
                            if(r.data){
                                let state = this.state;
                                state.phase = "4";
                                state.headingPhase = "4";
                                state.clauses4 = this.parseInput(state.threeString);
                                state.literals4 = this.getLiterals(this.parseInput(state.threeString));
                                this.setState(state);
                            }
                            else {
                                axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/testUP?CNF=" + this.state.threeString).then(r3 => {
                                    console.log("Hey")
                                    console.log(r3.data)
                                    if(r3.data) {
                                        this.props.doUP(this.state.threeString);
                                        console.log("1, "+r3.data)
                                    }
                                    else{
                                        console.log("Hey2")
                                        axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/testPL?CNF=" + this.state.threeString).then(r2 => {
                                            console.log("Hey3")
                                            if(r2.data) {
                                                console.log("Hey4")
                                                this.props.doPLE(this.state.threeString);
                                                console.log("2, "+r.data)
                                            }
                                            else{
                                                console.log("Hey5")
                                                console.log("oops, "+r.data)
                                                this.props.prepareBranch(this.state.threeString);
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }});
                }
                else{
                    //change header
                    let state = this.state;
                    state.headingPhase = "loserandom1";
                    this.setState(state);
                    this.props.wrongAnswerPenalty();
                }
                break;
            case "4" :
                let temp4 = "";
                for(let i= 0; i < this.state.literals4.length; i++) {
                    if (this.state.selections4.includes(this.state.literals4[i])){
                        temp4 += i;
                        temp4 += ",";
                    }
                }
                console.log("Phase 4: Sending "+temp4+" to server")
                axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/checkup4?CNF="+this.state.parsedString+"&NewCNF="+this.state.threeString+"&indexes="+temp4).then(r => {
                    console.log(r.data);
                    if(r.data === "@"){
                        let state = this.state;
                        state.headingPhase = "lose4";
                        this.setState(state);
                        this.props.wrongAnswerPenalty();
                    }
                    else {
                        //TODO Filter out case with x and ¬x?

                        let state = this.state;
                        console.log(r.data);
                        state.fourString = r.data;
                        state.headingPhase = "random1";
                        this.setState(state);

                        //this.randomise(r.data, this.state.threeString, "literal", "random2");
                        this.randomiseLiterals(r.data, this.state.threeString, "random2");
                    }
                })
                break;
            case "random2":
                if(this.state.fourString === this.state.fourChoice){

                    this.props.addPoints(10).then(() => {

                    if(this.props.detectBranchFailure(this.state.fourString)) {
                        this.props.sendData(this.state.fourString);
                        axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/testUP?CNF=" + this.state.fourString).then(r => {
                            if(r.data) {
                                console.log("Do up 2")
                                this.reinitialize(this.state.fourString);
                            }
                            else{
                                axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/testPL?CNF=" + this.state.fourString).then(r2 => {
                                    if(r2.data) {
                                        this.props.doPLE(this.state.fourString);
                                    }
                                    else{
                                        console.log("Skipping PLE")
                                        this.props.prepareBranch(this.state.fourString);
                                    }
                                })
                            }
                        })
                    }});
                }
                else{
                    //change header
                    let state = this.state;
                    state.headingPhase = "loserandom1";
                    this.setState(state);
                    this.props.wrongAnswerPenalty();
                }
                break;
            case "ple1":
                let temp5 = "";
                for(let i= 0; i < this.state.pleliterals.length; i++) {
                    if (this.state.pleselections1.includes(this.state.pleliterals[i])){
                        temp5 += i;
                        temp5 += ",";
                    }
                }
                console.log(temp5)
                axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/checkple1?CNF="+this.state.parsedString+"&indexes="+temp5).then(r => {
                    if(r.data){
                        //console.log("case1")
                        let state = this.state;
                        state.phase = "ple2";
                        state.headingPhase = "ple2";
                        this.setState(state);
                    }
                    else {
                        //console.log("case2")
                        let state = this.state;
                        state.headingPhase = "loseple1";
                        this.setState(state);
                        this.props.wrongAnswerPenalty();
                    }
                })
                break;
            case "ple2":
                if(this.state.pleselections2.length === 0){
                    let state = this.state;
                    state.headingPhase = "loseple2";
                    this.setState(state);
                    this.props.wrongAnswerPenalty();
                }
                else{
                    let temp6 = "";
                    for(let i= 0; i < this.state.clauses.length; i++) {
                        if (this.state.pleselections2.includes(this.state.clauses[i])){
                            temp6 += i;
                            temp6 += ",";
                        }
                    }
                    console.log("Phase 4: Sending "+temp6+" to server")
                    axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/checkple2?CNF="+this.state.parsedString+"&NewCNF="+this.state.pleString+"&indexes="+temp6).then(r => {
                        console.log(r.data);
                        if(r.data === "@"){
                            let state = this.state;
                            state.headingPhase = "loseple2";
                            this.setState(state);
                            this.props.wrongAnswerPenalty();
                        }
                        else {
                            let state = this.state;
                            console.log(r.data);
                            state.pleString = r.data;
                            state.headingPhase = "random1";
                            this.setState(state);

                            //this.randomise(r.data, this.state.parsedString, "clause", "random3")
                            this.randomiseClauses(r.data, this.state.parsedString, "random3");
                        }
                    })
                }
                break;
            case "random3":
                if(this.state.pleString === this.state.pleChoice){

                    this.props.addPoints(15).then(() => {

                    if(this.props.detectBranchFailure(this.state.pleString)) {
                        this.props.sendData(this.state.pleString);
                        axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/testUP?CNF=" + this.state.pleString).then(r2 => {
                            if(r2.data) {
                                this.props.doUP(this.state.pleString);
                            }
                            else{
                                axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/testPL?CNF=" + this.state.pleString).then(r3 => {
                                    if(r3.data) {
                                        this.props.doPLE(this.state.pleString);
                                    }
                                    else{
                                        console.log("Skipping PLE")
                                        this.props.prepareBranch(this.state.pleString);
                                    }
                                })
                            }
                        })
                    }});
                }
                else{
                    //change header
                    let state = this.state;
                    state.headingPhase = "loserandom1";
                    this.setState(state);
                    this.props.wrongAnswerPenalty();
                }
                break;
            case "branch1" :
                let temp7 = "";
                for(let i= 0; i < this.state.clauses.length; i++) {
                    if (this.state.branchselections1.includes(this.state.clauses[i])){
                        temp7 += i;
                        temp7 += ",";
                    }
                }
                console.log("Phase 1: Sending "+temp7+" to server")
                axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/branchClauses?CNF="+this.state.parsedString+"&brancher="+this.props.brancher+
                    "&indexes="+temp7).then(r => {
                    console.log(r.data);
                    if(r.data === "@"){
                        let state = this.state;
                        state.headingPhase = "losebranch1";
                        this.setState(state);
                        this.props.wrongAnswerPenalty();
                    }
                    else {
                        let state = this.state;
                        state.branch1string = r.data;
                        state.headingPhase = "random1";
                        this.setState(state);

                        //this.randomise(r.data, this.state.parsedString, "clause", "branchrand1");
                        this.randomiseClauses(r.data, this.state.parsedString, "branchrand1");
                    }
                })
                break;
            case "branchrand1":
                console.log(this.state.branch1string);
                console.log(this.state.branch1choice);
                if(this.state.branch1string === this.state.branch1choice){
                    this.props.addPoints(10).then(() => {
                    if(this.props.detectBranchFailure(this.state.branch1string)) {
                        this.props.sendData(this.state.branch1string);
                        axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/branchNecessary?CNF="+this.state.branch1string+"&brancher="+this.state.brancher).then(r => {
                            console.log(r.data);
                            if(r.data){
                                let state = this.state;
                                state.phase = "branch2";
                                state.headingPhase = "branch2";
                                state.clauses4 = this.parseInput(state.branch1choice);
                                state.literals4 = this.getLiterals(this.parseInput(state.branch1choice));
                                this.setState(state);
                                console.log(state.literals4);
                            }
                            else {
                                axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/testUP?CNF=" + this.state.branch1string).then(r3 => {
                                    console.log("Hey")
                                    console.log(r3.data)
                                    if(r3.data) {
                                        this.props.doUP(this.state.branch1string);
                                        console.log("1, "+r3.data)
                                    }
                                    else{
                                        console.log("Hey2")
                                        axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/testPL?CNF=" + this.state.branch1string).then(r2 => {
                                            console.log("Hey3")
                                            if(r2.data) {
                                                console.log("Hey4")
                                                this.props.doPLE(this.state.branch1string);
                                                console.log("2, "+r.data)
                                            }
                                            else{
                                                console.log("Hey5")
                                                console.log("oops, "+r.data)
                                                this.props.prepareBranch(this.state.branch1string);
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }});
                }
                else{
                    //change header
                    let state = this.state;
                    state.headingPhase = "loserandom1";
                    this.setState(state);
                    this.props.wrongAnswerPenalty();
                }
                break;
            case "branch2":
                let temp8 = "";
                for(let i= 0; i < this.state.literals4.length; i++) {
                    if (this.state.branchselections2.includes(this.state.literals4[i])){
                        temp8 += i;
                        temp8 += ",";
                    }
                }
                console.log("Phase 4: Sending "+temp8+" to server")
                axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/branchLiterals?CNF="+this.state.parsedString+"&brancher="+this.props.brancher+"&NewCNF="+this.state.branch1string+"&indexes="+temp8).then(r => {
                    console.log(r.data);
                    if(r.data === "@"){
                        let state = this.state;
                        state.headingPhase = "losebranch2";
                        this.setState(state);
                        this.props.wrongAnswerPenalty();
                    }
                    else {
                        let state = this.state;
                        console.log(r.data);
                        state.branch2string = r.data;
                        state.headingPhase = "random1";
                        this.setState(state);

                        //this.randomise(r.data, this.state.branch1string, "literal", "branchrand2");
                        this.randomiseLiterals(r.data, this.state.branch1string, "branchrand2");
                    }
                })
                break;
            case "branchrand2":
                if(this.state.branch2string === this.state.branch2choice){
                    //quickly handle the branching on visual tree before continuing
                    //this.props.setChoice();


                    this.props.addPoints(10).then(() => {

                    if(this.props.detectBranchFailure(this.state.branch2string)) {
                        this.props.sendData(this.state.branch2string)
                        console.log("Made it to here")
                        //I need to check that there are unit clauses, or pure literals
                        axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/testUP?CNF=" + this.state.branch2string).then(r => {
                            console.log("Hey")
                            console.log(r.data)
                            if(r.data) {
                                this.props.doUP(this.state.branch2string);
                                console.log("1, "+r.data)
                            }
                            else{
                                console.log("Hey2")
                                axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/testPL?CNF=" + this.state.branch2string).then(r2 => {
                                    console.log("Hey3")
                                    if(r2.data) {
                                        console.log("Hey4")
                                        this.props.doPLE(this.state.branch2string);
                                        console.log("2, "+r.data)
                                    }
                                    else{
                                        console.log("Hey5")
                                        console.log("oops, "+r.data)
                                        this.props.prepareBranch(this.state.branch2string);
                                    }
                                })
                            }
                        })
                    }});
            }
            else{
                //change header
                let state = this.state;
                state.headingPhase = "loserandom1";
                this.setState(state);
                this.props.wrongAnswerPenalty();
            }
                break;

        }
    }

    orderLiterals(literals, selections) {
        console.log("Literals: "+literals);
        console.log("Selections: "+selections);
        let arr = [];
        for (let lit of literals){
            if(selections.includes(lit)){
                arr.push(lit);
            }
        }
        return arr;
    }

    shuffle(arr){
        //Shuffle array
        for (let i = arr.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            const temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        }
        return arr;
    }


    //Also need to implement a way to check if my simplification is a result of empty clauses or no clauses.
    //TODO: Split up into randomiseLiterals and randomiseClauses
    randomise(simplification, string, type, goal){
        console.log("Simplification: "+simplification)
        let clauses = this.parseInput(string);
        let literals = this.getLiterals(clauses);
        let simpClauses = this.parseInput(simplification);
        let simpLiterals = this.getLiterals(simpClauses);
        //We let clauses = 0, literals = 1.
        let rem;
        //let lock = 0;
        console.log("SimpClauses: "+simpClauses)
        console.log("SimpLiterals: "+simpLiterals)

        //TODO consider dem cases innit
        // I need to take the unsimplified string and using the correct string, create some fake clause simplifications.
        // Two conditions - is it a clause simplification or a literal simplification?
        // The former implies we select some number from the set of clauses to remove - randomise the set.
        // Then divide into three (or two) equal parts.
        // For each new false simplification, use one of those subsets.
        // This only applies for n>=3 though

        //Construct array
        // No case for when simplification is both string and "" (we should capture that case and send to victory)
        let arr;
        console.log("Randomise string: "+string);
        console.log("Randomise simplification: "+simplification);

        if(simplification === "Fully simplified"){
            arr = ["Fully simplified"];
            rem = 3;
        } else {
            arr = [simplification, "Fully simplified"];
            rem = 2;
        }
        console.log("Rem: "+rem)
        console.log("Length of clauses: "+clauses.length)

        //The odd cases first
        if(clauses.length < 3){
            console.log("String: "+string)
            console.log("arr: "+arr)
            arr = arr.concat(string)
            if(clauses.length === 2){
                if(type === "clause"){
                    if(rem === 2){
                        //Need to bracket all elements in clauses before adding.
                        console.log("Removed element: "+arr.shift())
                        console.log("Array now: "+arr)
                        for(let i=0;i<2;i++)
                            arr = arr.concat("("+clauses[i]+")")
                        //arr = arr.concat(clauses)
                        console.log("Clauses: "+clauses)
                        console.log("Array now: "+arr)
                        //arr = arr.concat(string)
                        arr = this.shuffle(arr);
                        console.log("Array after shuffle: "+arr)
                    }
                    else{
                        arr.concat(clauses)
                    }
                    switch (goal){
                        default:
                            let state = this.state;
                            state.randomOutput1 = arr;
                            state.phase = goal;
                            this.setState(state);
                            break;
                        case "random1":
                            let state1 = this.state;
                            state1.randomOutput1 = arr;
                            state1.phase = goal;
                            this.setState(state1);
                            break;
                        case "random2" :
                            let state2 = this.state;
                            state2.randomOutput2 = arr;
                            state2.phase = goal;
                            this.setState(state2);
                            break;
                        case "random3" :
                            let state3 = this.state;
                            state3.randomOutput3 = arr;
                            state3.phase = goal;
                            this.setState(state3);
                            break;
                        case "branchrand1" :
                            let state4 = this.state;
                            state4.randomOutput4 = arr;
                            state4.phase = goal;
                            this.setState(state4);
                            break;
                        case "branchrand2" :
                            let state5 = this.state;
                            state5.randomOutput5 = arr;
                            state5.phase = goal;
                            this.setState(state5);
                            break;
                    }
                }
                else if(type === "literal"){
                    // a lot harder, I need to reduce all the clauses on each literal
                    // I'll do an api call for this perhaps
                    axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/reduceRandomiseBaby?CNF="+string+"&simp="+simplification+"&rem="+rem).then(r => {
                        if(r.data.includes(",")){
                            arr.concat(r.data.split(","));
                        } else arr.concat([r.data]);
                        arr = this.shuffle(arr);
                        console.log(arr);
                        switch (goal){
                            default:
                                let state = this.state;
                                state.randomOutput1 = arr;
                                state.phase = goal;
                                this.setState(state);
                                break;
                            case "random1":
                                let state1 = this.state;
                                state1.randomOutput1 = arr;
                                state1.phase = goal;
                                this.setState(state1);
                                break;
                            case "random2" :
                                let state2 = this.state;
                                state2.randomOutput2 = arr;
                                state2.phase = goal;
                                this.setState(state2);
                                break;
                            case "random3" :
                                let state3 = this.state;
                                state3.randomOutput3 = arr;
                                state3.phase = goal;
                                this.setState(state3);
                                break;
                            case "branchrand1" :
                                let state4 = this.state;
                                state4.randomOutput4 = arr;
                                state4.phase = goal;
                                this.setState(state4);
                                break;
                            case "branchrand2" :
                                let state5 = this.state;
                                state5.randomOutput5 = arr;
                                state5.phase = goal;
                                this.setState(state5);
                                break;
                        }
                    })
                }
            }
            else {
                switch (goal){
                    default:
                        let state = this.state;
                        state.randomOutput1 = arr;
                        state.phase = goal;
                        this.setState(state);
                        break;
                    case "random1":
                        let state1 = this.state;
                        state1.randomOutput1 = arr;
                        state1.phase = goal;
                        this.setState(state1);
                        break;
                    case "random2" :
                        let state2 = this.state;
                        state2.randomOutput2 = arr;
                        state2.phase = goal;
                        this.setState(state2);
                        break;
                    case "random3" :
                        let state3 = this.state;
                        state3.randomOutput3 = arr;
                        state3.phase = goal;
                        this.setState(state3);
                        break;
                    case "branchrand1" :
                        let state4 = this.state;
                        state4.randomOutput4 = arr;
                        state4.phase = goal;
                        this.setState(state4);
                        break;
                    case "branchrand2" :
                        let state5 = this.state;
                        state5.randomOutput5 = arr;
                        state5.phase = goal;
                        this.setState(state5);
                        break;
                }
            }
        }
        else {
            //do everything inside this, we know that we have at least 3 literals.
            if (simplification === "Fully simplified") {
                //Simplification has no literals or clauses.
                //I should generate an arbitrary cardinality less than that of the string
                //Then make all the results of that cardinality.
                if (type === "clause") {
                    let c = Math.floor(Math.random() * (clauses.length - 1)) + 1;
                    axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/reduceRandomiseClauses?CNF="+string+"&simplification="+simplification+"&cardinality="+c+"&rem="+rem).then(r => {
                        arr = this.shuffle(arr.concat(r.data.split(',')));
                        console.log(arr);
                        switch (goal){
                            default:
                                let state = this.state;
                                state.randomOutput1 = arr;
                                state.phase = goal;
                                this.setState(state);
                                break;
                            case "random1":
                                let state1 = this.state;
                                state1.randomOutput1 = arr;
                                state1.phase = goal;
                                this.setState(state1);
                                break;
                            case "random2" :
                                let state2 = this.state;
                                state2.randomOutput2 = arr;
                                state2.phase = goal;
                                this.setState(state2);
                                break;
                            case "random3" :
                                let state3 = this.state;
                                state3.randomOutput3 = arr;
                                state3.phase = goal;
                                this.setState(state3);
                                break;
                            case "branchrand1" :
                                let state4 = this.state;
                                state4.randomOutput4 = arr;
                                state4.phase = goal;
                                this.setState(state4);
                                break;
                            case "branchrand2" :
                                let state5 = this.state;
                                state5.randomOutput5 = arr;
                                state5.phase = goal;
                                this.setState(state5);
                                break;
                        }
                    })


                } else if (type === "literal") {
                    let c = Math.floor(Math.random() * (literals.length - 1)) + 1;
                    axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/reduceRandomiseLiterals?CNF="+string+"&simplification="+simplification+"&cardinality="+c+"&rem="+rem).then(r => {
                        arr = this.shuffle(arr.concat(r.data.split(',')));
                        console.log(arr);
                        switch (goal){
                            default:
                                let state = this.state;
                                state.randomOutput1 = arr;
                                state.phase = goal;
                                this.setState(state);
                                break;
                            case "random1":
                                let state1 = this.state;
                                state1.randomOutput1 = arr;
                                state1.phase = goal;
                                this.setState(state1);
                                break;
                            case "random2" :
                                let state2 = this.state;
                                state2.randomOutput2 = arr;
                                state2.phase = goal;
                                this.setState(state2);
                                break;
                            case "random3" :
                                let state3 = this.state;
                                state3.randomOutput3 = arr;
                                state3.phase = goal;
                                this.setState(state3);
                                break;
                            case "branchrand1" :
                                let state4 = this.state;
                                state4.randomOutput4 = arr;
                                state4.phase = goal;
                                this.setState(state4);
                                break;
                            case "branchrand2" :
                                let state5 = this.state;
                                state5.randomOutput5 = arr;
                                state5.phase = goal;
                                this.setState(state5);
                                break;
                        }
                    })

                }

            } else {
                //I have the number of simplification's clauses and literals.
                //I can use that to generate a result of the same cardinality.
                if (type === "clause") {
                    let c = clauses.length - simpClauses.length;
                    console.log("boutta do it "+c);
                    axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/reduceRandomiseClauses?CNF="+string+"&simplification="+simplification+"&cardinality="+c+"&rem="+rem).then(r => {
                        console.log("Received from API: "+r.data)
                        arr = this.shuffle(arr.concat(r.data.split(',')));
                        console.log(arr);
                        switch (goal){
                            default:
                                let state = this.state;
                                state.randomOutput1 = arr;
                                state.phase = goal;
                                this.setState(state);
                                break;
                            case "random1":
                                let state1 = this.state;
                                state1.randomOutput1 = arr;
                                state1.phase = goal;
                                this.setState(state1);
                                break;
                            case "random2" :
                                let state2 = this.state;
                                state2.randomOutput2 = arr;
                                state2.phase = goal;
                                this.setState(state2);
                                break;
                            case "random3" :
                                let state3 = this.state;
                                state3.randomOutput3 = arr;
                                state3.phase = goal;
                                this.setState(state3);
                                break;
                            case "branchrand1" :
                                let state4 = this.state;
                                state4.randomOutput4 = arr;
                                state4.phase = goal;
                                this.setState(state4);
                                break;
                            case "branchrand2" :
                                let state5 = this.state;
                                state5.randomOutput5 = arr;
                                state5.phase = goal;
                                this.setState(state5);
                                break;
                        }
                    })

                } else if (type === "literal") {
                    console.log("this one?")
                    console.log("Lit: "+literals)
                    console.log("SimpLit: "+simpLiterals)
                    let c = literals.length - simpLiterals.length;
                    axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/reduceRandomiseLiterals?CNF="+string+"&simplification="+simplification+"&cardinality="+c+"&rem="+rem).then(r => {
                        arr = this.shuffle(arr.concat(r.data.split(',')));
                        console.log(arr)
                        switch (goal){
                            default:
                                let state = this.state;
                                state.randomOutput1 = arr;
                                state.phase = goal;
                                this.setState(state);
                                break;
                            case "random1":
                                let state1 = this.state;
                                state1.randomOutput1 = arr;
                                state1.phase = goal;
                                this.setState(state1);
                                break;
                            case "random2" :
                                let state2 = this.state;
                                state2.randomOutput2 = arr;
                                state2.phase = goal;
                                this.setState(state2);
                                break;
                            case "random3" :
                                let state3 = this.state;
                                state3.randomOutput3 = arr;
                                state3.phase = goal;
                                this.setState(state3);
                                break;
                            case "branchrand1" :
                                let state4 = this.state;
                                state4.randomOutput4 = arr;
                                state4.phase = goal;
                                this.setState(state4);
                                break;
                            case "branchrand2" :
                                let state5 = this.state;
                                state5.randomOutput5 = arr;
                                state5.phase = goal;
                                this.setState(state5);
                                break;
                        }
                    })

                }

            }
        }
    }

    randomiseLiterals(simplification, string, goal){
        console.log("Simplification: "+simplification)
        let clauses = this.parseInput(string);
        let literals = this.getLiterals(clauses);
        let simpClauses = this.parseInput(simplification);
        let simpLiterals = this.getLiterals(simpClauses);
        //We let clauses = 0, literals = 1.
        let rem;
        //let lock = 0;
        console.log("SimpClauses: "+simpClauses)
        console.log("SimpLiterals: "+simpLiterals)

        //TODO consider dem cases innit
        // I need to take the unsimplified string and using the correct string, create some fake clause simplifications.
        // Two conditions - is it a clause simplification or a literal simplification?
        // The former implies we select some number from the set of clauses to remove - randomise the set.
        // Then divide into three (or two) equal parts.
        // For each new false simplification, use one of those subsets.
        // This only applies for n>=3 though

        //Construct array
        // No case for when simplification is both string and "" (we should capture that case and send to victory)
        let arr;
        console.log("Randomise string: "+string);
        console.log("Randomise simplification: "+simplification);

        if(simplification === "Fully simplified"){
            arr = ["Fully simplified"];
            rem = 3;
        } else {
            arr = [simplification, "Fully simplified"];
            rem = 2;
        }
        console.log("Rem: "+rem)
        console.log("Length of clauses: "+clauses.length)

        //The odd cases first
        if(literals.length < 3){
            console.log("String: "+string)
            console.log("arr: "+arr)
            arr = arr.concat(string)
            if(literals.length === 2){
                // a lot harder, I need to reduce all the clauses on each literal
                // I'll do an api call for this perhaps
                axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/reduceRandomiseBaby?CNF="+string+"&simp="+simplification+"&rem="+rem).then(r => {
                    if(r.data.includes(",")){
                        arr.concat(r.data.split(","));
                    } else arr.concat([r.data]);
                    arr = this.shuffle(arr);
                    console.log(arr);
                    switch (goal){
                        default:
                            let state = this.state;
                            state.randomOutput1 = arr;
                            state.phase = goal;
                            this.setState(state);
                            break;
                        case "random1":
                            let state1 = this.state;
                            state1.randomOutput1 = arr;
                            state1.phase = goal;
                            this.setState(state1);
                            break;
                        case "random2" :
                            let state2 = this.state;
                            state2.randomOutput2 = arr;
                            state2.phase = goal;
                            this.setState(state2);
                            break;
                        case "random3" :
                            let state3 = this.state;
                            state3.randomOutput3 = arr;
                            state3.phase = goal;
                            this.setState(state3);
                            break;
                        case "branchrand1" :
                            let state4 = this.state;
                            state4.randomOutput4 = arr;
                            state4.phase = goal;
                            this.setState(state4);
                            break;
                        case "branchrand2" :
                            let state5 = this.state;
                            state5.randomOutput5 = arr;
                            state5.phase = goal;
                            this.setState(state5);
                            break;
                    }
                })
            }
            else {
                switch (goal){
                    default:
                        let state = this.state;
                        state.randomOutput1 = arr;
                        state.phase = goal;
                        this.setState(state);
                        break;
                    case "random1":
                        let state1 = this.state;
                        state1.randomOutput1 = arr;
                        state1.phase = goal;
                        this.setState(state1);
                        break;
                    case "random2" :
                        let state2 = this.state;
                        state2.randomOutput2 = arr;
                        state2.phase = goal;
                        this.setState(state2);
                        break;
                    case "random3" :
                        let state3 = this.state;
                        state3.randomOutput3 = arr;
                        state3.phase = goal;
                        this.setState(state3);
                        break;
                    case "branchrand1" :
                        let state4 = this.state;
                        state4.randomOutput4 = arr;
                        state4.phase = goal;
                        this.setState(state4);
                        break;
                    case "branchrand2" :
                        let state5 = this.state;
                        state5.randomOutput5 = arr;
                        state5.phase = goal;
                        this.setState(state5);
                        break;
                }
            }
        }
        else {
            //do everything inside this, we know that we have at least 3 literals.
            if (simplification === "Fully simplified") {
                //Simplification has no literals or clauses.
                //I should generate an arbitrary cardinality less than that of the string
                //Then make all the results of that cardinality.
                let c = Math.floor(Math.random() * (literals.length - 1)) + 1;
                axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/reduceRandomiseLiterals?CNF="+string+"&simplification="+simplification+"&cardinality="+c+"&rem="+rem).then(r => {
                    arr = this.shuffle(arr.concat(r.data.split(',')));
                    console.log(arr);
                    switch (goal){
                        default:
                            let state = this.state;
                            state.randomOutput1 = arr;
                            state.phase = goal;
                            this.setState(state);
                            break;
                        case "random1":
                            let state1 = this.state;
                            state1.randomOutput1 = arr;
                            state1.phase = goal;
                            this.setState(state1);
                            break;
                        case "random2" :
                            let state2 = this.state;
                            state2.randomOutput2 = arr;
                            state2.phase = goal;
                            this.setState(state2);
                            break;
                        case "random3" :
                            let state3 = this.state;
                            state3.randomOutput3 = arr;
                            state3.phase = goal;
                            this.setState(state3);
                            break;
                        case "branchrand1" :
                            let state4 = this.state;
                            state4.randomOutput4 = arr;
                            state4.phase = goal;
                            this.setState(state4);
                            break;
                        case "branchrand2" :
                            let state5 = this.state;
                            state5.randomOutput5 = arr;
                            state5.phase = goal;
                            this.setState(state5);
                            break;
                    }
                })
            } else {
                //I have the number of simplification's clauses and literals.
                //I can use that to generate a result of the same cardinality.
                console.log("this one?")
                console.log("Lit: "+literals)
                console.log("SimpLit: "+simpLiterals)
                let c = literals.length - simpLiterals.length;
                axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/reduceRandomiseLiterals?CNF="+string+"&simplification="+simplification+"&cardinality="+c+"&rem="+rem).then(r => {
                    arr = this.shuffle(arr.concat(r.data.split(',')));
                    console.log(arr)
                    switch (goal){
                        default:
                            let state = this.state;
                            state.randomOutput1 = arr;
                            state.phase = goal;
                            this.setState(state);
                            break;
                        case "random1":
                            let state1 = this.state;
                            state1.randomOutput1 = arr;
                            state1.phase = goal;
                            this.setState(state1);
                            break;
                        case "random2" :
                            let state2 = this.state;
                            state2.randomOutput2 = arr;
                            state2.phase = goal;
                            this.setState(state2);
                            break;
                        case "random3" :
                            let state3 = this.state;
                            state3.randomOutput3 = arr;
                            state3.phase = goal;
                            this.setState(state3);
                            break;
                        case "branchrand1" :
                            let state4 = this.state;
                            state4.randomOutput4 = arr;
                            state4.phase = goal;
                            this.setState(state4);
                            break;
                        case "branchrand2" :
                            let state5 = this.state;
                            state5.randomOutput5 = arr;
                            state5.phase = goal;
                            this.setState(state5);
                            break;
                    }
                })
            }
        }
    }


    randomiseClauses(simplification, string, goal){
        console.log("Simplification: "+simplification)
        let clauses = this.parseInput(string);
        let simpClauses = this.parseInput(simplification);
        //We let clauses = 0, literals = 1.
        let rem;
        //let lock = 0;
        console.log("SimpClauses: "+simpClauses)

        //TODO consider dem cases innit
        // I need to take the unsimplified string and using the correct string, create some fake clause simplifications.
        // Two conditions - is it a clause simplification or a literal simplification?
        // The former implies we select some number from the set of clauses to remove - randomise the set.
        // Then divide into three (or two) equal parts.
        // For each new false simplification, use one of those subsets.
        // This only applies for n>=3 though

        //Construct array
        // No case for when simplification is both string and "" (we should capture that case and send to victory)
        let arr;
        console.log("Randomise string: "+string);
        console.log("Randomise simplification: "+simplification);

        if(simplification === "Fully simplified"){
            arr = ["Fully simplified"];
            rem = 3;
        } else {
            arr = [simplification, "Fully simplified"];
            rem = 2;
        }
        console.log("Rem: "+rem)
        console.log("Length of clauses: "+clauses.length)

        //The odd cases first
        if(clauses.length < 3){
            console.log("String: "+string)
            console.log("arr: "+arr)
            arr = arr.concat(string)
            if(clauses.length === 2){
                if(rem === 2){
                    //Need to bracket all elements in clauses before adding.
                    console.log("Removed element: "+arr.shift())
                    console.log("Array now: "+arr)
                    for(let i=0;i<2;i++)
                        arr = arr.concat("("+clauses[i]+")")
                    //arr = arr.concat(clauses)
                    console.log("Clauses: "+clauses)
                    console.log("Array now: "+arr)
                    //arr = arr.concat(string)
                    arr = this.shuffle(arr);
                    console.log("Array after shuffle: "+arr)
                }
                else{
                    arr.concat(clauses)
                }
                switch (goal){
                    default:
                        let state = this.state;
                        state.randomOutput1 = arr;
                        state.phase = goal;
                        this.setState(state);
                        break;
                    case "random1":
                        let state1 = this.state;
                        state1.randomOutput1 = arr;
                        state1.phase = goal;
                        this.setState(state1);
                        break;
                    case "random2" :
                        let state2 = this.state;
                        state2.randomOutput2 = arr;
                        state2.phase = goal;
                        this.setState(state2);
                        break;
                    case "random3" :
                        let state3 = this.state;
                        state3.randomOutput3 = arr;
                        state3.phase = goal;
                        this.setState(state3);
                        break;
                    case "branchrand1" :
                        let state4 = this.state;
                        state4.randomOutput4 = arr;
                        state4.phase = goal;
                        this.setState(state4);
                        break;
                    case "branchrand2" :
                        let state5 = this.state;
                        state5.randomOutput5 = arr;
                        state5.phase = goal;
                        this.setState(state5);
                        break;
                }
            }
            else {
                switch (goal){
                    default:
                        let state = this.state;
                        state.randomOutput1 = arr;
                        state.phase = goal;
                        this.setState(state);
                        break;
                    case "random1":
                        let state1 = this.state;
                        state1.randomOutput1 = arr;
                        state1.phase = goal;
                        this.setState(state1);
                        break;
                    case "random2" :
                        let state2 = this.state;
                        state2.randomOutput2 = arr;
                        state2.phase = goal;
                        this.setState(state2);
                        break;
                    case "random3" :
                        let state3 = this.state;
                        state3.randomOutput3 = arr;
                        state3.phase = goal;
                        this.setState(state3);
                        break;
                    case "branchrand1" :
                        let state4 = this.state;
                        state4.randomOutput4 = arr;
                        state4.phase = goal;
                        this.setState(state4);
                        break;
                    case "branchrand2" :
                        let state5 = this.state;
                        state5.randomOutput5 = arr;
                        state5.phase = goal;
                        this.setState(state5);
                        break;
                }
            }
        }
        else {
            //do everything inside this, we know that we have at least 3 literals.
            if (simplification === "Fully simplified") {
                //Simplification has no literals or clauses.
                //I should generate an arbitrary cardinality less than that of the string
                //Then make all the results of that cardinality.
                let c = Math.floor(Math.random() * (clauses.length - 1)) + 1;
                axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/reduceRandomiseClauses?CNF="+string+"&simplification="+simplification+"&cardinality="+c+"&rem="+rem).then(r => {
                    arr = this.shuffle(arr.concat(r.data.split(',')));
                    console.log(arr);
                    switch (goal){
                        default:
                            let state = this.state;
                            state.randomOutput1 = arr;
                            state.phase = goal;
                            this.setState(state);
                            break;
                        case "random1":
                            let state1 = this.state;
                            state1.randomOutput1 = arr;
                            state1.phase = goal;
                            this.setState(state1);
                            break;
                        case "random2" :
                            let state2 = this.state;
                            state2.randomOutput2 = arr;
                            state2.phase = goal;
                            this.setState(state2);
                            break;
                        case "random3" :
                            let state3 = this.state;
                            state3.randomOutput3 = arr;
                            state3.phase = goal;
                            this.setState(state3);
                            break;
                        case "branchrand1" :
                            let state4 = this.state;
                            state4.randomOutput4 = arr;
                            state4.phase = goal;
                            this.setState(state4);
                            break;
                        case "branchrand2" :
                            let state5 = this.state;
                            state5.randomOutput5 = arr;
                            state5.phase = goal;
                            this.setState(state5);
                            break;
                    }
                })
            } else {
                //I have the number of simplification's clauses and literals.
                //I can use that to generate a result of the same cardinality.
                let c = clauses.length - simpClauses.length;
                console.log("boutta do it "+c);
                axios.get("http://javarestapitest-env-1.eba-4mxzy2sm.eu-west-2.elasticbeanstalk.com/api/reduceRandomiseClauses?CNF="+string+"&simplification="+simplification+"&cardinality="+c+"&rem="+rem).then(r => {
                    console.log("Received from API: "+r.data)
                    arr = this.shuffle(arr.concat(r.data.split(',')));
                    console.log(arr);
                    switch (goal){
                        default:
                            let state = this.state;
                            state.randomOutput1 = arr;
                            state.phase = goal;
                            this.setState(state);
                            break;
                        case "random1":
                            let state1 = this.state;
                            state1.randomOutput1 = arr;
                            state1.phase = goal;
                            this.setState(state1);
                            break;
                        case "random2" :
                            let state2 = this.state;
                            state2.randomOutput2 = arr;
                            state2.phase = goal;
                            this.setState(state2);
                            break;
                        case "random3" :
                            let state3 = this.state;
                            state3.randomOutput3 = arr;
                            state3.phase = goal;
                            this.setState(state3);
                            break;
                        case "branchrand1" :
                            let state4 = this.state;
                            state4.randomOutput4 = arr;
                            state4.phase = goal;
                            this.setState(state4);
                            break;
                        case "branchrand2" :
                            let state5 = this.state;
                            state5.randomOutput5 = arr;
                            state5.phase = goal;
                            this.setState(state5);
                            break;
                    }
                })
            }
        }
    }

    displaySelections(){
        let temp = "";
        for(let i= 0; i < this.state.selections.length; i++) {
            temp += this.state.selections[i];
        }
        return temp;
    }

    assignmentAction(button, bool){
        let state = this.state;
        state.assignments[state.selections.indexOf(button)] = bool
        this.setState(state);
    }

    getAssignments(){
        let str = "";
        for(let i=0;i<this.state.assignments.length;i++){
            if(this.state.assignments[i]){
                str += "1";
                //str += ",";
            }
            else{
                str += "0";
                //str += ",";
            }
        }
        return str;
    }

    componentDidMount() {
        let state = this.state;
        state.clauses = this.parseInput(state.parsedString);
        state.literals = this.getLiterals(state.clauses);
        state.phase = this.props.phase;
        state.headingPhase = this.props.phase;
        state.pleliterals = this.getLiterals(state.clauses)
        console.log(state.pleliterals);
        this.setState(state);
        //this.getLiterals();
        //this.getVariables();

    }

        render() {
            switch (this.state.phase) {
                default :
                    return(
                        <div>
                            <h3>Current CNF: {this.state.parsedString}</h3>
                            <p id={"heading"}>{this.heading()}</p>
                            <ButtonGroup buttons={this.state.clauses} doSomethingAfterClick={this.buttonAction}></ButtonGroup>
                            <Button onClick={this.submission}>Submit</Button>
                            <p>Selections: {this.state.selections.length}</p>
                        </div>
                    )
                case "2" :
                    //For this one, I need a different structure - can't remove clauses.
                    return(
                        <div>
                            <h3>Current CNF: {this.state.parsedString}</h3>
                            <p id={"heading"}>{this.heading()}</p>
                            <AssignmentGroup buttons={this.state.selections} doSomethingAfterClick={this.assignmentAction}></AssignmentGroup>
                            <Button onClick={this.submission}>Submit</Button>
                            <p>Assignments: {this.getAssignments()}</p>
                        </div>
                    )
                case "3" :
                    return(
                        <div>
                            <h3>Current CNF: {this.state.parsedString}</h3>
                            <p id={"heading"}>{this.heading()}</p>
                            <ButtonGroup buttons={this.state.clauses} doSomethingAfterClick={this.buttonAction}></ButtonGroup>
                            <Button onClick={this.submission}>Submit</Button>
                            <p>Selections: {this.state.selections3.length}</p>
                        </div>
                    )
                case "random1" :
                    return(
                        <div>
                            <h3>Current CNF: {this.state.parsedString}</h3>
                            <p id={"heading"}>{this.heading()}</p>
                            <ButtonGroup buttons={this.state.randomOutput1} doSomethingAfterClick={this.buttonAction}></ButtonGroup>
                            <p>Selections: {this.state.selections3.length}</p>
                        </div>
                    )
                case "4" :
                    //This one is literals of the negations.ss
                    return(
                        <div>
                            <h3>Current CNF: {this.state.threeString}</h3>
                            <p id={"heading"}>{this.heading()}</p>
                            <ButtonGroup buttons={this.state.literals4} doSomethingAfterClick={this.buttonAction}></ButtonGroup>
                            <Button onClick={this.submission}>Submit</Button>
                            <p>Selections: {this.state.selections4.length}</p>
                        </div>
                    )
                case "random2" :
                    return(
                        <div>
                            <h3>Current CNF: {this.state.threeString}</h3>
                            <p id={"heading"}>{this.heading()}</p>
                            <ButtonGroup buttons={this.state.randomOutput2} doSomethingAfterClick={this.buttonAction}></ButtonGroup>
                            <p>Selections: {this.state.selections4.length}</p>
                        </div>
                    )
                case "ple1" :
                    //For this one, I need a different structure - can't remove clauses.
                    return(
                        <div>
                            <h3>Current CNF: {this.state.parsedString}</h3>
                            <p id={"heading"}>{this.heading()}</p>
                            <ButtonGroup buttons={this.state.pleliterals} doSomethingAfterClick={this.buttonAction}></ButtonGroup>
                            <Button onClick={this.submission}>Submit</Button>
                            <p>Selections: {this.state.pleselections1.length}</p>
                            <p>ParsedString: {this.state.parsedString}</p>
                            <p>Plestring: {this.state.pleString}</p>
                        </div>
                    )
                case "ple2" :
                    //For this one, I need a different structure - can't remove clauses.
                    return(
                        <div>
                            <h3>Current CNF: {this.state.parsedString}</h3>
                            <p id={"heading"}>{this.heading()}</p>
                            <ButtonGroup buttons={this.state.clauses} doSomethingAfterClick={this.buttonAction}></ButtonGroup>
                            <Button onClick={this.submission}>Submit</Button>
                            <p>Selections: {this.state.pleselections2.length}</p>
                            <p>ParsedString: {this.state.parsedString}</p>
                            <p>Plestring: {this.state.pleString}</p>
                        </div>
                    )
                case "random3" :
                    return(
                        <div>
                            <h3>Current CNF: {this.state.parsedString}</h3>
                            <p id={"heading"}>{this.heading()}</p>
                            <ButtonGroup buttons={this.state.randomOutput3} doSomethingAfterClick={this.buttonAction}></ButtonGroup>
                            <p>Selections: {this.state.pleselections2.length}</p>
                            <p>ParsedString: {this.state.parsedString}</p>
                            <p>Plestring: {this.state.pleString}</p>
                        </div>
                    )
                case "branch1" :
                    return(
                        <div>
                            <h3>Current CNF: {this.state.parsedString}</h3>
                            <p id={"heading"}>{this.heading()}</p>
                            <ButtonGroup buttons={this.state.clauses} doSomethingAfterClick={this.buttonAction}></ButtonGroup>
                            <Button onClick={this.submission}>Submit</Button>
                            <p>Selections: {this.state.branchselections1.length}</p>
                        </div>
                    )
                case "branch2" :
                    return(
                        <div>
                            <h3>Current CNF: {this.state.branch1string}</h3>
                            <p id={"heading"}>{this.heading()}</p>
                            <ButtonGroup buttons={this.state.literals4} doSomethingAfterClick={this.buttonAction}></ButtonGroup>
                            <Button onClick={this.submission}>Submit</Button>
                            <p>Selections: {this.state.branchselections2.length}</p>
                            </div>
                    )
                case "branchrand1" :
                    return(
                        <div>
                            <h3>Current CNF: {this.state.parsedString}</h3>
                            <p id={"heading"}>{this.heading()}</p>
                            <ButtonGroup buttons={this.state.randomOutput4} doSomethingAfterClick={this.buttonAction}></ButtonGroup>
                            <p>Selections: {this.state.branchselections1.length}</p>
                        </div>
                    )
                case "branchrand2" :
                    return(
                        <div>
                            <h3>Current CNF: {this.state.branch1string}</h3>
                            <p id={"heading"}>{this.heading()}</p>
                            <ButtonGroup buttons={this.state.randomOutput5} doSomethingAfterClick={this.buttonAction}></ButtonGroup>
                            <p>Selections: {this.state.branchselections2.length}</p>
                        <p>{this.state.branch2string}</p>
                            <p>{this.state.branch2choice}</p>
                        </div>
                    )


            }
        }
    }

    export default Identify;