import React from "react";
import {Button, TextField} from "@mui/material";
import Identify from "./Identify";
import Branch from "./variableBranch";

class BackBranchWrapper extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            parsedString: this.props.string,
            phase: 2,
            button: this.props.button,
            selection: this.props.selection,
            correct: "",
            brancher: this.props.brancher
        };

        this.phase2 = this.phase2.bind(this);
        this.passChoice = this.passChoice.bind(this);
        this.setChoice = this.setChoice.bind(this);

    }

    passChoice(b, s, c) {
        let state = this.state;
        state.button = b;
        state.selection = s;
        state.correct = c;
        if(s)
            state.brancher = b;
        else
            state.brancher = ("¬"+b);
        this.setState(state);
    }

    setChoice(){
        this.props.handleBranch(this.state.button, this.state.selection);
    }

    phase2(){
        let state = this.state;
        state.phase = "2";
        this.setState(state);
    }

    render() {
        return(
                    <div>
                        <Identify phase={"branch1"}
                                  prepareBranch={this.props.prepareBranch}
                                  string={this.state.parsedString}
                                  handleBranch = {this.props.handleBranch}
                                  sendData={this.props.sendData}
                                  doUP={this.props.doUP}
                                  doPLE={this.props.doPLE}
                                  detectBranchFailure={this.props.detectBranchFailure}
                                  doSuccess = {this.props.doSuccess}
                                  setChoice={this.setChoice}
                                  brancher={this.state.brancher}
                                  handleConflict = {this.props.handleConflict}
                                  addPoints = {this.props.addPoints}
                                  wrongAnswerPenalty = {this.props.wrongAnswerPenalty}>
                        </Identify>
                    </div>
        )
    }
}

export default BackBranchWrapper;