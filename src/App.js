import './styles/App.scss';
import React, {createRef, useEffect, useState} from "react";
import Entry from './components/cnfEntry'
import BackBranchWrapper from './components/backbranchWrapper'
//import Branch from "./components/variableBranch";
//import TreeNode from "./components/Node";
import Root from "./components/Root";
import {BinaryTreeNode, drawBinaryTree, setTheme, VisualizationType} from "binary-tree-visualizer";
//import Failure from "./components/Failure";
//import Success from "./components/Success";
//import Backtrack from "./components/Backtrack";
//import axios from "axios";
import UP from "./components/UP";
import PLE from "./components/PLE";
import BranchWrapper from "./components/branchWrapper";
//import {render} from "@testing-library/react";
import ModeChooser from "./components/modeChooser";
import TreeView from "./components/treeView";
//import {render} from "@testing-library/react";
import Login from "./components/Login";
import Cookies from "universal-cookie";
import {LinearProgress, TextField} from "@mui/material";
import Endings from "./components/Endings";
//import success from "./components/Success";
import Shop from "./components/Shop";
import Settings from "./components/Settings";
import MultiplayerWrapper from "./components/multiplayer/MultiplayerWrapper";
//import WebSocketTester2 from "./components/WebSocketTester2";
import { GiTwoCoins } from "react-icons/gi";
import BranchWrapper2 from "./components/branchWrapper2";
import Branch2 from "./components/variableBranch2";
import Challenge from "./components/Challenge";
import Info from "./components/Info";

function App() {
    let cookies = new Cookies();
    let tempRoot = new Root();
    tempRoot.variable = "Current";
    let childRef = createRef();
    document.documentElement.setAttribute("data-theme", cookies.get("theme"));

    const [parsedString, setParsedString] = useState("");
    const [currentComponent, setCurrentComponent] = useState("chooser")
    const [root, setRoot] = useState(tempRoot);
    const [canvasState, setCanvasState] = useState(0);
    const [loginState, setLoginState] = useState(1);
    const [multiplayerComponent, setMultiplayerComponent] = useState("lobbySelect")
    const [initLobby, setInitLobby] = useState(0)

    const [deacPassword, setDeacPassword] = useState("");
    const [deactivateIncorrectError, setdeactivateIncorrectError] = useState(false);
    const [deactivateIncorrectText, setdeactivateIncorrectText] = useState("");

    const [username, setusername] = useState("Guest");
    const [userid, setuserid] = useState(-1);
    const [xp, setxp] = useState(0);
    const [level, setlevel] = useState(0);
    const [coins, setcoins] = useState(0);

    const [button, setbutton] = useState("");
    const [selection, setselection] = useState(false);
    const [brancher, setbrancher] = useState("");

    const [wrongAnswers, setWrongAnswers] = useState(0);
    const [backtracks, setBacktracks] = useState(0);
    const [rawPoints, setRawPoints] = useState(0);
    const [currentPoints, setCurrentPoints] = useState(0);
    //const [finalPoints, setFinalPoints] = useState(0);

    const [failureReason, setFailureReason] = useState("");

    const [fromEnding, setFromEnding] = useState(0);

    const [currentlyPlaying, setCurrentlyPlaying] = useState(0);

    const [dataSent, setDataSent] = useState("failed");
    const [dataErrorMessage, setDataErrorMessage] = useState("");
    const [notLogged, setNotLogged] = useState(0);
    const [difficulty, setDifficulty] = useState(0);

    useEffect(() => {
        // here I am guaranteed to get the updated tree
        // since I waited for it to change.
        renderTree();
        // here we added tree to the dependency array to
        // listen for changes to that state variable.
    }, [renderTree, root]);

    async function addPoints(num){
        await setCurrentPoints(currentPoints + num);
        await setRawPoints(rawPoints + num);
        console.log("Added "+num+" points to total.")
    }

    /*function assignFinalPointsSuccess(){
        setFinalPoints(currentPoints - (30*backtracks))
    }
    function assignFinalPointsFailure(){
        setFinalPoints(currentPoints)
    }*/

    function wrongAnswerPenalty() {
        setWrongAnswers(wrongAnswers + 1);
        if(currentPoints < 5){
            setCurrentPoints(0)
        } else setCurrentPoints(currentPoints - 5)
    }


    function constructSubtree(subtree){
        console.log("variable value:"+subtree.variable);

        let tree= new BinaryTreeNode(subtree.variable);
        //I want to check the contents of the current node's children.
        //Are there no children?
        if((subtree.varTrue === undefined) && (subtree.varFalse === undefined)){
            return tree;
        }
        //So there must be children. Which one(s)?
        else{
            if(subtree.varTrue === undefined){
                if(subtree.varFalse.variable === ""){
                    tree.right = new BinaryTreeNode("null");
                }
                else{
                    tree.right = constructSubtree(subtree.varFalse)
                }
            }
            else if(subtree.varFalse === undefined){
                if(subtree.varTrue.variable === ""){
                    tree.left = new BinaryTreeNode("null");
                }
                else{
                    tree.left = constructSubtree(subtree.varTrue)
                }
            }
            else{
                if(subtree.varTrue.variable === ""){
                    if(subtree.varFalse.variable === ""){
                        tree.left = new BinaryTreeNode("null")
                        tree.right = new BinaryTreeNode("null")
                    }
                    else{
                        tree.left = new BinaryTreeNode("null")
                        tree.right = constructSubtree(subtree.varFalse)
                    }
                }
                else{
                    if(subtree.varFalse.variable === ""){
                        tree.left = constructSubtree(subtree.varTrue);
                        tree.right = new BinaryTreeNode("null")
                    }
                    else{
                        tree.left = constructSubtree(subtree.varTrue);
                        tree.right = constructSubtree(subtree.varFalse);
                    }
                }
            }
        }

        return tree;
    }
    function renderTree() {
        if(canvasState !== 0){
            //change tree color depending on theme
            let bgStroke = (getComputedStyle(document.body).getPropertyValue('--treeStroke'))
            let bgBorder = (getComputedStyle(document.body).getPropertyValue('--treeBorder'))
            let bgColor = (getComputedStyle(document.body).getPropertyValue('--treeBg'))
            console.log(bgColor)
            setTheme({
                strokeColor : bgStroke,
                colorArray : [{
                    borderColor : bgBorder,
                    bgColor: bgColor
                }]
            })
            console.log("Rendering tree.")
            //I need to map my concept of a "tree" with the tree here.
            let tree;
            /*
                    let r = root;
                    r.setString("Stringy")
                    r.current.branchLeft("a (root)");
                    r.current.branchLeft("b");
                    r.current.branchLeft("c");
                    console.log("root value1:" + r.variable);
                    r.current.backtrack();
                    console.log("root value2:" + r.variable);
                    r.current.backtrack();
                    console.log("root value3:" + r.variable);
                    r.current.branchLeft("d");
                    r.current.backtrack();
                    console.log("root value4:" + r.variable);
                    r.current.backtrack();
                    console.log("root value5:" + r.variable);
                    r.current.branchLeft("e");
                    setRoot(r);
                    console.log("root value6:" + r.variable);
            */
            //build tree

            if(root.string === ""){
                tree = null;
                console.log("Tree is null.")
            }
            else{
                tree = new BinaryTreeNode(root.variable);
                if(root.varTrue != null){
                    console.log("constructing left subtree")
                    tree.left = constructSubtree(root.varTrue);
                }
                if(root.varFalse != null){
                    console.log("constructing right subtree")
                    tree.right = constructSubtree(root.varFalse);
                }
            }

            //draw tree
            if (tree != null){
                return drawBinaryTree(tree, document.getElementById("can"), {
                    type: VisualizationType.SIMPLE,
                    //maxHeight: (document.getElementById("candiv").height),
                    //maxWidth: (document.getElementById("candiv").width)
                    maxHeight: (0.5 * window.innerHeight),
                    maxWidth: (0.5 * window.innerWidth),
                });
            }
            else return null;
        }
        return null;
    }

    function handleBranch(variable, bool){
        let r = root;
        if(bool){
            r.current.branchLeft(variable)
        }
        else{
            r.current.branchRight(variable)
        }
        setRoot(r);
        renderTree();
    }

    function handleBranchMP(brancher){
        let r = root;
        let variable = brancher.slice(1);
        console.log(variable);
        if(brancher.charAt(0) === '¬'){
            r.current.branchRight(variable)
        }
        else{
            r.current.branchLeft(brancher)
        }
        setRoot(r);
        renderTree();
    }

    function handleBacktrackMP(){
        let rot = root;
        rot.current.variable = "Failure";
        rot.current.backtrack();
        renderTree();
        setRoot(rot);
    }

    /*
    async function startTreeMPHelper(){
        let rot = root;
        rot.current.string = "MultiplayerPlaceholder";
        await setRoot(rot);
        await setCanvasState(1);
    }

    async function startTreeMP(){
        startTreeMPHelper().then(() => {renderTree()});
    }*/
    function toggleCanvasMP(){
        let rot = root;
        rot.string = "MultiplayerPlaceholder";
        setRoot(rot);
        setCanvasState(1);
    }

    function handleBacktrack(){
        setBacktracks(backtracks + 1);

        let rot = root;
        rot.current.variable = "Failure";
        //rot.current.backtrack();
        //Desired return:
        //String of the parent of the current node
        //Variable that the parent of the current node branched off
        //Direction of the current node (True/False)
        const [backString, branchVar, direction] = rot.current.backtrack();
        console.log(backString + branchVar + direction)
        renderTree();
        setRoot(rot);
        setParsedString(backString);

        let brancher;
        if(direction)
            brancher = branchVar;
        else
            brancher = ("¬"+branchVar);
        setbrancher(brancher);
        setbutton(branchVar);
        setselection(direction)

        console.log("Backtracking fields")
        console.log("backString: "+backString)
        console.log("brancher: "+brancher)
        console.log("button: "+button)
        console.log("selection: "+selection)



        //Need to generate the correct props for backbranch.Pass using mount prop?


        setCurrentComponent("backbranch");

        /*
        //Port of submitChoice() from variableBranch.js
        axios.get("//localhost:8080/api/justBranch?CNF="+parsedString+
            "&variable="+branchVar+"&value="+direction).then(r => {
            setParsedString(r.data);

            if(detectBranchFailure(r.data)) {
                axios.get("//localhost:8080/api/oneRound2?myCNF=" + parsedString).then(r => {
                    setParsedString(r.data)
                    doBranch(r.data);
                })
            }
        })*/
    }

    //getter function from child
    function getData(val){
        // do not forget to bind getData in constructor
        setParsedString(val);
        let r = root;
        r.current.string = val;
        setRoot(r);
    }
    function getComponent(){
        let component;
        switch (currentComponent){
            default :
                component = <h1>CNFLearner</h1>
                break;
            case "failure" :
                component = <Endings setDifficulty = {setDifficulty} difficulty = {difficulty} notLogged = {notLogged} setNotLogged = {setNotLogged} dataSent = {dataSent} setDataSent = {setDataSent} dataErrorMessage = {dataErrorMessage} setDataErrorMessage = {setDataErrorMessage} showLogin = {showLogin} fromEnding = {fromEnding} setFromEnding = {setFromEnding} rawPoints={rawPoints} wrongAnswers={wrongAnswers} sendUserData={receiveUserData} userid = {userid} username={username} backtracks = {backtracks} points = {currentPoints} failureReason={failureReason} ending={"failure"}></Endings>
                break;
            case "success" :
                component = <Endings setDifficulty = {setDifficulty} difficulty = {difficulty} notLogged = {notLogged} setNotLogged = {setNotLogged} dataSent = {dataSent} setDataSent = {setDataSent} dataErrorMessage = {dataErrorMessage} setDataErrorMessage = {setDataErrorMessage} showLogin = {showLogin} fromEnding = {fromEnding} setFromEnding = {setFromEnding} rawPoints={rawPoints} wrongAnswers={wrongAnswers} sendUserData={receiveUserData} userid = {userid} username={username} backtracks = {backtracks} points = {currentPoints} ending={"success"}></Endings>
                break;
            case "UP" :
                component = <UP wrongAnswerPenalty = {wrongAnswerPenalty} addPoints = {addPoints} handleConflict = {handleConflict} doSuccess = {doSuccess} detectBranchFailure = {detectBranchFailure} string={parsedString} sendData={getData} prepareBranch={doBranch} doPLE={doPLE}></UP>
                break;
            case "PLE" :
                component = <PLE doPLE = {doPLE} wrongAnswerPenalty = {wrongAnswerPenalty} addPoints = {addPoints} doSuccess = {doSuccess} detectBranchFailure = {detectBranchFailure} string={parsedString} sendData={getData} prepareBranch={doBranch} doUP={doUP}></PLE>
                break;
            case "backtrack" :
                component = <Endings ending = {"backtrack"} /*detectBranchFailure = {detectBranchFailure}*/ handleBacktrack={handleBacktrack} failureReason={failureReason}></Endings>
                break;
            case "entry" :
                component = <Entry setDifficulty = {setDifficulty} setCurrentlyPlaying = {setCurrentlyPlaying} toggleCanvas = {toggleCanvas} detectBranchFailure = {detectBranchFailure} id="entry" sendData={getData} prepareBranch={doBranch} doUP={doUP} doPLE={doPLE}/>;
                break;
            case "tree" :
                component = <TreeView renderTree = {renderTree} setTree = {overrideTree} detectBranchFailure = {detectBranchFailure} id="entry" sendData={getData} prepareBranch={doBranch} doUP={doUP} doPLE={doPLE}/>
                break;
            //case "branch" :
            //    component = <Branch id={"branch"} string={parsedString} detectBranchFailure = {detectBranchFailure} handleBranch = {handleBranch} sendData={getData} prepareBranch={doBranch} doUP={doUP} doPLE={doPLE}/>;
            //    break;
            case "branch" :
                component = <BranchWrapper wrongAnswerPenalty = {wrongAnswerPenalty} addPoints = {addPoints} handleConflict = {handleConflict} phase={"1"} id={"branch"} string={parsedString} detectBranchFailure = {detectBranchFailure} handleBranch = {handleBranch} sendData={getData} prepareBranch={doBranch} doUP={doUP} doPLE={doPLE}/>;
                break;
            case "branch1" :
                component = <Branch2 handleBranch = {handleBranch} phase2 = {phase2} setBrancher={setbrancher} string={parsedString}></Branch2>
                break;
            case "branch2" :
                component = <BranchWrapper2 setBrancher={setbrancher} brancher={brancher} wrongAnswerPenalty = {wrongAnswerPenalty} addPoints = {addPoints} handleConflict = {handleConflict} phase={"2"} id={"branch"} string={parsedString} detectBranchFailure = {detectBranchFailure} handleBranch = {handleBranch} sendData={getData} prepareBranch={doBranch} doUP={doUP} doPLE={doPLE}/>;
                break;
            case "backbranch" :
                component = <BackBranchWrapper wrongAnswerPenalty = {wrongAnswerPenalty} addPoints = {addPoints} handleConflict = {handleConflict} selection={selection} button={button} brancher={brancher} buttonphase={"2"} id={"branch"} string={parsedString} detectBranchFailure = {detectBranchFailure} handleBranch = {handleBranch} sendData={getData} prepareBranch={doBranch} doUP={doUP} doPLE={doPLE}/>;
                break;
            case "chooser" :
                component = <ModeChooser doChallenge = {doChallenge} doMultiplayer = {doMultiplayer} doEntry={doEntry} doTree={treeView}/>
                break;
            case "shop" :
                component = <Shop setCoins = {setcoins} userid={userid}></Shop>
                break;
            case "settings" :
                component = <Settings userid = {userid}></Settings>
                break;
            case "multiplayer" :
                component = <MultiplayerWrapper renderTree = {renderTree} toggleCanvas = {toggleCanvasMP} handleBacktrack = {handleBacktrackMP} handleBranch = {handleBranchMP} sendUserData = {receiveUserData} setInitLobby = {setInitLobby} initLobby = {initLobby} setComponent = {setMultiplayerComponent} component = {multiplayerComponent} username = {username} userid={userid}></MultiplayerWrapper>
                break;
            case "challenge" :
                component = <Challenge setDifficulty = {setDifficulty} userid={userid} username={username} setCurrentlyPlaying = {setCurrentlyPlaying} toggleCanvas = {toggleCanvas} detectBranchFailure = {detectBranchFailure} sendData={getData} prepareBranch={doBranch} doUP={doUP} doPLE={doPLE}></Challenge>
                break;
            case "blank" :
                component = <div></div>
                break;
        }
        return component;
    }
    function doBranch(data){
        renderTree();
        console.log("Here!")
        if(detectBranchFailure(data)) {
            console.log("Here2!")
            setCurrentComponent("branch1")
        }
    }

    function phase2(){
        setCurrentComponent("branch2")
    }

    /*
    function clearCanvas(){
        let canvas = document.getElementById("can");
        const context = canvas.getContext('2d');
        context.clearRect(0, 0, canvas.width, canvas.height);
    }*/
    function doChallenge(){
        console.log("Challenge mode!")
        setCurrentComponent("challenge")
    }

    async function overrideTree(tree) {
        await setRoot(tree);
        await renderTree();
    }

    function doEntry() {
        setCurrentComponent("entry")
    }

    function doMultiplayer() {
        setCurrentComponent("multiplayer")
    }

    function treeView() {
        toggleCanvas();
        renderTree();
        setCurrentComponent("tree")
    }

    function handleConflict(){
        addPoints(20).then(() => {
            setFailureReason("Conflict of attempting to set a variable to both true and false")
            doBranch("Empty clauses detected");
        });
    }

    function doUP(data){
        renderTree();
        if(detectBranchFailure(data)) {
            setCurrentComponent("UP")
        }
    }

    function doSuccess(){
        renderTree();
        setCurrentComponent("success")
    }

    function doPLE(data){
        renderTree();
        if(detectBranchFailure(data)) {
            setCurrentComponent("PLE")
        }
    }

    function detectBranchFailure(data){
        //TODO: Instead of checking for that phrase, we examine it for some empty clause.

        if((data === "Empty clauses detected") /*||(data === "")*/){
            if(root.current === root){
                console.log("made it");
                console.log("current points "+currentPoints)
                setCurrentlyPlaying(0);
                setCurrentComponent("failure")
            } else
                setCurrentComponent("backtrack")
            return false
        }
        else if(data === "Fully simplified"){
            successThing().then( () => {
                return false
            });
        }
        else {
            return true
        }
    }

    async function successThing(){
        console.log("current points "+currentPoints)
        await setCurrentPoints(currentPoints - (30*backtracks))
        console.log("current points " + currentPoints)
        await setCurrentlyPlaying(0);
        await setCurrentComponent("success")
    }
    function refreshPage() {
        window.location.reload();
    }

    function toggleCanvas() {
        if (canvasState === 0){
            setCanvasState(1);
        }
        else setCanvasState(0);
    }
    function choose() {
        if (canvasState){
            return(
                <div className="contents2" id={"candiv"}>
                    <h3>Decision tree</h3>
                    <canvas className="canvas" id={"can"}></canvas>
                </div>
            )
        }
        return null;
    }

    function showLogin() {
        const loginCon = document.getElementById("loginContainer");
        console.log("show")
        loginCon.style.display = "block";
    }
    function hideLogin() {
        const loginCon = document.getElementById("loginContainer");
        console.log("hide")
        loginCon.style.display = "none";
    }

    function showInfo() {
        const InfoCon = document.getElementById("infoContainer");
        console.log("show")
        InfoCon.style.display = "block";
    }
    function hideInfo() {
        const InfoCon = document.getElementById("infoContainer");
        console.log("hide")
        InfoCon.style.display = "none";
    }

    function showDeactivate() {
        const deacon = document.getElementById("deactivateContainer");
        console.log("show")
        deacon.style.display = "block";
        hideLogin();
    }
    function hideDeactivate() {
        showLogin();
        const deacon = document.getElementById("deactivateContainer");
        console.log("hide");
        deacon.style.display = "none";
    }

    function showConfirm() {
        const concon = document.getElementById("confirmContainer");
        console.log("show")
        concon.style.display = "block";
    }
    function hideConfirm() {
        const concon = document.getElementById("confirmContainer");
        console.log("hide");
        concon.style.display = "none";
    }

    function showConfirm2() {
        const concon = document.getElementById("confirm2Container");
        console.log("show")
        concon.style.display = "block";
    }
    function hideConfirm2() {
        const concon = document.getElementById("confirm2Container");
        console.log("hide");
        concon.style.display = "none";
    }

    function openSettings(){
        if(currentlyPlaying === 1){
            //Open confirmation popup (generic? do on home button too?)
            showConfirm2()
        }
        else{
            //Directly switch to shop component
            doSettings()
        }
    }

    function doSettings(){
        hideConfirm2()
        setCurrentlyPlaying(0)
        setCurrentComponent("settings")
        setCanvasState(0);
    }

    function doDeactivate() {
        childRef.current.deactivate(deacPassword);
    }


    function loginButtonText() {
        if (loginState === 1) {
            return "Log in"
        }
        else return "Account ("+username+")"
    }

    function receiveUserData(data, username){
        //console.log("me data is "+data.userid)
        setuserid(data.userid);
        setxp(data.xp);
        setcoins(data.coins);
        setlevel(data.level);
        setusername(username);
        console.log("userdata received:")
        console.log(data)
        console.log("username: "+username)
    }

    function scoreDisplay(){
        if(currentlyPlaying === 1){
            return <h2>Score: {currentPoints}</h2>
        }
        else return null;
    }

    function openShop(){
        if(currentlyPlaying === 1){
            //Open confirmation popup (generic? do on home button too?)
            showConfirm()
        }
        else{
            //Directly switch to shop component
            doShop()
        }
    }

    function doShop(){
        hideConfirm()
        setCurrentlyPlaying(0)
        setCurrentComponent("shop")
        setCanvasState(0);
    }

    function getCurrentXP(){
        if(xp < 50){
            return xp;
        } else if(xp < 110){
            return xp - 50;
        } else if(xp < 180){
            return xp - 110;
        } else if(xp < 260){
            return xp - 180;
        } else if(xp < 350){
            return xp - 260;
        } else {
            let smallXP = 100 - ((350 + (100*(level - 5))) - xp) // xp must be more than 350
            return smallXP;
        }
    }
    function getRemainingXP(){
        if(xp < 50){
            return 50;
        } else if(xp < 110){
            return 60;
        } else if(xp < 180){
            return 70;
        } else if(xp < 260){
            return 80;
        } else if(xp < 350){
            return 90;
        } else {
            return 100
        }
    }

    /*
    setdeactivateIncorrectError(false);
    setdeactivateIncorrectText("");
    <button className="menuButton"
                            onClick={toggleCanvas}>toggle Canvas</button>
                    <button className="menuButton"
                            onClick={clearCanvas}>clear Canvas</button>
    */

  return (
    <div className="App">
        <div id="loginContainer" className="loginContainer">
            <div className="loginInner">
                <div>
                    <div className="loginBox">
                        <Login setNotLogged = {setNotLogged} dataSent = {dataSent} setDataSent = {setDataSent} dataErrorMessage = {dataErrorMessage} setDataErrorMessage = {setDataErrorMessage} points = {currentPoints} fromEnding = {fromEnding} setFromEnding = {setFromEnding} ref={childRef} id="login" sendUserData={receiveUserData} setdeactivateIncorrectError={setdeactivateIncorrectError} setdeactivateIncorrectText={setdeactivateIncorrectText } showDeactivate={showDeactivate} hideDeactivate={hideDeactivate} setLoginState={setLoginState} hideLogin={hideLogin}></Login>
                    </div>
                </div>
            </div>
        </div>
        <div id="infoContainer" className="infoContainer">
            <div className="infoInner">
                <div>
                    <div className="infoBox">
                        <Info hideInfo={hideInfo} multiplayerComponent={multiplayerComponent} currentComponent={currentComponent}></Info>
                    </div>
                </div>
            </div>
        </div>
        <div id="deactivateContainer" className="deacContainer">
            <div className="deacInner">
                <div>
                    <div className="deacBox">
                        <div>
                            <h2>Deactivate</h2>
                            <p>Enter password to confirm account deletion.</p>
                            <div>
                                <TextField
                                    className={"inputBox"}
                                    id={"entryField"}
                                    name={"myCNF"}
                                    error={deactivateIncorrectError}
                                    helperText={deactivateIncorrectText}
                                    value={deacPassword} label="password"
                                    onChange={(e) => {
                                        setDeacPassword(e.target.value);
                                        setdeactivateIncorrectError(false);
                                        setdeactivateIncorrectText("");
                                    }}
                                />
                            </div>
                            <button onClick ={doDeactivate}>Confirm</button>
                            <button onClick={hideDeactivate}>Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="confirmContainer" className="confirmContainer">
            <div className="confirmInner">
                <div>
                    <div className="confirmBox">
                        <div>
                            <p>Points will be lost on leaving!</p>
                            <button onClick ={doShop}>Confirm</button>
                            <button onClick={hideConfirm}>Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="confirm2Container" className="confirmContainer">
            <div className="confirmInner">
                <div>
                    <div className="confirmBox">
                        <div>
                            <p>Points will be lost on leaving!</p>
                            <button onClick ={doSettings}>Confirm</button>
                            <button onClick={hideConfirm2}>Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className="frameContainers">
            <div className="menu">
                <div>
                    <button className="menuButton"
                            onClick={refreshPage}>Home</button>
                    <button className="menuButton"
                            onClick={showLogin}>{loginButtonText()}</button>
                    <button className="menuButton"
                            onClick={openShop}>Shop</button>
                    <button className="menuButton"
                            onClick={openSettings}>Settings</button>
                    <button className="menuButton"
                            onClick={showInfo}>Info</button>
                </div>
                <h2 className="menuHeader" >CNFLearner</h2>
                <div className={"levelBar"}>
                    Level {level} ({getCurrentXP()} / {getRemainingXP()})
                    <progress className={"bar"} value = {(getCurrentXP() / getRemainingXP())}></progress>
                    <div className={"coins"}>Coins: <GiTwoCoins></GiTwoCoins>{coins}</div>
                </div>
            </div>
        </div>
        <div className="frameContainers">
            <div className="binky">
                <div className="contents">
                    <div>
                        {getComponent()}
                        {scoreDisplay()}
                    </div>
                </div>
                {choose()}
            </div>
        </div>
    </div>
  );
}
/*        <h3>User ID: {userid}</h3>
        <h3>Level {level} ({getCurrentXP()} / {getRemainingXP()})</h3>
        <h3>Coins: {coins}</h3>
        <button className="menuButton"
                onClick={toggleCanvas}>toggle Canvas</button>
        <button className="menuButton"
                onClick={renderTree}>render Tree</button>*/

//<h2>From ending: {fromEnding}</h2>
//        <h3>XP: {xp} / {((level - 5)*100) + 450}</h3>
//<li className="myLi">
//                         <Button onClick={refreshPage}>Reset</Button>
//                     </li>
//<button onClick={() => console.log("var: "+root.current.variable+", string: "+root.current.string)}>Log string value of current node</button>
//         <button onClick={() => console.log("var: "+root.current.variable+", string: "+root.current.parent.string)/*ignore the error*/}>Log string value of parent node</button>
//

//<h3>Root value: {root.variable}</h3>
//                     <button onClick={renderTree}>Render tree</button>
//<div>
//                         <h3>Current string: {parsedString}</h3>
//                     </div>
export default App;
